package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.Random;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.comm.EnumUpdateType;
import com.pixelmonmod.pixelmon.comm.packetHandlers.OpenReplaceMoveScreen;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.storage.PlayerNotLoadedException;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.EntityInteract;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class TutorBookHandler 
{
	Random rng;
	
	public TutorBookHandler()
	{
		rng = new Random();
	}
	
	@SubscribeEvent
	public void onPlayerInteract(EntityInteract event) throws PlayerNotLoadedException
	{
		if(event.getTarget() instanceof EntityPixelmon)
		{
			if(event.getHand() != EnumHand.MAIN_HAND)
			{
				return;
			}
			EntityPixelmon pixelmon = (EntityPixelmon) event.getTarget();
			EntityPlayerMP player = (EntityPlayerMP) event.getEntityPlayer();
			
			if(!pixelmon.belongsTo(player) && !pixelmon.isInRanchBlock)
			{
				return;
			}
			if(player.getHeldItem(EnumHand.MAIN_HAND) != null)
			{
				ItemStack item = player.getHeldItem(EnumHand.MAIN_HAND);
				if(item.getItem() == Items.ENCHANTED_BOOK)
				{
					NBTTagCompound nbt = item.getTagCompound();
					if(nbt != null)
					{
						if(nbt.hasKey("movename"))
						{
							event.setCanceled(true);
							Attack attack = getTutorAttack(nbt.getString("movename"), pixelmon);
							if(attack != null)
							{
								if(pixelmon.baseStats.canLearn(attack.baseAttack.getUnLocalizedName()))
								{
									if (pixelmon.getMoveset().size() >= 4)
							        {
							          for (int i = 0; i < 4; i++) {
							            if (pixelmon.getMoveset().get(i).baseAttack.equals(attack.baseAttack)) {

											player.sendMessage(new TextComponentString(TextFormatting.RED + "Pokemon already knows this move!"));
											return;
							            }
							          }
							          Pixelmon.network.sendTo(new OpenReplaceMoveScreen(pixelmon.getPokemonId(), attack.baseAttack.attackIndex, 0, pixelmon.getLvl().getLevel()),player);
							          //player.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Learned " + attack.baseAttack.getLocalizedName()));

										if(player.getHeldItem(EnumHand.MAIN_HAND).getCount() > 1)
										{
											player.getHeldItem(EnumHand.MAIN_HAND).setCount(player.getHeldItem(EnumHand.MAIN_HAND).getCount() - 1);
										}
										else
										{
											player.inventory.removeStackFromSlot(player.inventory.currentItem);
										}
							        }
							        else
							        {
							          for (int i = 0; i < pixelmon.getMoveset().size(); i++) {
							            if (pixelmon.getMoveset().get(i).baseAttack.equals(attack.baseAttack)) {
											player.sendMessage(new TextComponentString(TextFormatting.RED + pixelmon.getPokemonName() + " already knows " + attack.baseAttack.getLocalizedName()));
											return;
							            }
							          }
							          pixelmon.getMoveset().add(attack);
							          player.sendMessage(new TextComponentString(TextFormatting.GRAY + pixelmon.getPokemonName() + " learned " + attack.baseAttack.getLocalizedName() + "!"));

										if(player.getHeldItem(EnumHand.MAIN_HAND).getCount() > 1)
										{
											player.getHeldItem(EnumHand.MAIN_HAND).setCount(player.getHeldItem(EnumHand.MAIN_HAND).getCount() - 1);
										}
										else
										{
											player.inventory.removeStackFromSlot(player.inventory.currentItem);
										}
							        }
							        pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });
									
									player.inventoryContainer.detectAndSendChanges();
									//event.setCanceled(true);
								}
								else
								{
									player.sendMessage(new TextComponentString(TextFormatting.RED + pixelmon.getPokemonName() + " can't learn " + attack.baseAttack.getLocalizedName()));
								}
							}
							else
							{
								player.sendMessage(new TextComponentString(TextFormatting.RED + pixelmon.getPokemonName() + " can't learn " + nbt.getString("movename")));
							}
						}
					}
				}
			}
//			if(Block.getBlockFromItem(item.getItem()) != null)
//			{
//				if(Block.getBlockFromItem(item.getItem()) == PixelUtilitiesAdditions.boxBlock)
//				{
//					NBTTagCompound nbt = item.getTagCompound();
//					if(nbt != null)
//					{
//						if(nbt.hasKey("identifier"))
//						{
		}
	}
	public Attack getTutorAttack(String pAttackName, EntityPixelmon pixelmon)
	{
//		ArrayList<Attack> tutorList = DatabaseMoves.getAllTutorAttacks(pixelid);//DatabaseMoves.getAllTutorAttacks(true);

		ArrayList<Attack> tutorList = pixelmon.baseStats.getTutorMoves();//DatabaseMoves.getAllTutorAttacks(true);
		
		for(Attack att : tutorList)
		{
			if(att.baseAttack.getUnLocalizedName().equals(pAttackName))
			{
				return att;
			}
		}
		return null;
	}
}
