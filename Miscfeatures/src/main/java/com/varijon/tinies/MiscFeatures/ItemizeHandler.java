package com.varijon.tinies.MiscFeatures;

import java.util.Optional;

import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
import com.pixelmonmod.pixelmon.config.PixelmonItems;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.storage.ComputerBox;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerComputerStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ItemizeHandler 
{
	@SubscribeEvent
	public void onPlayerInteract(PlayerInteractEvent.RightClickBlock event)
	{
//		if(event.action == Action.RIGHT_CLICK_BLOCK || event.action == Action.RIGHT_CLICK_AIR)
//		{
			EntityPlayerMP player = (EntityPlayerMP) event.getEntityPlayer();
			if(player.getHeldItem(EnumHand.MAIN_HAND) != null)
			{
				ItemStack item = player.getHeldItem(EnumHand.MAIN_HAND);
				if(item.getItem() == PixelmonItems.itemPixelmonSprite)
				{
					NBTTagCompound tags = item.getTagCompound();
					if(tags.hasKey("pixelmonData"))
					{
						Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
						if (!optPlayerStorage.isPresent()) 
						{
					        return;
					    }
						PlayerStorage playerStorage = (PlayerStorage) optPlayerStorage.get();
						PlayerComputerStorage playerComputer = PixelmonStorage.computerManager.getPlayerStorage(player);
															
						if(checkExists(playerStorage, playerComputer, tags.getCompoundTag("pixelmonData").getLong("UUIDLeast"),tags.getCompoundTag("pixelmonData").getLong("UUIDMost")))
						{
							player.sendMessage(new TextComponentString(TextFormatting.RED + "This pixelmon already exists in your storage!"));
							return;
						}
						
						EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(tags.getCompoundTag("pixelmonData"), player.world);
						playerStorage.addToParty(pokemon);
						
						MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
						
						PixelmonStorage.pokeBallManager.savePlayer(server, playerStorage);
						
						if(player.getHeldItem(EnumHand.MAIN_HAND).getCount() > 1)
						{
							player.getHeldItem(EnumHand.MAIN_HAND).setCount(player.getHeldItem(EnumHand.MAIN_HAND).getCount());
						}
						else
						{
							player.inventory.removeStackFromSlot(player.inventory.currentItem);
						}
						player.sendMessage(new TextComponentString(TextFormatting.GREEN + "De-itemized " + TextFormatting.RED + pokemon.getPokemonName()));

					}
				}
			}
			
//		}
	}
	
	private boolean checkExists(PlayerStorage ps, PlayerComputerStorage pc, long UUIDL, long UUIDM)
	{
		for(NBTTagCompound tags : ps.partyPokemon)
		{
			if(tags != null)
			{
				long least = tags.getLong("UUIDLeast");
				long most = tags.getLong("UUIDMost");
				if(UUIDL == least && UUIDM == most)
				{
					return true;
				}
			}
		}
		for(ComputerBox box : pc.getBoxList())
		{
			for(NBTTagCompound tags : box.getStoredPokemon())
			{
				if(tags != null)
				{
					long least = tags.getLong("UUIDLeast");
					long most = tags.getLong("UUIDMost");
					if(UUIDL == least && UUIDM == most)
					{
						return true;
					}
				}
			}
		}
		return false;
	}
//	private void setAllShinyToNormal(PlayerStorage ps, PlayerComputerStorage pc)
//	{
//		for(NBTTagCompound tags : ps.partyPokemon)
//		{
//			if(tags.getBoolean("isShiny"))
//			{
//				tags.setBoolean("isShiny", false);
//			}
//		}
//		for(ComputerBox box : pc.getBoxList())
//		{
//			for(NBTTagCompound tags : box.getStoredPokemon())
//			{
//				if(tags.getBoolean("isShiny"))
//				{
//					tags.setBoolean("isShiny", false);
//				}
//			}
//		}
//	}
}
