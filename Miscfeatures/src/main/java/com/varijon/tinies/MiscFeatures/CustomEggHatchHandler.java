package com.varijon.tinies.MiscFeatures;

import java.util.HashMap;
import java.util.Optional;

import com.pixelmonmod.pixelmon.comm.EnumUpdateType;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;

public class CustomEggHatchHandler 
{
	int counter = 0;
	MinecraftServer server;

	HashMap<String, HatchPlayer> userList;
	
	public CustomEggHatchHandler()
	{
		server = FMLCommonHandler.instance().getMinecraftServerInstance();
		userList = new HashMap<String, HatchPlayer>();
	}
	
	@SubscribeEvent
	public void onPlayerJoin (PlayerLoggedInEvent event)
	{
		EntityPlayerMP targetPlayer = (EntityPlayerMP) event.player;
		userList.put(targetPlayer.getUniqueID().toString(), new HatchPlayer(targetPlayer.getPosition().getX(), targetPlayer.getPosition().getZ()));
	}
	
	@SubscribeEvent
	public void onWorldTick (WorldTickEvent event)
	{
		if(event.phase != Phase.END)
		{
			return;
		}
		if(!event.world.getWorldInfo().getWorldName().equals("world"))
		{
			return;
		}
		if(counter == 20)
		{
			for(EntityPlayerMP player : server.getPlayerList().getPlayers())
			{
				HatchPlayer hatchPlayer = userList.get(player.getUniqueID().toString());
				if(hatchPlayer != null)
				{
					if(hatchPlayer.getLastX() != player.getPosition().getX() || hatchPlayer.getLastZ() != player.getPosition().getZ())
					{
						hatchPlayer.setLastX(player.getPosition().getX());
						hatchPlayer.setLastZ(player.getPosition().getZ());
						
						if(hatchPlayer.getEggSteps() < 50)
						{
							hatchPlayer.incrementEggSteps();
						}
						else
						{
							updatePlayerEggs(player);
							hatchPlayer.resetEggSteps();
						}
					}
				}
			}
			counter = 0;
			return;
		}
		counter++;
	}
	
	public void updatePlayerEggs(EntityPlayerMP player)
	{
		Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
		if (!optPlayerStorage.isPresent()) 
		{
	        return;
	    }
		PlayerStorage playerStorage = (PlayerStorage) optPlayerStorage.get();
				
		int partyCount = playerStorage.count();
		boolean flameBody = false;
		
		for(int x = 0; x < 6; x++)
		{
			
			EntityPixelmon pixelmon = playerStorage.getPokemon(playerStorage.getIDFromPosition(x), player.getEntityWorld());
			if (pixelmon == null)
			{
				continue;
			}
			if(pixelmon.getAbility().getLocalizedName().equals("Flame Body") || pixelmon.getAbility().getLocalizedName().equals("Magma Armor"))
			{
				flameBody = true;
				break;
			}
		}
		
		for(int x = 0; x < 6; x++)
		{
			EntityPixelmon pixelmon = playerStorage.getPokemon(playerStorage.getIDFromPosition(x), player.getEntityWorld());
			if (pixelmon == null)
			{
				continue;
			}
			if(!pixelmon.isEgg)
			{
				continue;
			}
			if(pixelmon.eggCycles > 0)
			{
				if(flameBody)
				{
					pixelmon.eggCycles -=2;					
				}
				else
				{
					pixelmon.eggCycles -=1;					
				}
			}
			if(pixelmon.eggCycles <= 0)
			{
				pixelmon.isEgg = false;
				ItemStack item = Util.getItem(pixelmon);
				TextComponentTranslation chatTrans = new TextComponentTranslation("", new Object());
				chatTrans.appendSibling(new TextComponentString(TextFormatting.GREEN + "Your egg in slot " + TextFormatting.RED + (x+1) + TextFormatting.GREEN + " hatched into a "));
				chatTrans.appendSibling(item.getTextComponent());
				chatTrans.appendSibling(new TextComponentString(TextFormatting.GREEN + " !"));
				//player.sendMessage(new TextComponentString(TextFormatting.GREEN + "Your egg in slot " + TextFormatting.RED + (x+1) + TextFormatting.GREEN + " hatched into a " + pixelmon.getPokemonName() + " !"));
				player.sendMessage(chatTrans);
				player.getServerWorld().playSound(null, new BlockPos(player.posX, player.posY, player.posZ), SoundEvents.ENTITY_CHICKEN_EGG, SoundCategory.PLAYERS, 0.5f, 1f);	
				
				
				if(pixelmon.getIsShiny())
				{
					StringBuilder sb = new StringBuilder();
					
					sb.append(TextFormatting.WHITE + "[" + TextFormatting.RED + "Oh Yes" + TextFormatting.WHITE + "] ");
					sb.append(TextFormatting.LIGHT_PURPLE + player.getName() + TextFormatting.GRAY + " has hatched a ");
					sb.append(TextFormatting.GOLD + "Shiny ");
					sb.append(TextFormatting.AQUA + pixelmon.getPokemonName() + TextFormatting.GRAY +"!");
					server.getPlayerList().sendMessage(new TextComponentString(sb.toString()));			

				}
			}
			pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Egg });
			
		}
	}
}
