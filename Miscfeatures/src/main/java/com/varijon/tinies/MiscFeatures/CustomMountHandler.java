package com.varijon.tinies.MiscFeatures;

import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.EntityInteract;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class CustomMountHandler 
{
	int counter = 0;
	MinecraftServer server;
	//ArrayList<BlockPos> tempBlock;
	World world;
	
	public CustomMountHandler()
	{
		server = FMLCommonHandler.instance().getMinecraftServerInstance();
		//tempBlock = new ArrayList<BlockPos>();
	}
	
	@SubscribeEvent
	public void onPlayerEntityInteract(EntityInteract event)
	{
		if(event.getTarget() instanceof EntityPixelmon)
		{
			EntityPixelmon pixelmon = (EntityPixelmon)event.getTarget();
			EntityPlayer player = event.getEntityPlayer();
			
			
			if(!player.isRiding() && player.getHeldItem(EnumHand.MAIN_HAND).getItem() == Items.AIR)
			{
				if(pixelmon.belongsTo(player) && !pixelmon.isInRanchBlock)
				{
					
					if(checkIfAllowedMount(pixelmon.getPokemonName()))
					{
						EntityPlayerMP playerMP = (EntityPlayerMP) player;
						playerMP.startRiding(pixelmon);
					}
					if(checkIfAllowedFlying(pixelmon.getPokemonName()))
					{
						pixelmon.baseStats.canFly = true;
						pixelmon.baseStats.isRideable = true;
					}
					if(checkIfAllowedSurf(pixelmon.getPokemonName()))
					{
						pixelmon.baseStats.canSurf = true;
					}
				}
			}
		}
	}
	
//	@SubscribeEvent
//	public void onWorldTick (WorldTickEvent event)
//	{
//		if(counter == 5)
//		{
//			if(world == null)
//			{
//				world = DimensionManager.getWorld(0);
//			}
//			for(BlockPos pos : tempBlock)
//			{
//				world.setBlockToAir(pos);
//			}
//			tempBlock.clear();
//		}
//		if(counter == 20)
//		{
//			for(EntityPlayerMP player : server.getPlayerList().getPlayers())
//			{
//				if(player.isRiding())
//				{
//					Entity ridden = player.getRidingEntity();
//					if(ridden instanceof EntityPixelmon || ridden instanceof EntityBoat)
//					{
//						if(world.getBlockState(ridden.getPosition()).getBlock() == Blocks.AIR)
//						{
//							
//							world.setBlockState(ridden.getPosition(), Blocks.STRUCTURE_VOID.getDefaultState());
//							tempBlock.add(ridden.getPosition());
//						}
//					}
//				}
//			}
//			counter = 0;
//		}
//		counter++;
//	}
	
	private boolean checkIfAllowedMount(String pName)
	{
		for(String name : Main.allowedMounts)
		{
			if(name.equals(pName))
			{
				return true;
			}
		}
		return false;
	}
	private boolean checkIfAllowedFlying(String pName)
	{
		for(String name : Main.allowedFlying)
		{
			if(name.equals(pName))
			{
				return true;
			}
		}
		return false;
	}
	private boolean checkIfAllowedSurf(String pName)
	{
		for(String name : Main.allowedSurf)
		{
			if(name.equals(pName))
			{
				return true;
			}
		}
		return false;
	}
}
