package com.varijon.tinies.MiscFeatures;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;

public class PortalBlockHandler 
{
	int counter = 0;
	MinecraftServer server;
	
	public PortalBlockHandler()
	{
		server = FMLCommonHandler.instance().getMinecraftServerInstance();
	}
	
	@SubscribeEvent
	public void onWorldTick (WorldTickEvent event)
	{
		if(event.phase != Phase.END)
		{
			return;
		}
		if(!event.world.getWorldInfo().getWorldName().equals("world"))
		{
			return;
		}
		if(counter == 10)
		{
			for(EntityPlayerMP targetPlayer : server.getPlayerList().getPlayers())
			{
				removePortalBlocks(targetPlayer.getPosition(), targetPlayer.getServerWorld());
			}
			counter = 0;
			return;
		}
		counter++;
	}
	public void removePortalBlocks(BlockPos loc, World w)
	{
		for(int x = -1; x < 1; x++)
		{
			for(int z = -1; z < 1; z++)
			{
				for(int y = -1; y < 1; y++)
				{
					BlockPos locNew = loc.add(x, y, z);
					
					if(w.getBlockState(locNew).getBlock().equals(Blocks.PORTAL))
					{
						w.playSound(null, loc, SoundEvents.ENTITY_CHICKEN_EGG, SoundCategory.PLAYERS, 1f, 1f);
						w.setBlockToAir(locNew);
					}
														
				}
			}
		}
	}
}
