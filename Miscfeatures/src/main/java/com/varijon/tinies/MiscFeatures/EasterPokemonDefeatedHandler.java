package com.varijon.tinies.MiscFeatures;

import java.util.Calendar;

import com.pixelmonmod.pixelmon.RandomHelper;
import com.pixelmonmod.pixelmon.api.events.BeatWildPixelmonEvent;
import com.pixelmonmod.pixelmon.config.PixelmonItems;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EasterPokemonDefeatedHandler 
{
	@SubscribeEvent
	public void onWildDefeated(BeatWildPixelmonEvent event)
	{
		Calendar cal = Calendar.getInstance();
		boolean isEaster = false;
		if(cal.get(Calendar.MONTH) == 3)
		{
			if(cal.get(Calendar.DAY_OF_MONTH) == 14 || cal.get(Calendar.DAY_OF_MONTH) == 15 || cal.get(Calendar.DAY_OF_MONTH) == 16 || cal.get(Calendar.DAY_OF_MONTH) == 17 || cal.get(Calendar.DAY_OF_MONTH) == 18
					 || cal.get(Calendar.DAY_OF_MONTH) == 19 || cal.get(Calendar.DAY_OF_MONTH) == 20 || cal.get(Calendar.DAY_OF_MONTH) == 21 || cal.get(Calendar.DAY_OF_MONTH) == 22)
			{
				isEaster = true;
			}
		}
		if(!isEaster)
		{
			return;
		}
		
		//EntityPixelmon pixelmon = event.wpp.controlledPokemon[0].pokemon;
		EntityPixelmon pixelmon = event.wpp.controlledPokemon.get(0).pokemon;
		MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
		EntityPlayerMP player = event.player;
		
		if(RandomHelper.getRandomChance(20))
		{			
			int randomNum = RandomHelper.getRandomNumberBetween(0, 100);
			if(randomNum <= 10)
			{
				ItemStack finalItem = new ItemStack(PixelmonItems.itemPixelmonSprite);
				finalItem.setTagCompound(new NBTTagCompound());
									
				NBTTagCompound tags = finalItem.getTagCompound();
				tags.setString("SpriteName", "pixelmon:sprites/eggs/manaphy1");
				tags.setTag("display", new NBTTagCompound());
				tags.getCompoundTag("display").setString("Name", TextFormatting.YELLOW + "Rare Easter Egg");					

				NBTTagList loreList = new NBTTagList();
				loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Right click for " + TextFormatting.YELLOW + "great " + TextFormatting.GRAY + "Easter loot"));
				
				tags.getCompoundTag("display").setTag("Lore", loreList);
				
				tags.setString("identifier", "rareeaster");		
				
				finalItem.setTagCompound(tags);
				if(!player.inventory.addItemStackToInventory(finalItem))
				{
					World w = player.getEntityWorld();
					w.spawnEntity(new EntityItem(w, player.lastTickPosX, player.lastTickPosY, player.lastTickPosZ, finalItem));
				}
				player.sendMessage(new TextComponentString(TextFormatting.GREEN + "You received a " + TextFormatting.YELLOW + "Rare Easter Egg"));	
				return;							
			}
			if(randomNum <= 30)
			{
				ItemStack finalItem = new ItemStack(PixelmonItems.itemPixelmonSprite);
				finalItem.setTagCompound(new NBTTagCompound());
									
				NBTTagCompound tags = finalItem.getTagCompound();
				tags.setString("SpriteName", "pixelmon:sprites/eggs/togepi1");
				tags.setTag("display", new NBTTagCompound());
				tags.getCompoundTag("display").setString("Name", TextFormatting.BLUE + "Uncommon Easter Egg");					

				NBTTagList loreList = new NBTTagList();
				loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Right click for " + TextFormatting.BLUE + "good " + TextFormatting.GRAY + "Easter loot"));
				
				tags.getCompoundTag("display").setTag("Lore", loreList);
				
				tags.setString("identifier", "uncommoneaster");		
				
				finalItem.setTagCompound(tags);
				if(!player.inventory.addItemStackToInventory(finalItem))
				{
					World w = player.getEntityWorld();
					w.spawnEntity(new EntityItem(w, player.lastTickPosX, player.lastTickPosY, player.lastTickPosZ, finalItem));
				}
				player.sendMessage(new TextComponentString(TextFormatting.GREEN + "You received an " + TextFormatting.BLUE + "Uncommon Easter Egg"));	
				return;							
			}
			if(randomNum <= 100)
			{
				ItemStack finalItem = new ItemStack(PixelmonItems.itemPixelmonSprite);
				finalItem.setTagCompound(new NBTTagCompound());
									
				NBTTagCompound tags = finalItem.getTagCompound();
				tags.setString("SpriteName", "pixelmon:sprites/eggs/egg1");
				tags.setTag("display", new NBTTagCompound());
				tags.getCompoundTag("display").setString("Name", TextFormatting.GREEN + "Common Easter Egg");					

				NBTTagList loreList = new NBTTagList();
				loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Right click for " + TextFormatting.GREEN + "decent " + TextFormatting.GRAY + "Easter loot"));
				
				tags.getCompoundTag("display").setTag("Lore", loreList);
				
				tags.setString("identifier", "commoneaster");		
				
				finalItem.setTagCompound(tags);
				if(!player.inventory.addItemStackToInventory(finalItem))
				{
					World w = player.getEntityWorld();
					w.spawnEntity(new EntityItem(w, player.lastTickPosX, player.lastTickPosY, player.lastTickPosZ, finalItem));
				}
				player.sendMessage(new TextComponentString(TextFormatting.GREEN + "You received a " + TextFormatting.GREEN + "Common Easter Egg"));	
				return;						
			}				
		}	
		
	}
//	@SubscribeEvent
//	public void onPixelmonDelete(PixelmonDeletedEvent event)
//	{
//		EntityPixelmon pixelmon = (EntityPixelmon) PixelmonEntityList.createEntityFromNBT(event.pokemon, event.player.worldObj);
//		if(pixelmon.getIsShiny())
//		{
//			if(pixelmon.isEgg)
//			{			
//				MinecraftServer.getServer().getConfigurationManager().sendChatMsg(new TextComponentString(TextFormatting.WHITE + "[" + TextFormatting.RED + "Oh No" + TextFormatting.WHITE + "] " + TextFormatting.LIGHT_PURPLE + event.player.getName() + TextFormatting.GRAY + " has released a " + TextFormatting.GOLD + "SHINY " + TextFormatting.LIGHT_PURPLE + pixelmon.getPokemonName() + " Egg" + TextFormatting.GRAY + "!"));			
//			}
//			else
//			{				
//				MinecraftServer.getServer().getConfigurationManager().sendChatMsg(new TextComponentString(TextFormatting.WHITE + "[" + TextFormatting.RED + "Oh No" + TextFormatting.WHITE + "] " + TextFormatting.LIGHT_PURPLE + event.player.getName() + TextFormatting.GRAY + " has released a " + TextFormatting.GOLD + "SHINY " + TextFormatting.LIGHT_PURPLE + pixelmon.getPokemonName() + TextFormatting.GRAY + "!"));					
//			}
//			ItemStack finalItem = new ItemStack(PixelmonItems.itemPixelmonSprite);
//			finalItem.setTagCompound(new NBTTagCompound());
//								
//			NBTTagCompound tags = finalItem.getTagCompound();
//			tags.setString("SpriteName", "pixelmon:sprites/shinypokemon/star");
//			tags.setTag("display", new NBTTagCompound());
//			tags.getCompoundTag("display").setString("Name", TextFormatting.GOLD + "Shiny Star");					
//			
//			NBTTagList loreList = new NBTTagList();
//			loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Belonged to a " + TextFormatting.GOLD + "SHINY " + TextFormatting.LIGHT_PURPLE + pixelmon.getPokemonName()));
//			tags.getCompoundTag("display").setTag("Lore", loreList);
//			
//			finalItem.setTagCompound(tags);
//			
//			if(!event.player.inventory.addItemStackToInventory(finalItem))
//			{
//				World w = event.player.getEntityWorld();
//				w.spawnEntityInWorld(new EntityItem(w, event.player.lastTickPosX, event.player.lastTickPosY, event.player.lastTickPosZ, finalItem));
//			}
//			return;
//		}
//	}
}
