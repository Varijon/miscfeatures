package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import com.pixelmonmod.pixelmon.RandomHelper;
import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.comm.EnumUpdateType;
import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Gender;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class PokemonTokenHandler 
{		
	
	String[] eventMoves = new String[]{
		    "Absorb",
		    "Agility",
		    "Air Slash",
		    "Amnesia",
		    "Aqua Ring",
		    "Aromatherapy",
		    "Aura Sphere",
		    "Barrier",
		    "Baton Pass",
		    "Beat Up",
		    "Belly Drum",
		    "Bite",
		    "Blaze Kick",
		    "Bubble",
		    "Celebrate",
		    "Charm",
		    "Cotton Guard",
		    "Crush Claw",
		    "Dizzy Punch",
		    "Double Slap",
		    "Electro Ball",
		    "Encore",
		    "Eruption",
		    "Extrasensory",
		    "ExtremeSpeed",
		    "Feint Attack",
		    "Fake Out",
		    "FeatherDance",
		    "Flail",
		    "Flare Blitz",
		    "Focus Energy",
		    "Follow Me",
		    "Foresight",
		    "Fury Attack",
		    "Growth",
		    "Gust",
		    "Happy Hour",
		    "Harden",
		    "Haze",
		    "Heal Pulse",
		    "Heart Stamp",
		    "Hold Back",
		    "Hold Hands",
		    "Horn Attack",
		    "Howl",
		    "Hurricane",
		    "Hydro Pump",
		    "Hypnosis",
		    "Imprison",
		    "Inferno",
		    "Leech Seed",
		    "Leer",
		    "Lovely Kiss",
		    "Metal Sound",
		    "Meteor Mash",
		    "Mind Reader",
		    "Mist",
		    "Moonblast",
		    "Moonlight",
		    "Morning Sun",
		    "Mud Sport",
		    "Nasty Plot",
		    "Night Shade",
		    "Octazooka",
		    "Peck",
		    "Petal Dance",
		    "Phantom Force",
		    "Play Rough",
		    "Present",
		    "Psycho Boost",
		    "Pursuit",
		    "Quick Attack",
		    "Rapid Spin",
		    "Refresh",
		    "Reversal",
		    "Rock Throw",
		    "Scary Face",
		    "Screech",
		    "Sharpen",
		    "Sheer Cold",
		    "Shift Gear",
		    "Sing",
		    "Slash",
		    "SonicBoom",
		    "Spike Cannon",
		    "Splash",
		    "Stomp",
		    "Sweet Kiss",
		    "Tackle",
		    "Teeter Dance",
		    "Thrash",
		    "Tickle",
		    "V-create",
		    "Water Sport",
		    "Weather Ball",
		    "Wish",
		    "Yawn"
		    };
	ArrayList<String> eventMoveList;
	
	public PokemonTokenHandler()
	{
		eventMoveList = new ArrayList<String>();
		eventMoveList.addAll(Arrays.asList(eventMoves));
	}
	
	@SubscribeEvent
	public void onPlayerInteract(PlayerInteractEvent.RightClickItem event)
	{
		if(event.getHand() != EnumHand.MAIN_HAND)
		{
			return;
		}
		EntityPlayerMP player = (EntityPlayerMP) event.getEntityPlayer();
		if(CheckForPokemonToken(player.getHeldItem(EnumHand.MAIN_HAND)))
		{
			EntityPixelmon pokemonToGive = null;
//			boolean isShiny = false;
//			boolean isLegendary = false;
//			if(RandomHelper.getRandomChance(0.025f))
//			{
//				isLegendary = true;
//			}
//			if(RandomHelper.getRandomChance(0.05f))
//			{
//				isShiny = true;
//			}
//			if(isLegendary)
//			{
//				pokemonToGive = (EntityPixelmon)PixelmonEntityList.createEntityByName(getRandomLegendaryName(), player.getServerWorld());
//			}
//			else
//			{
			pokemonToGive = (EntityPixelmon)PixelmonEntityList.createEntityByName(getRandomPokemonName(), player.getServerWorld());
			if(pokemonToGive.getPreEvolutions().length > 0)
			{
				pokemonToGive = (EntityPixelmon)PixelmonEntityList.createEntityByName(pokemonToGive.getPreEvolutions()[pokemonToGive.getPreEvolutions().length-1].name, player.getServerWorld());
			}
			pokemonToGive.getLvl().setLevel(1);
			setTutorMove(pokemonToGive);
//			}
//			pokemonToGive.setIsShiny(isShiny);
			
			Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
			if (!optPlayerStorage.isPresent()) 
			{
		        return;
		    }
			PlayerStorage playerStorage = (PlayerStorage) optPlayerStorage.get();
			playerStorage.addToParty(pokemonToGive);
			
			MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
			
			PixelmonStorage.pokeBallManager.savePlayer(server, playerStorage);
			
			playerStorage.sendUpdatedList();
			player.sendMessage(new TextComponentString(TextFormatting.GREEN + "Obtained " + TextFormatting.RED + pokemonToGive.getPokemonName() + TextFormatting.GREEN + "!"));
			
			if(player.getHeldItem(EnumHand.MAIN_HAND).getCount() > 1)
			{
				player.getHeldItem(EnumHand.MAIN_HAND).setCount(player.getHeldItem(EnumHand.MAIN_HAND).getCount() - 1);
			}
			else
			{
				player.inventory.removeStackFromSlot(player.inventory.currentItem);
			}
			player.inventoryContainer.detectAndSendChanges();

//			if(isLegendary && !isShiny)
//			{
//				StringBuilder sb = new StringBuilder();
//				
//				sb.append(TextFormatting.WHITE + "[" + TextFormatting.RED + "Token" + TextFormatting.WHITE + "] ");
//				sb.append(TextFormatting.LIGHT_PURPLE + player.getName() + TextFormatting.GRAY + " obtained a ");
//				sb.append(TextFormatting.AQUA + pokemonToGive.getPokemonName() + TextFormatting.GRAY + " from a Pokemon Token!");
//				server.getPlayerList().sendMessage(new TextComponentString(sb.toString()));			
//			}
//			if(isShiny)
//			{
//				StringBuilder sb = new StringBuilder();
//				
//				sb.append(TextFormatting.WHITE + "[" + TextFormatting.RED + "Token" + TextFormatting.WHITE + "] ");
//				sb.append(TextFormatting.LIGHT_PURPLE + player.getName() + TextFormatting.GRAY + " obtained a " + TextFormatting.GOLD + "Shiny ");
//				sb.append(TextFormatting.AQUA + pokemonToGive.getPokemonName() + TextFormatting.GRAY + " from a Pokemon Token!");
//				server.getPlayerList().sendMessage(new TextComponentString(sb.toString()));			
//			}

		}

	}
	
	public boolean CheckForPokemonToken(ItemStack item)
	{
		if(item != null)
		{
			NBTTagCompound nbt = item.getTagCompound();
			if(nbt != null)
			{
				if(nbt.hasKey("isPokemonToken"))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public void setTutorMove(EntityPixelmon pixelmon)
	{
		ArrayList<Attack> tutorList = pixelmon.baseStats.getTutorMoves();
		ArrayList<Attack> eventList = new ArrayList<Attack>();
		
		for(Attack att : tutorList)
		{
			boolean cangetnormally = false;
			for(Attack lvlatt : pixelmon.baseStats.getMovesAtLevel(100))
			{
				if(lvlatt.baseAttack.getUnLocalizedName().equals(att.baseAttack.getUnLocalizedName()))
				{
						cangetnormally = true;
				}
			}
//			for(Attack lvlatt : DatabaseMoves.getAllEggAttacks(pixelmon))
//			{
//				if(lvlatt.baseAttack.getUnLocalizedName().equals(att.baseAttack.getUnLocalizedName()))
//				{
//					cangetnormally = true;
//				}
//			}
			if(!cangetnormally)
			{
				eventList.add(att);									
			}	
		}
		
		if(eventList.isEmpty())
		{
			return;
		}
		
		Attack pickedMove = eventList.get(RandomHelper.getRandomNumberBetween(0, eventList.size()-1));
//		System.out.println(pickedMove.baseAttack.getUnLocalizedName() + " size: " + eventList.size());
		if(pixelmon.getGender() == Gender.Female)
		{
			if(pixelmon.baseStats.malePercent != 0)
			{
				pixelmon.setGender(Gender.Male);							
			}						
		}
		pixelmon.loadMoveset();
		if(pixelmon.getMoveset().size() != 4)
		{
			pixelmon.getMoveset().add(pickedMove);
		}
		else
		{
			pixelmon.getMoveset().set(0,pickedMove);			
		}
		pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });
	}
	
	public String getRandomPokemonName()
	{
		String pokemonName = null;
		do
		{
			pokemonName = EnumPokemon.randomPoke().name;
		}
		while(EnumPokemon.legendaries.contains(pokemonName));
		return pokemonName;
	}
	public String getRandomLegendaryName()
	{
		String pokemonName = null;
		do
		{
			pokemonName = EnumPokemon.randomPoke().name;
		}
		while(!EnumPokemon.legendaries.contains(pokemonName));
		return pokemonName;
	}
}
