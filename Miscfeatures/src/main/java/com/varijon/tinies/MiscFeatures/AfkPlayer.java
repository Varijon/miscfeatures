package com.varijon.tinies.MiscFeatures;

public class AfkPlayer 
{
	double lastYaw;
	double lastPitch;
	long lastHeadMovement;
	
	public AfkPlayer(double lastYaw, double lastPitch, long lastHeadMovement)
	{
		this.lastYaw = lastYaw;
		this.lastPitch = lastPitch;
		this.lastHeadMovement = lastHeadMovement;
	}

	public double getLastYaw() {
		return lastYaw;
	}

	public void setLastYaw(double lastYaw) {
		this.lastYaw = lastYaw;
	}

	public double getLastPitch() {
		return lastPitch;
	}

	public void setLastPitch(double lastPitch) {
		this.lastPitch = lastPitch;
	}

	public long getLastHeadMovement() {
		return lastHeadMovement;
	}

	public void setLastHeadMovement(long lastHeadMovement) {
		this.lastHeadMovement = lastHeadMovement;
	}
	
}
