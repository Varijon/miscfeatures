package com.varijon.tinies.MiscFeatures;

import java.util.Random;

import com.pixelmonmod.pixelmon.RandomHelper;
import com.pixelmonmod.pixelmon.config.PixelmonBlocks;
import com.pixelmonmod.pixelmon.config.PixelmonItems;
import com.pixelmonmod.pixelmon.storage.PlayerNotLoadedException;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.HoverEvent;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EasterEggHandler 
{
	Random rng;
	
	public EasterEggHandler()
	{
		rng = new Random();
	}
	
	@SubscribeEvent
	public void onPlayerInteract(PlayerInteractEvent.RightClickItem event) throws PlayerNotLoadedException
	{
//		if(event..action == Action.RIGHT_CLICK_BLOCK || event.action == Action.RIGHT_CLICK_AIR)
//		{
			EntityPlayerMP player = (EntityPlayerMP) event.getEntityPlayer();
			if(player.getHeldItem(EnumHand.MAIN_HAND) != null)
			{
				ItemStack item = player.getHeldItem(EnumHand.MAIN_HAND);
				if(item.getItem() == PixelmonItems.itemPixelmonSprite && event.getWorld().getBlockState(event.getPos()).getBlock() != PixelmonBlocks.pokeChest
						&& event.getWorld().getBlockState(event.getPos()).getBlock() != PixelmonBlocks.ultraChest 
						&& event.getWorld().getBlockState(event.getPos()).getBlock() != PixelmonBlocks.masterChest)
				{
					NBTTagCompound nbt = item.getTagCompound();
					if(nbt != null)
					{
						if(nbt.hasKey("identifier"))
						{
							LootBox lootbox = LootBoxConfigManager.GetFromIdentifier(nbt.getString("identifier"));
							if(lootbox != null)
							{
								StringBuilder sb = new StringBuilder();
								sb.append(TextFormatting.GOLD + "Obtained loot:");
								for(LootItem lootitem : lootbox.GetItems())
								{
									if(RandomHelper.getRandomChance(lootitem.GetRarity()))
									{
										//rng stuff todo
										if(lootitem.GetItemData().size() > 1)
										{
											ItemStack finalItem = new ItemStack(lootitem.GetItemData().get(RandomHelper.getRandomNumberBetween(0, lootitem.GetItemData().size()-1)));
											finalItem.setCount(RandomHelper.getRandomNumberBetween(lootitem.GetLowAmount(), lootitem.GetHighAmount()));
											if(finalItem.getCount() != 0)
											{
												sb.append("\n" + TextFormatting.GRAY + "" + finalItem.getCount() + "x " + TextFormatting.GREEN + finalItem.getDisplayName());
												if(!player.inventory.addItemStackToInventory(finalItem))
												{
													World w = player.getEntityWorld();
													w.spawnEntity(new EntityItem(w, player.lastTickPosX, player.lastTickPosY, player.lastTickPosZ, finalItem));
												}
											}
										}
										else
										{
											ItemStack finalItem = new ItemStack(lootitem.GetItemData().get(0));
											finalItem.setCount(RandomHelper.getRandomNumberBetween(lootitem.GetLowAmount(), lootitem.GetHighAmount()));
											if(finalItem.getCount() != 0)
											{
												sb.append("\n" + TextFormatting.GRAY + "" + finalItem.getCount() + "x " + TextFormatting.GREEN + finalItem.getDisplayName());
												if(!player.inventory.addItemStackToInventory(finalItem))
												{
													World w = player.getEntityWorld();
													w.spawnEntity(new EntityItem(w, player.lastTickPosX, player.lastTickPosY, player.lastTickPosZ, finalItem));
												}
											}
										}
									}

								}

								if(player.getHeldItem(EnumHand.MAIN_HAND).getCount() > 1)
								{
									player.getHeldItem(EnumHand.MAIN_HAND).setCount(player.getHeldItem(EnumHand.MAIN_HAND).getCount() - 1);
								}
								else
								{
									player.inventory.removeStackFromSlot(player.inventory.currentItem);
								}
								player.inventoryContainer.detectAndSendChanges();
								TextComponentTranslation chatMsg = new TextComponentTranslation(TextFormatting.BOLD + "" + TextFormatting.GOLD + "[Show obtained Easter Loot]", new Object[0]);
								chatMsg.getStyle().setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponentString(sb.toString())));
								player.sendMessage(chatMsg);
								event.setCanceled(true);
							}
						}
					}
				}
//			}
//			if(Block.getBlockFromItem(item.getItem()) != null)
//			{
//				if(Block.getBlockFromItem(item.getItem()) == PixelUtilitiesAdditions.boxBlock)
//				{
//					NBTTagCompound nbt = item.getTagCompound();
//					if(nbt != null)
//					{
//						if(nbt.hasKey("identifier"))
//						{
		}
	}
}
