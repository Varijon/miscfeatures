package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.Arrays;

import com.pixelmonmod.pixelmon.RandomHelper;
import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.comm.EnumUpdateType;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Gender;

import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EventPokemonSpawn 
{
	
	String[] eventMoves = new String[]{
		    "Absorb",
		    "Agility",
		    "Air Slash",
		    "Amnesia",
		    "Aqua Ring",
		    "Aromatherapy",
		    "Aura Sphere",
		    "Barrier",
		    "Baton Pass",
		    "Beat Up",
		    "Belly Drum",
		    "Bite",
		    "Blaze Kick",
		    "Bubble",
		    "Celebrate",
		    "Charm",
		    "Cotton Guard",
		    "Crush Claw",
		    "Dizzy Punch",
		    "Double Slap",
		    "Electro Ball",
		    "Encore",
		    "Eruption",
		    "Extrasensory",
		    "ExtremeSpeed",
		    "Feint Attack",
		    "Fake Out",
		    "FeatherDance",
		    "Flail",
		    "Flare Blitz",
		    "Focus Energy",
		    "Follow Me",
		    "Foresight",
		    "Fury Attack",
		    "Growth",
		    "Gust",
		    "Happy Hour",
		    "Harden",
		    "Haze",
		    "Heal Pulse",
		    "Heart Stamp",
		    "Hold Back",
		    "Hold Hands",
		    "Horn Attack",
		    "Howl",
		    "Hurricane",
		    "Hydro Pump",
		    "Hypnosis",
		    "Imprison",
		    "Inferno",
		    "Leech Seed",
		    "Leer",
		    "Lovely Kiss",
		    "Metal Sound",
		    "Meteor Mash",
		    "Mind Reader",
		    "Mist",
		    "Moonblast",
		    "Moonlight",
		    "Morning Sun",
		    "Mud Sport",
		    "Nasty Plot",
		    "Night Shade",
		    "Octazooka",
		    "Peck",
		    "Petal Dance",
		    "Phantom Force",
		    "Play Rough",
		    "Present",
		    "Psycho Boost",
		    "Pursuit",
		    "Quick Attack",
		    "Rapid Spin",
		    "Refresh",
		    "Reversal",
		    "Rock Throw",
		    "Scary Face",
		    "Screech",
		    "Sharpen",
		    "Sheer Cold",
		    "Shift Gear",
		    "Sing",
		    "Slash",
		    "SonicBoom",
		    "Spike Cannon",
		    "Splash",
		    "Stomp",
		    "Sweet Kiss",
		    "Tackle",
		    "Teeter Dance",
		    "Thrash",
		    "Tickle",
		    "V-create",
		    "Water Sport",
		    "Weather Ball",
		    "Wish",
		    "Yawn"
		    };
	ArrayList<String> eventMoveList;
	
	public EventPokemonSpawn()
	{
		eventMoveList = new ArrayList<String>();
		eventMoveList.addAll(Arrays.asList(eventMoves));
	}
	
//	@SubscribeEvent
//	public void onWildPixelmonBattle(BattleStartedEvent event)
//	{
//		System.out.println("debug1");
//		if(event.participant2[0] instanceof WildPixelmonParticipant)
//		{
//			System.out.println("debug2");
//			if(event.participant1[0] instanceof PlayerParticipant)
//			{
//				System.out.println("debug3");
//				
//			}
//			else
//			{
//				return;
//			}
//			
//			PlayerParticipant playerPart = (PlayerParticipant) event.participant1[0];
//			EntityPlayerMP player = playerPart.player;
//			
//			EntityPixelmon pixelmon = event.participant2[0].controlledPokemon.get(0).pokemon;
//			for(Attack att : pixelmon.getMoveset().attacks)
//			{
//				System.out.println("debug4");
//				if(eventMoveList.contains(att.baseAttack.getUnLocalizedName()))
//				{
//					System.out.println("debug5");
//					player.sendMessage(new TextComponentString(TextFormatting.GREEN + "This wild " + pixelmon.getPokemonName() + " has the Event Move " + att.baseAttack.getUnLocalizedName()));
//				}
//			}
//		}
//	}
	
	@SubscribeEvent
	public void onPixelmonSpawn(EntityJoinWorldEvent event)
	{	
		if(event.getEntity() instanceof EntityPixelmon)
		{
			EntityPixelmon pixelmon = (EntityPixelmon) event.getEntity();
			if(!pixelmon.hasOwner())
			{
				BattleControllerBase battle = pixelmon.battleController;
				if(pixelmon.battleController != null)
				{
					if(battle.isInBattle(pixelmon.getPixelmonWrapper()))
					{
						return;
					}
				}
//				if(EnumMegaPokemon.getMega(pixelmon.getSpecies()) != null)
//				{
//					return;
//				}
//				if(pixelmon.spawner != null)
//				{
//					//test this pls, else just lower odds
//					return;
//				}
				if(RandomHelper.getRandomNumberBetween(0, 50) == 1)
				{
					ArrayList<Attack> tutorList = pixelmon.baseStats.getTutorMoves();
					ArrayList<Attack> eventList = new ArrayList<Attack>();
					
					for(Attack att : tutorList)
					{
						if(eventMoveList.contains(att.baseAttack.getUnLocalizedName()))
						{
							boolean cangetnormally = false;
							for(Attack lvlatt : pixelmon.baseStats.getMovesAtLevel(100))
							{
								if(lvlatt.baseAttack.getUnLocalizedName().equals(att.baseAttack.getUnLocalizedName()))
								{
									cangetnormally = true;
								}
							}
							for(Attack lvlatt : pixelmon.baseStats.getEggMoves())
							{
								if(lvlatt.baseAttack.getUnLocalizedName().equals(att.baseAttack.getUnLocalizedName()))
								{
									cangetnormally = true;
								}
							}
							if(!cangetnormally)
							{
								eventList.add(att);									
							}	
						}
					}
					
					if(eventList.isEmpty())
					{
						return;
					}
					
					Attack pickedMove = eventList.get(RandomHelper.getRandomNumberBetween(0, eventList.size()-1));
//					System.out.println(pickedMove.baseAttack.getUnLocalizedName() + " size: " + eventList.size());
					if(pixelmon.getGender() == Gender.Female)
					{
						if(pixelmon.baseStats.malePercent != 0)
						{
							pixelmon.setGender(Gender.Male);							
						}						
					}
					pixelmon.loadMoveset();
					pixelmon.getMoveset().set(0,pickedMove);
					pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });
				}

			}
		}
	}
	
}
