package com.varijon.tinies.MiscFeatures;

import java.util.Optional;

import com.pixelmonmod.pixelmon.enums.EnumMegaItem;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class MegaItemHandler 
{		
	@SubscribeEvent
	public void onPlayerInteract(PlayerInteractEvent.RightClickItem event)
	{
		EntityPlayerMP player = (EntityPlayerMP) event.getEntityPlayer();
		if(CheckForMegaItem(player.getHeldItem(EnumHand.MAIN_HAND)))
		{
			Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
			if (!optPlayerStorage.isPresent()) 
			{
		        return;
		    }
							
			PlayerStorage playerStorage = (PlayerStorage) optPlayerStorage.get();
			playerStorage.megaData.setMegaItem(EnumMegaItem.None, true);
			
//			for(EnumPokemon pokemon : EnumPokemon.values())
//			{
//				playerStorage.megaData.obtainedItem(pokemon, 0, player);
//				playerStorage.megaData.obtainedItem(pokemon, 1, player);
//			}

			playerStorage.megaData.obtainedItem(EnumPokemon.Aipom, 0, player);
			
			MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
			
			PixelmonStorage.pokeBallManager.savePlayer(server, playerStorage);	
			player.sendMessage(new TextComponentString(TextFormatting.GREEN + "Obtained a " + TextFormatting.RED + "Mega Bracelet" + TextFormatting.GREEN + "!"));				

			
			if(player.getHeldItem(EnumHand.MAIN_HAND).getCount() > 1)
			{
				player.getHeldItem(EnumHand.MAIN_HAND).setCount(player.getHeldItem(EnumHand.MAIN_HAND).getCount() - 1);
			}
			else
			{
				player.inventory.removeStackFromSlot(player.inventory.currentItem);
			}
			player.inventoryContainer.detectAndSendChanges();
		}
	}
	
	public boolean CheckForMegaItem(ItemStack item)
	{
		if(item != null)
		{
			NBTTagCompound nbt = item.getTagCompound();
			if(nbt != null)
			{
				if(nbt.hasKey("megaItem"))
				{
					return true;
				}
			}
		}
		return false;
	}
}
