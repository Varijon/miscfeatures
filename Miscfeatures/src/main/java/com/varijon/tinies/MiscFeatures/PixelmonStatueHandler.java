package com.varijon.tinies.MiscFeatures;

import java.util.UUID;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.api.events.StatueEvent;
import com.pixelmonmod.pixelmon.comm.packetHandlers.statueEditor.StatuePacketClient;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityStatue;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class PixelmonStatueHandler 
{
	public PixelmonStatueHandler()
	{
	}
	
	@SubscribeEvent
	public void onPlayerInteract(PlayerInteractEvent.RightClickBlock event)
	{
		if(event.getHand() != EnumHand.MAIN_HAND)
		{
			return;
		}
		EntityPlayerMP player = (EntityPlayerMP) event.getEntityPlayer();
		if(checkForStatueItem(player.getHeldItem(EnumHand.MAIN_HAND)))
		{	
			//figure out how to precisely place it, spawns in odd spot atm
			EntityStatue statue = new EntityStatue(event.getWorld());
			statue.init("Abra");
			UUID uuid = UUID.randomUUID();
			statue.setPokemonId(new int[]{(int)uuid.getMostSignificantBits(),(int)uuid.getLeastSignificantBits()});
			statue.setPosition((double)event.getPos().getX() + 0.5d,(double)event.getPos().getY() + 1,(double)event.getPos().getZ() + 0.5d);
			StatueEvent.CreateStatue createStatueEvent = new StatueEvent.CreateStatue(player, player.getServerWorld(), event.getPos(), statue);
			if (Pixelmon.EVENT_BUS.post((Event)createStatueEvent)) 
			{
                return;
            }
			statue = createStatueEvent.getStatue();
			event.getWorld().spawnEntity((Entity)statue);
			statue.setRotation(player.rotationYaw + 180.0f);
			Pixelmon.network.sendTo((IMessage) new StatuePacketClient(statue.getPokemonId()), player);
		}
	}
	public boolean checkForStatueItem(ItemStack item)
	{
		if(item != null)
		{
			NBTTagCompound nbt = item.getTagCompound();
			if(nbt != null)
			{
				if(nbt.hasKey("isStatueItem"))
				{
					return true;
				}
			}
		}
		return false;
	}
}
