package com.varijon.tinies.MiscFeatures;

import com.pixelmonmod.pixelmon.RandomHelper;
import com.pixelmonmod.pixelmon.config.PixelmonBlocks;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;

public class PokelootSpawnHandler 
{
	int counter = 0;
	MinecraftServer server;
	World world;
	
	int XMINPOS = 750;
	int XMAXPOS = 1900;
	int ZMINPOS = -250;
	int ZMAXPOS = 700;
	int attempts = 100;
	long lastSpawn = System.currentTimeMillis();
	
	public PokelootSpawnHandler()
	{
		server = FMLCommonHandler.instance().getMinecraftServerInstance();
	}
	
	@SubscribeEvent
	public void onWorldTick (WorldTickEvent event)
	{
		//try catch!
		//do a few attempts
		//odds for better chest
		if(event.phase != Phase.END)
		{
			return;
		}
		if(!event.world.getWorldInfo().getWorldName().equals("parkWorld"))
		{
			return;
		}
		if(counter == 20)
		{
			counter = 0;
			if(world == null)
			{
				world = DimensionManager.getWorld(2);
			}
			if(world == null)
			{
				return;
			}
			if((lastSpawn + (Main.lootSpawnSeconds * 1000)) - (System.currentTimeMillis()) <= 0)
			{
				lastSpawn = System.currentTimeMillis();
				for(int x = 0; x < attempts; x++)
				{
					int randomX = RandomHelper.getRandomNumberBetween(XMINPOS, XMAXPOS);
					int randomZ = RandomHelper.getRandomNumberBetween(ZMINPOS, ZMAXPOS);

					
					BlockPos lastAirPos = new BlockPos(randomX, 255, randomZ);
//					for(int y = 255; y > 1; y--)
//					{
//						if(world.isAirBlock(new BlockPos(randomX, y, randomZ)))
//						{
//							lastAirPos = new BlockPos(randomX, y, randomZ);
//						}
//						else
//						{
//							break;
//						}
//					}
					while(world.getBlockState(lastAirPos.add(0, -1, 0)).getBlock() == Blocks.AIR && lastAirPos.getY() >= 40)
					{
						lastAirPos = lastAirPos.add(0, -1, 0);
					}
					if (!checkAllowedBlock(world.getBlockState(lastAirPos.add(0, -1, 0)).getBlock()))
					{
						continue;
					}
					BlockPos pos = lastAirPos;
					if(pos != null)
					{
						int randomNum = RandomHelper.getRandomNumberBetween(0, 100);
						if(randomNum <= 10)
						{
							FMLLog.getLogger().info("Spawned MasterChest at: " + lastAirPos.getX() + " " + lastAirPos.getY() + " " + lastAirPos.getZ() );
							world.setBlockState(lastAirPos, PixelmonBlocks.masterChest.getDefaultState());
							break;							
						}
						if(randomNum <= 30)
						{
							FMLLog.getLogger().info("Spawned UltraChest at: " + lastAirPos.getX() + " " + lastAirPos.getY() + " " + lastAirPos.getZ() );
							world.setBlockState(lastAirPos, PixelmonBlocks.ultraChest.getDefaultState());
							break;							
						}
						if(randomNum <= 100)
						{
							FMLLog.getLogger().info("Spawned PokeChest at: " + lastAirPos.getX() + " " + lastAirPos.getY() + " " + lastAirPos.getZ() );
							world.setBlockState(lastAirPos, PixelmonBlocks.pokeChest.getDefaultState());	
							break;						
						}
					}
				}
			}
		}
		counter++;
	}
	private boolean checkAllowedBlock(Block target)
	{
		boolean allowed = true;
		
		if(target == Blocks.AIR || target == Blocks.TALLGRASS || target == Blocks.RED_FLOWER || target == Blocks.YELLOW_FLOWER
				|| target == Blocks.WATER || target == Blocks.LAVA || target == Blocks.FLOWING_WATER || target == Blocks.FLOWING_LAVA
				|| target == PixelmonBlocks.bolderBlock || target == Blocks.VINE || target == Blocks.SNOW_LAYER || target == Blocks.DOUBLE_PLANT
				|| target == Blocks.REEDS || target == Blocks.WATERLILY || target == Blocks.DEADBUSH)
		{
			allowed = false;
		}
		
		return allowed;
	}
}
