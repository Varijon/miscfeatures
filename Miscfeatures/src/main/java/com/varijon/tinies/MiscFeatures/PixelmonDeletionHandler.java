package com.varijon.tinies.MiscFeatures;

import com.pixelmonmod.pixelmon.config.PixelmonItemsTools;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.EntityInteract;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class PixelmonDeletionHandler 
{
	@SubscribeEvent
	public void onPlayerEntityInteract(EntityInteract event)
	{
		if(event.getTarget() instanceof EntityPixelmon)
		{
			if(event.getEntityPlayer().canUseCommand(4, "miscfeatures.pixelmondelete"))
			{
				if(event.getEntityPlayer().getHeldItem(EnumHand.MAIN_HAND) != null)
				{
					ItemStack item = event.getEntityPlayer().getHeldItem(EnumHand.MAIN_HAND);
					if(item.getItem() == PixelmonItemsTools.crystalSwordItem)
					{
						NBTTagCompound nbt = item.getTagCompound();
						if(nbt != null)
						{
							if(nbt.hasKey("kill"))
							{
								EntityPixelmon pixelmon = (EntityPixelmon) event.getTarget();
								pixelmon.setHealth(0);
							}
						}
					}
				}
			}
		}
	}
}
