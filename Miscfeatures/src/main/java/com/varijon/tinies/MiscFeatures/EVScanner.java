package com.varijon.tinies.MiscFeatures;

import java.util.Random;

import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.StatsType;
import com.pixelmonmod.pixelmon.storage.PlayerNotLoadedException;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.EntityInteract;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EVScanner 
{
	Random rng;
	
	public EVScanner()
	{
	}
	
	@SubscribeEvent
	public void onPlayerInteract(EntityInteract event) throws PlayerNotLoadedException
	{
		if(event.getTarget() instanceof EntityPixelmon)
		{
			if(event.getHand() != EnumHand.MAIN_HAND)
			{
				return;
			}
			EntityPixelmon pixelmon = (EntityPixelmon) event.getTarget();
			EntityPlayerMP player = (EntityPlayerMP) event.getEntityPlayer();
			
			
			if(pixelmon.belongsTo(player))
			{
				return;
			}
			
			if(player.getHeldItem(EnumHand.MAIN_HAND) != null)
			{
				ItemStack item = player.getHeldItem(EnumHand.MAIN_HAND);
				NBTTagCompound nbt = item.getTagCompound();
				if(nbt != null)
				{
					if(nbt.hasKey("isEVScanner"))
					{
						event.setCanceled(true);

						int pixelmonEVHP = pixelmon.baseStats.evYields.get(StatsType.HP) != null ? pixelmon.baseStats.evYields.get(StatsType.HP) : 0;
						int pixelmonEVAttack = pixelmon.baseStats.evYields.get(StatsType.Attack) != null ? pixelmon.baseStats.evYields.get(StatsType.Attack) : 0;
						int pixelmonEVDefense = pixelmon.baseStats.evYields.get(StatsType.Defence) != null ?  pixelmon.baseStats.evYields.get(StatsType.Defence) : 0;
						int pixelmonEVSpAtt = pixelmon.baseStats.evYields.get(StatsType.SpecialAttack) != null ? pixelmon.baseStats.evYields.get(StatsType.SpecialAttack) : 0;
						int pixelmonEVSpDef = pixelmon.baseStats.evYields.get(StatsType.SpecialDefence) != null ? pixelmon.baseStats.evYields.get(StatsType.SpecialDefence) : 0;
						int pixelmonEVSpeed = pixelmon.baseStats.evYields.get(StatsType.Speed) != null ? pixelmon.baseStats.evYields.get(StatsType.Speed) : 0;
						
						StringBuilder strBuild = new StringBuilder();
						strBuild.append(TextFormatting.GREEN + pixelmon.getPokemonName()+ " " + TextFormatting.GRAY + "EV Yield: ");
						
						if(pixelmonEVHP != 0)
						{
							strBuild.append(TextFormatting.LIGHT_PURPLE + "" + pixelmonEVHP + " HP");
						}
						if(pixelmonEVAttack != 0)
						{
							strBuild.append(TextFormatting.RED + "" + pixelmonEVAttack + " Attack ");
						}
						if(pixelmonEVDefense != 0)
						{
							strBuild.append(TextFormatting.GOLD + "" + pixelmonEVDefense + " Defense ");
						}
						if(pixelmonEVSpAtt != 0)
						{
							strBuild.append(TextFormatting.DARK_PURPLE + "" + pixelmonEVSpAtt + " Special Attack ");
						}
						if(pixelmonEVSpDef != 0)
						{
							strBuild.append(TextFormatting.YELLOW + "" + pixelmonEVSpDef + " Special Defense ");
						}
						if(pixelmonEVSpeed != 0)
						{
							strBuild.append(TextFormatting.AQUA + "" + pixelmonEVSpeed + " Speed ");
						}
						player.sendMessage(new TextComponentString(strBuild.toString()));
					}
				}
			}
		}
	}
}
