package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.Random;

import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;

import net.minecraft.init.Blocks;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class StopSuffocateHandler 
{
	@SubscribeEvent
	public void onLivingDamage(LivingHurtEvent event)
	{
		if(event.getEntity() instanceof EntityPixelmon)
		{
			EntityPixelmon pixelmon = (EntityPixelmon) event.getEntity();
			World w = pixelmon.world;
			
			if(pixelmon.hasOwner())
			{
				return;
			}
			
			ArrayList<BlockPos> possibleSpots = new ArrayList<BlockPos>();
			
			if(event.getSource() == DamageSource.IN_WALL)
			{
				for(int x = -1; x < 2; x++)
				{
					for(int z = -1; z < 2; z++)
					{
						for(int y = -1; y < 2; y++)
						{
							BlockPos loc = event.getEntity().getPosition().add(x, y, z);
							if(w.getBlockState(loc).getBlock().equals(Blocks.AIR) || w.getBlockState(loc).getBlock().equals(Blocks.WATER)|| w.getBlockState(loc).getBlock().equals(Blocks.FLOWING_WATER))
							{
								possibleSpots.add(loc);
							}
						}
					}
				}
				if(!possibleSpots.isEmpty())
				{
					Random rnd = new Random();
					BlockPos loc = possibleSpots.get(rnd.nextInt(possibleSpots.size()));
					pixelmon.setPosition(loc.getX(), loc.getY(), loc.getZ());
				}
				else
				{
					pixelmon.setPosition(pixelmon.getPosition().getX(), pixelmon.getPosition().getY()+1, pixelmon.getPosition().getZ());
				}
			}
		}
	}
}
