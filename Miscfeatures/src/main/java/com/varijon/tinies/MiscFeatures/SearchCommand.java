//package com.varijon.tinies.MiscFeatures;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import com.pixelmonmod.pixelmon.Pixelmon;
//import com.pixelmonmod.pixelmon.PixelmonMethods;
//import com.pixelmonmod.pixelmon.api.PixelmonApi;
//import com.pixelmonmod.pixelmon.battles.BattleRegistry;
//import com.pixelmonmod.pixelmon.battles.attacks.Attack;
//import com.pixelmonmod.pixelmon.battles.attacks.AttackBase;
//import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
//import com.pixelmonmod.pixelmon.battles.controller.participants.BattleParticipant;
//import com.pixelmonmod.pixelmon.blocks.PixelmonBlock;
//import com.pixelmonmod.pixelmon.blocks.ranch.RanchBounds;
//import com.pixelmonmod.pixelmon.comm.PixelmonData;
//import com.pixelmonmod.pixelmon.comm.PixelmonMovesetData;
//import com.pixelmonmod.pixelmon.comm.PixelmonPlayerTracker;
//import com.pixelmonmod.pixelmon.comm.packetHandlers.BossDropPacket;
//import com.pixelmonmod.pixelmon.comm.packetHandlers.SendExtraData;
//import com.pixelmonmod.pixelmon.comm.packetHandlers.npc.NPCLearnMove;
//import com.pixelmonmod.pixelmon.config.PixelmonBlocks;
//import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
//import com.pixelmonmod.pixelmon.config.PixelmonItems;
//import com.pixelmonmod.pixelmon.database.DatabaseMoves;
//import com.pixelmonmod.pixelmon.database.DatabaseStats;
//import com.pixelmonmod.pixelmon.entities.npcs.NPCShopkeeper;
//import com.pixelmonmod.pixelmon.entities.npcs.NPCTutor;
//import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopItem;
//import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopItemWithVariation;
//import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopkeeperData;
//import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
//import com.pixelmonmod.pixelmon.items.PixelmonItem;
//import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
//import com.pixelmonmod.pixelmon.storage.PlayerExtraData;
//import com.pixelmonmod.pixelmon.storage.PlayerNotLoadedException;
//import com.pixelmonmod.pixelmon.storage.PlayerStorage;
//import com.pixelmonmod.pixelmon.storage.PokeballManager;
//
//import net.minecraft.command.CommandException;
//import net.minecraft.command.ICommand;
//import net.minecraft.command.ICommandSender;
//import net.minecraft.command.server.CommandMessageRaw;
//import net.minecraft.entity.player.EntityPlayer;
//import net.minecraft.entity.player.EntityPlayerMP;
//import net.minecraft.inventory.Container;
//import net.minecraft.inventory.ContainerChest;
//import net.minecraft.inventory.IInventory;
//import net.minecraft.item.ItemAnvilBlock;
//import net.minecraft.item.ItemStack;
//import net.minecraft.nbt.NBTTagCompound;
//import net.minecraft.server.MinecraftServer;
//import net.minecraft.tileentity.TileEntityChest;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.util.text.TextComponentString;
//import net.minecraft.util.text.TextFormatting;
//import net.minecraft.util.IChatComponent;
//import net.minecraftforge.common.MinecraftForge;
//import net.minecraftforge.event.entity.EntityEvent.EnteringChunk;
//
//public class SearchCommand implements ICommand {
//
//	private List aliases;
//	public SearchCommand()
//	{
//	   this.aliases = new ArrayList();
//	   this.aliases.add("search");
//	}
//	
//	@Override
//	public int compareTo(ICommand arg0) {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	@Override
//	public String getCommandName() {
//		// TODO Auto-generated method stub
//		return "search";
//	}
//
//	@Override
//	public String getCommandUsage(ICommandSender sender) {
//		// TODO Auto-generated method stub
//		return "search [pokemon] [replaceslot]";
//	}
//
//	@Override
//	public List<String> getCommandAliases() {
//		// TODO Auto-generated method stub
//		return this.aliases;
//	}
//
//	@Override
//	public void processCommand(ICommandSender sender, String[] args) throws CommandException 
//	{
//		if(sender.canCommandSenderUseCommand(4, "miscfeatures.search"))
//		{
//			if(args.length == 0 || args.length > 2)
//			{
//				sender.addChatMessage(new TextComponentString(TextFormatting.RED + "Usage: /search [pokemon] [replaceslot]"));						
//				return;
//			}
//			if(sender instanceof EntityPlayerMP)
//			{
//				EntityPlayerMP player = (EntityPlayerMP) sender;
//				
//				try {
//					PlayerStorage ps = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
//					PlayerExtraData data = ps.getExtraData();
//					data.hasSash = true;
//					data.hasRainbowSash = true;
//					data.isDeveloper = false;
//					data.isSupport = true;
//					data.hasHalloween = true;
//					data.isAdmin = false;
//					data.setHatType(2);
//					
//					PixelmonStorage.save(player);
//					
//					Pixelmon.network.sendToAll(new SendExtraData(data));
//					
//				} catch (PlayerNotLoadedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				
//				//Pixelmon.network.sendTo(new BossDropPacket(false, new ItemStack[]{new ItemStack(PixelmonItems.aluminiumIngot,15),new ItemStack(PixelmonItems.aluminiumIngot)}),player);
////				IChatComponent message = IChatComponent.Serializer.jsonToComponent("[\"\",{\"text\":\"Test\",\"hoverEvent\":{\"action\":\"show_item\",\"value\":{id:\"pixelmon:PixelmonSprite\",Count:1b,tag:{display:{Name:\"Trapinch Photo\"},SpriteName:\"pixelmon:sprites/pokemon/328\"},Damage:0s}}}]");
//	//			player.addChatMessage(message);
//
//			}
//			return;
//		}
//		else
//		{
//			sender.addChatMessage(new TextComponentString(TextFormatting.RED + "You don't have permission to use this command"));
//			return;
//		}
//
//	}
//
//	@Override
//	public boolean canCommandSenderUseCommand(ICommandSender sender) {
//		// TODO Auto-generated method stub
//		return true;
//	}
//
//	@Override
//	public List<String> addTabCompletionOptions(ICommandSender sender,
//			String[] args, BlockPos pos) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public boolean isUsernameIndex(String[] args, int index) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//	
////	private String buildTellRaw()
////	{
////		StringBuilder sb = new StringBuilder();
////		
////	}
//	
//}
