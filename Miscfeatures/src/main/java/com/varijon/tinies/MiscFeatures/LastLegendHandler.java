package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;

import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;

import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class LastLegendHandler 
{
	public static ArrayList<LegendSpawn> listLegendSpawns;
	
	public LastLegendHandler()
	{
		listLegendSpawns = new ArrayList<LegendSpawn>();
	}
	
	@SubscribeEvent
	public void onPixelmonSpawn(EntityJoinWorldEvent event)
	{	
		if(event.getEntity() instanceof EntityPixelmon)
		{
			EntityPixelmon pixelmon = (EntityPixelmon) event.getEntity();
			if(!pixelmon.hasOwner() && EnumPokemon.legendaries.contains(pixelmon.getPokemonName()))
			{
				if(!CheckIfNameExists(pixelmon.getPokemonName()))
				{
					pixelmon.getEntityWorld().getBiome(pixelmon.getPosition());
					listLegendSpawns.add(new LegendSpawn(System.currentTimeMillis(),pixelmon.getPokemonName(), pixelmon.getUniqueID().toString()));
				}
				else
				{
					if(pixelmon.battleController != null)
					{
						BattleControllerBase battle = pixelmon.battleController;
						if(battle.isInBattle(pixelmon.getPixelmonWrapper()))
						{
							return;
						}
					}
					GetNameMatch(pixelmon.getPokemonName()).setTimeSpawned(System.currentTimeMillis());	
				}
			}
		}
	}
	
	private boolean CheckIfNameExists(String pokemonName)
	{
		for(LegendSpawn legend : listLegendSpawns)
		{
			if(legend.pokemonName.equalsIgnoreCase(pokemonName))
			{
				return true;
			}
		}
		return false;
	}
	private boolean CheckIfSameUUID(String uuid)
	{
		for(LegendSpawn legend : listLegendSpawns)
		{
			if(legend.UUID.equalsIgnoreCase(uuid))
			{
				return true;
			}
		}
		return false;
	}
	
	private LegendSpawn GetNameMatch(String pokemonName)
	{
		for(LegendSpawn legend : listLegendSpawns)
		{
			if(legend.pokemonName.equalsIgnoreCase(pokemonName))
			{
				return legend;
			}
		}
		return null;
	}
}
