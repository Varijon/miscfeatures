package com.varijon.tinies.MiscFeatures;

import java.util.Optional;

import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.SPacketDisplayObjective;
import net.minecraft.network.play.server.SPacketScoreboardObjective;
import net.minecraft.network.play.server.SPacketUpdateScore;
import net.minecraft.scoreboard.Score;
import net.minecraft.scoreboard.ScoreCriteria;
import net.minecraft.scoreboard.ScoreObjective;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;

public class InfoBoardHandler 
{
	int counter = 0;
	MinecraftServer server;
	
	public InfoBoardHandler()
	{
		server = FMLCommonHandler.instance().getMinecraftServerInstance();
	}
	

	@SubscribeEvent
	public void onPlayerJoin (PlayerLoggedInEvent event)
	{
		EntityPlayerMP targetPlayer = (EntityPlayerMP) event.player;
		if(InfoBoardCommand.userBoards.containsKey(targetPlayer.getUniqueID().toString()))
		{
			sendPlayerObjectives(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), targetPlayer);
		}
	}
	
	@SubscribeEvent
	public void onWorldTick (WorldTickEvent event)
	{
		if(event.phase != Phase.END)
		{
			return;
		}
		if(!event.world.getWorldInfo().getWorldName().equals("world"))
		{
			return;
		}
		if(counter == 20)
		{
			for(EntityPlayerMP targetPlayer : server.getPlayerList().getPlayers())
			{
				if(InfoBoardCommand.userModes.containsKey((targetPlayer.getUniqueID().toString())))
				{
					if(!InfoBoardCommand.userBoards.containsKey(targetPlayer.getUniqueID().toString()))
					{
						InfoBoardCommand.userBoards.put(targetPlayer.getUniqueID().toString(), new Scoreboard());
						createEVObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), 1);
						createEVObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), 2);
						createEVObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), 3);
						createEVObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), 4);
						createEVObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), 5);
						createEVObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), 6);
						createEggObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()));
						sendPlayerObjectives(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), targetPlayer);
					}
					setDisplayedObjective(targetPlayer, InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()));
					String type = InfoBoardCommand.userModes.get(targetPlayer.getUniqueID().toString());
					if(type.equals("evs1"))
					{
						updateEVObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), targetPlayer, 1);
					}
					if(type.equals("evs2"))
					{
						updateEVObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), targetPlayer, 2);
					}
					if(type.equals("evs3"))
					{
						updateEVObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), targetPlayer, 3);
					}
					if(type.equals("evs4"))
					{
						updateEVObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), targetPlayer, 4);
					}
					if(type.equals("evs5"))
					{
						updateEVObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), targetPlayer, 5);
					}
					if(type.equals("evs6"))
					{
						updateEVObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), targetPlayer, 6);
					}
					if(type.equals("egg"))
					{
						updateEggObjective(InfoBoardCommand.userBoards.get(targetPlayer.getUniqueID().toString()), targetPlayer);
					}
				}
			}
			counter = 0;
			return;
		}
		counter++;
	}
	
	public void updateEggObjective(Scoreboard scoreboard, EntityPlayerMP player)
	{
		Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
		if (!optPlayerStorage.isPresent()) 
		{
	        return;
	    }
		PlayerStorage playerStorage = (PlayerStorage) optPlayerStorage.get();
				
		int partyCount = playerStorage.count();
		
		for(int x = 0; x < 6; x++)
		{
			EntityPixelmon pixelmon = playerStorage.getPokemon(playerStorage.getIDFromPosition(x), player.getEntityWorld());
			if (pixelmon == null)
			{
				Score scoreEggSteps = scoreboard.getOrCreateScore(TextFormatting.YELLOW + "#" + (x+1), scoreboard.getObjective("Egg Progress"));
				scoreEggSteps.setScorePoints(-1);
				player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreEggSteps));

				continue;
			}
			if(!pixelmon.isEgg)
			{
				Score scoreEggSteps = scoreboard.getOrCreateScore(TextFormatting.YELLOW + "#" + (x+1), scoreboard.getObjective("Egg Progress"));
				scoreEggSteps.setScorePoints(-1);
				player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreEggSteps));
				
				continue;
			}
			int pixelmonEggSteps = (int) (100.0 - ((double)pixelmon.eggCycles / pixelmon.baseStats.eggCycles * 100.0));
			Score scoreEggSteps = scoreboard.getOrCreateScore(TextFormatting.YELLOW + "#" + (x+1), scoreboard.getObjective("Egg Progress"));
			scoreEggSteps.setScorePoints(pixelmonEggSteps);
			
			player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreEggSteps));
		}
	}
	
	public void updateEVObjective(Scoreboard scoreboard, EntityPlayerMP player, int partySlot)
	{
		Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
		if (!optPlayerStorage.isPresent()) 
		{
	        return;
	    }
		PlayerStorage playerStorage = (PlayerStorage) optPlayerStorage.get();
		
		int partyNumber = partySlot - 1;
		
		EntityPixelmon pixelmon = playerStorage.getPokemon(playerStorage.getIDFromPosition(partyNumber), player.getEntityWorld());

		int pixelmonEVHP = 0;
		int pixelmonEVAttack = 0;
		int pixelmonEVDefense = 0;
		int pixelmonEVSpAtt = 0;
		int pixelmonEVSpDef = 0;
		int pixelmonEVSpeed = 0;
		if (pixelmon != null)
		{
			pixelmonEVHP = pixelmon.stats.evs.hp;
			pixelmonEVAttack = pixelmon.stats.evs.attack;
			pixelmonEVDefense = pixelmon.stats.evs.defence;
			pixelmonEVSpAtt = pixelmon.stats.evs.specialAttack;
			pixelmonEVSpDef = pixelmon.stats.evs.specialDefence;
			pixelmonEVSpeed = pixelmon.stats.evs.speed;
		}

		Score evHP = scoreboard.getOrCreateScore(TextFormatting.LIGHT_PURPLE + "HP", scoreboard.getObjective("Slot " + partySlot + " EV"));
		evHP.setScorePoints(pixelmonEVHP);
		Score evAtt = scoreboard.getOrCreateScore(TextFormatting.RED + "Attack", scoreboard.getObjective("Slot " + partySlot + " EV"));
		evAtt.setScorePoints(pixelmonEVAttack);
		Score evDef = scoreboard.getOrCreateScore(TextFormatting.GOLD + "Defense", scoreboard.getObjective("Slot " + partySlot + " EV"));
		evDef.setScorePoints(pixelmonEVDefense);
		Score evSpatt = scoreboard.getOrCreateScore(TextFormatting.DARK_PURPLE + "Sp. Attack", scoreboard.getObjective("Slot " + partySlot + " EV"));
		evSpatt.setScorePoints(pixelmonEVSpAtt);
		Score evSpdef = scoreboard.getOrCreateScore(TextFormatting.YELLOW + "Sp. Defense", scoreboard.getObjective("Slot " + partySlot + " EV"));
		evSpdef.setScorePoints(pixelmonEVSpDef);
		Score evSpeed = scoreboard.getOrCreateScore(TextFormatting.AQUA + "Speed", scoreboard.getObjective("Slot " + partySlot + " EV"));
		evSpeed.setScorePoints(pixelmonEVSpeed);
		
		player.connection.netManager.sendPacket(new SPacketUpdateScore(evHP));
		player.connection.netManager.sendPacket(new SPacketUpdateScore(evAtt));
		player.connection.netManager.sendPacket(new SPacketUpdateScore(evDef));
		player.connection.netManager.sendPacket(new SPacketUpdateScore(evSpatt));
		player.connection.netManager.sendPacket(new SPacketUpdateScore(evSpdef));
		player.connection.netManager.sendPacket(new SPacketUpdateScore(evSpeed));
	}
	
	public void createEVObjective(Scoreboard scoreboard, int partySlot)
	{
		scoreboard.addScoreObjective("Slot " + partySlot +  " EV", ScoreCriteria.DUMMY);			
	}
	public void createEggObjective(Scoreboard scoreboard)
	{
		scoreboard.addScoreObjective("Egg Progress", ScoreCriteria.DUMMY);			
	}
	public void sendPlayerObjectives(Scoreboard scoreboard, EntityPlayerMP player)
	{
		for(ScoreObjective objective : scoreboard.getScoreObjectives())
		{
			player.connection.netManager.sendPacket(new SPacketScoreboardObjective(objective, 0));
		}
	}
	public void setDisplayedObjective(EntityPlayerMP player, Scoreboard scoreboard)
	{
		String type = InfoBoardCommand.userModes.get(player.getUniqueID().toString());
		if(type.equals("evs1"))
		{
			player.connection.netManager.sendPacket(new SPacketDisplayObjective(1, scoreboard.getObjective("Slot 1 EV")));
		}
		if(type.equals("evs2"))
		{
			player.connection.netManager.sendPacket(new SPacketDisplayObjective(1, scoreboard.getObjective("Slot 2 EV")));
		}
		if(type.equals("evs3"))
		{
			player.connection.netManager.sendPacket(new SPacketDisplayObjective(1, scoreboard.getObjective("Slot 3 EV")));
		}
		if(type.equals("evs4"))
		{
			player.connection.netManager.sendPacket(new SPacketDisplayObjective(1, scoreboard.getObjective("Slot 4 EV")));
		}
		if(type.equals("evs5"))
		{
			player.connection.netManager.sendPacket(new SPacketDisplayObjective(1, scoreboard.getObjective("Slot 5 EV")));
		}
		if(type.equals("evs6"))
		{
			player.connection.netManager.sendPacket(new SPacketDisplayObjective(1, scoreboard.getObjective("Slot 6 EV")));
		}
		if(type.equals("egg"))
		{
			player.connection.netManager.sendPacket(new SPacketDisplayObjective(1, scoreboard.getObjective("Egg Progress")));
		}	
		if(type.equals("off"))
		{
			player.connection.netManager.sendPacket(new SPacketDisplayObjective(1, null));
		}	
	}
	
//	public void addScoreBoardToPlayer(EntityPlayerMP player, String type)
//	{
//		if(type.equals("off"))
//		{
//			Scoreboard testBoard = new Scoreboard();
//			ScoreObjective scoreObjective = new ScoreObjective(testBoard, "", ScoreCriteria.DUMMY);
//			player.connection.netManager.sendPacket(new SPacketScoreboardObjective(scoreObjective, 1));
//			player.connection.netManager.sendPacket(new SPacketDisplayObjective(1, scoreObjective));
//			InfoBoardCommand.userModes.remove(player.getUniqueID().toString());
//			return;
//		}
//		if(type.equals("evs1"))
//		{
//			setEVScoreBoard(player, 1);
//		}
//		if(type.equals("evs2"))
//		{
//			setEVScoreBoard(player, 2);
//		}
//		if(type.equals("evs3"))
//		{
//			setEVScoreBoard(player, 3);
//		}
//		if(type.equals("evs4"))
//		{
//			setEVScoreBoard(player, 4);
//		}
//		if(type.equals("evs5"))
//		{
//			setEVScoreBoard(player, 5);
//		}
//		if(type.equals("evs6"))
//		{
//			setEVScoreBoard(player, 6);
//		}
//		if(type.equals("egg"))
//		{
//			setEggScoreboard(player);
//		}
//	}
//	
//	public void setEVScoreBoard(EntityPlayerMP player, int partySlot)
//	{		
//		Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
//		if (!optPlayerStorage.isPresent()) 
//		{
//	        return;
//	    }
//		PlayerStorage playerStorage = (PlayerStorage) optPlayerStorage.get();
//		
//		int partyNumber = partySlot - 1;
//		
//		EntityPixelmon pixelmon = playerStorage.getPokemon(playerStorage.getIDFromPosition(partyNumber), player.getEntityWorld());
//		if (pixelmon == null)
//		{
//			Scoreboard testBoard = new Scoreboard();		
//			
//			ScoreObjective scoreObjective = new ScoreObjective(testBoard, "Empty Party Slot", ScoreCriteria.DUMMY);
//			Score scoreHP = new Score(testBoard, scoreObjective, TextFormatting.LIGHT_PURPLE + "HP");
//			scoreHP.setScorePoints(-1);
//			Score scoreAtt = new Score(testBoard, scoreObjective, TextFormatting.RED + "Attack");
//			scoreAtt.setScorePoints(-1);
//			Score scoreDef = new Score(testBoard, scoreObjective, TextFormatting.GOLD + "Defense");
//			scoreDef.setScorePoints(-1);
//			Score scoreSpatt = new Score(testBoard, scoreObjective, TextFormatting.DARK_PURPLE + "Sp. Attack");
//			scoreSpatt.setScorePoints(-1);
//			Score scoreSpdef = new Score(testBoard, scoreObjective, TextFormatting.YELLOW + "Sp. Defense");
//			scoreSpdef.setScorePoints(-1);
//			Score scoreSpeed = new Score(testBoard, scoreObjective, TextFormatting.AQUA + "Speed");
//			scoreSpeed.setScorePoints(-1);
//			
//			player.connection.netManager.sendPacket(new SPacketScoreboardObjective(scoreObjective, 0));
//			player.connection.netManager.sendPacket(new SPacketDisplayObjective(1, scoreObjective));
//			player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreHP));
//			player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreAtt));
//			player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreDef));
//			player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreSpatt));
//			player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreSpdef));
//			player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreSpeed));
//			return;
//		}
//		int pixelmonEVHP = pixelmon.stats.EVs.HP;
//		int pixelmonEVAttack = pixelmon.stats.EVs.Attack;
//		int pixelmonEVDefense = pixelmon.stats.EVs.Defence;
//		int pixelmonEVSpAtt = pixelmon.stats.EVs.SpecialAttack;
//		int pixelmonEVSpDef = pixelmon.stats.EVs.SpecialDefence;
//		int pixelmonEVSpeed = pixelmon.stats.EVs.Speed;
//		
//		Scoreboard testBoard = new Scoreboard();		
//		
//		ScoreObjective scoreObjective = new ScoreObjective(testBoard, pixelmon.getPokemonName() + " EV", ScoreCriteria.DUMMY);
//		Score scoreHP = new Score(testBoard, scoreObjective, TextFormatting.LIGHT_PURPLE + "HP");
//		scoreHP.setScorePoints(pixelmonEVHP);
//		Score scoreAtt = new Score(testBoard, scoreObjective, TextFormatting.RED + "Attack");
//		scoreAtt.setScorePoints(pixelmonEVAttack);
//		Score scoreDef = new Score(testBoard, scoreObjective, TextFormatting.GOLD + "Defense");
//		scoreDef.setScorePoints(pixelmonEVDefense);
//		Score scoreSpatt = new Score(testBoard, scoreObjective, TextFormatting.DARK_PURPLE + "Sp. Attack");
//		scoreSpatt.setScorePoints(pixelmonEVSpAtt);
//		Score scoreSpdef = new Score(testBoard, scoreObjective, TextFormatting.YELLOW + "Sp. Defense");
//		scoreSpdef.setScorePoints(pixelmonEVSpDef);
//		Score scoreSpeed = new Score(testBoard, scoreObjective, TextFormatting.AQUA + "Speed");
//		scoreSpeed.setScorePoints(pixelmonEVSpeed);
//		
//		player.connection.netManager.sendPacket(new SPacketScoreboardObjective(scoreObjective, 0));
//		player.connection.netManager.sendPacket(new SPacketDisplayObjective(1, scoreObjective));
//		player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreHP));
//		player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreAtt));
//		player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreDef));
//		player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreSpatt));
//		player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreSpdef));
//		player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreSpeed));
//	}
//	
//	public void setEggScoreboard(EntityPlayerMP player)
//	{		
//		Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
//		if (!optPlayerStorage.isPresent()) 
//		{
//	        return;
//	    }
//		PlayerStorage playerStorage = (PlayerStorage) optPlayerStorage.get();
//		
//		int partyCount = playerStorage.count();
//		Scoreboard testBoard = new Scoreboard();
//		ScoreObjective scoreObjective = new ScoreObjective(testBoard, "Egg Progress", ScoreCriteria.DUMMY);
//		
//		for(int x = 0; x < 6; x++)
//		{
//			EntityPixelmon pixelmon = playerStorage.getPokemon(playerStorage.getIDFromPosition(x), player.getEntityWorld());
//			if (pixelmon == null)
//			{
//				Score scoreEggSteps = new Score(testBoard, scoreObjective, TextFormatting.YELLOW + "#" + (x+1));
//				scoreEggSteps.setScorePoints(-1);
//				
//				player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreEggSteps));
//				continue;
//			}
//			if(!pixelmon.isEgg)
//			{
//				Score scoreEggSteps = new Score(testBoard, scoreObjective, TextFormatting.YELLOW + "#" + (x+1));
//				scoreEggSteps.setScorePoints(-1);
//				
//				player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreEggSteps));
//				continue;
//			}
//			int pixelmonEggSteps = (int) (100.0 - ((double)pixelmon.eggCycles / pixelmon.baseStats.eggCycles * 100.0));
//			Score scoreEggSteps = new Score(testBoard, scoreObjective, TextFormatting.YELLOW + "#" + (x+1));
//			scoreEggSteps.setScorePoints(pixelmonEggSteps);
//			
//			player.connection.netManager.sendPacket(new SPacketUpdateScore(scoreEggSteps));
//		}
//		player.connection.netManager.sendPacket(new SPacketScoreboardObjective(scoreObjective, 0));
//		player.connection.netManager.sendPacket(new SPacketDisplayObjective(1, scoreObjective));
//		
//	}
}
