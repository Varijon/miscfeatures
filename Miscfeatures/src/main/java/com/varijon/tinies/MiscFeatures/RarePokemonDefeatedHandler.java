package com.varijon.tinies.MiscFeatures;

import com.pixelmonmod.pixelmon.api.events.BeatWildPixelmonEvent;
import com.pixelmonmod.pixelmon.config.PixelmonItems;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.enums.EnumBossMode;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class RarePokemonDefeatedHandler 
{
	@SubscribeEvent
	public void onPixelmonDelete(BeatWildPixelmonEvent event)
	{
		EntityPixelmon pixelmon = event.wpp.controlledPokemon.get(0).pokemon;
		MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
		
		
		if(EnumPokemon.legendaries.contains(pixelmon.getPokemonName()))
		{
			if(pixelmon.getBossMode() == EnumBossMode.NotBoss)
			{
				server.getPlayerList().sendMessage(new TextComponentString(TextFormatting.WHITE + "[" + TextFormatting.RED + "Oh No" + TextFormatting.WHITE + "] " + TextFormatting.LIGHT_PURPLE + event.player.getName() + TextFormatting.GRAY + " has defeated a " + TextFormatting.AQUA + pixelmon.getPokemonName() + TextFormatting.GRAY + "!"));
			}
			else
			{
				server.getPlayerList().sendMessage(new TextComponentString(TextFormatting.WHITE + "[" + TextFormatting.RED + "Oh No" + TextFormatting.WHITE + "] " + TextFormatting.LIGHT_PURPLE + event.player.getName() + TextFormatting.GRAY + " was forced to defeat a " + TextFormatting.RED + "Boss " + TextFormatting.AQUA + pixelmon.getPokemonName() + TextFormatting.GRAY + "!"));
			}
		}
		if(pixelmon.getIsShiny() && !pixelmon.isMega)
		{
			if(pixelmon.getBossMode() == EnumBossMode.NotBoss)
			{			
				server.getPlayerList().sendMessage(new TextComponentString(TextFormatting.WHITE + "[" + TextFormatting.RED + "Oh No" + TextFormatting.WHITE + "] " + TextFormatting.LIGHT_PURPLE + event.player.getName() + TextFormatting.GRAY + " has defeated a " + TextFormatting.GOLD + "Shiny " + TextFormatting.AQUA + pixelmon.getPokemonName() + TextFormatting.GRAY + "!"));			
			}
			else
			{
				server.getPlayerList().sendMessage(new TextComponentString(TextFormatting.WHITE + "[" + TextFormatting.RED + "Oh No" + TextFormatting.WHITE + "] " + TextFormatting.LIGHT_PURPLE + event.player.getName() + TextFormatting.GRAY + " was forced to defeat a " + TextFormatting.GOLD + "Shiny " + TextFormatting.RED + "Boss " + TextFormatting.AQUA + pixelmon.getPokemonName() + TextFormatting.GRAY + "!"));	
			}
			ItemStack finalItem = new ItemStack(PixelmonItems.itemPixelmonSprite);
			finalItem.setTagCompound(new NBTTagCompound());
								
			NBTTagCompound tags = finalItem.getTagCompound();
			tags.setString("SpriteName", "pixelmon:sprites/shinypokemon/star");
			tags.setTag("display", new NBTTagCompound());
			tags.getCompoundTag("display").setString("Name", TextFormatting.GOLD + "Shiny Star");					

			NBTTagList loreList = new NBTTagList();
			
			if(pixelmon.getBossMode() == EnumBossMode.NotBoss)
			{
				loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Belonged to a " + TextFormatting.GOLD + "Shiny " + TextFormatting.LIGHT_PURPLE + pixelmon.getPokemonName()));
			}
			else
			{
				loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Belonged to a " + TextFormatting.GOLD + "Shiny " + TextFormatting.RED + "Boss " + TextFormatting.LIGHT_PURPLE + pixelmon.getPokemonName()));
				loreList.appendTag(new NBTTagString(TextFormatting.GOLD + ";-;"));
			}
			tags.getCompoundTag("display").setTag("Lore", loreList);
			
			finalItem.setTagCompound(tags);
			
			if(!event.player.inventory.addItemStackToInventory(finalItem))
			{
				World w = event.player.getEntityWorld();
				w.spawnEntity(new EntityItem(w, event.player.lastTickPosX, event.player.lastTickPosY, event.player.lastTickPosZ, finalItem));
			}
		}
		
	}
//	@SubscribeEvent
//	public void onPixelmonDelete(PixelmonDeletedEvent event)
//	{
//		EntityPixelmon pixelmon = (EntityPixelmon) PixelmonEntityList.createEntityFromNBT(event.pokemon, event.player.worldObj);
//		if(pixelmon.getIsShiny())
//		{
//			if(pixelmon.isEgg)
//			{			
//				MinecraftServer.getServer().getConfigurationManager().sendChatMsg(new TextComponentString(TextFormatting.WHITE + "[" + TextFormatting.RED + "Oh No" + TextFormatting.WHITE + "] " + TextFormatting.LIGHT_PURPLE + event.player.getName() + TextFormatting.GRAY + " has released a " + TextFormatting.GOLD + "SHINY " + TextFormatting.LIGHT_PURPLE + pixelmon.getPokemonName() + " Egg" + TextFormatting.GRAY + "!"));			
//			}
//			else
//			{				
//				MinecraftServer.getServer().getConfigurationManager().sendChatMsg(new TextComponentString(TextFormatting.WHITE + "[" + TextFormatting.RED + "Oh No" + TextFormatting.WHITE + "] " + TextFormatting.LIGHT_PURPLE + event.player.getName() + TextFormatting.GRAY + " has released a " + TextFormatting.GOLD + "SHINY " + TextFormatting.LIGHT_PURPLE + pixelmon.getPokemonName() + TextFormatting.GRAY + "!"));					
//			}
//			ItemStack finalItem = new ItemStack(PixelmonItems.itemPixelmonSprite);
//			finalItem.setTagCompound(new NBTTagCompound());
//								
//			NBTTagCompound tags = finalItem.getTagCompound();
//			tags.setString("SpriteName", "pixelmon:sprites/shinypokemon/star");
//			tags.setTag("display", new NBTTagCompound());
//			tags.getCompoundTag("display").setString("Name", TextFormatting.GOLD + "Shiny Star");					
//			
//			NBTTagList loreList = new NBTTagList();
//			loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Belonged to a " + TextFormatting.GOLD + "SHINY " + TextFormatting.LIGHT_PURPLE + pixelmon.getPokemonName()));
//			tags.getCompoundTag("display").setTag("Lore", loreList);
//			
//			finalItem.setTagCompound(tags);
//			
//			if(!event.player.inventory.addItemStackToInventory(finalItem))
//			{
//				World w = event.player.getEntityWorld();
//				w.spawnEntityInWorld(new EntityItem(w, event.player.lastTickPosX, event.player.lastTickPosY, event.player.lastTickPosZ, finalItem));
//			}
//			return;
//		}
//	}
}
