package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.List;

import com.pixelmonmod.pixelmon.config.PixelmonItems;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class LootBoxCommand implements ICommand {

	private List aliases;
	public LootBoxCommand()
	{
	   this.aliases = new ArrayList();
	   this.aliases.add("lb");
	   this.aliases.add("lootbox");
	}
	
	@Override
	public int compareTo(ICommand arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "lootbox";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		// TODO Auto-generated method stub
		return "lootbox reload|add|get";
	}

	@Override
	public List<String> getAliases() {
		// TODO Auto-generated method stub
		return this.aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException 
	{
		if(sender.canUseCommand(4, "miscfeatures.lootbox"))
		{
			if(args.length == 0)
			{
				sender.sendMessage(new TextComponentString(TextFormatting.RED + "/lootbox reload|add|get"));						
				return;				
			}
			if(args[0].equals("reload"))
			{
				if(args.length > 1)
				{
					sender.sendMessage(new TextComponentString(TextFormatting.RED + "Too many arguments"));						
					return;
				}
				try {
					LootBoxConfigManager.loadConfig();
					sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "LootBox config reloaded"));	
				} catch (NBTException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return;
			}
			if(sender instanceof EntityPlayer)
			{
				EntityPlayer player = (EntityPlayer) sender;
				if(args[0].equals("add"))
				{
					if(args.length != 4)
					{
						sender.sendMessage(new TextComponentString(TextFormatting.RED + "Usage: /lb add lootboxname quantity rarity(0.0-1.0)"));						
						return;
					}
					LootBoxConfigManager.WriteNewItem(player.getHeldItem(EnumHand.MAIN_HAND).writeToNBT(new NBTTagCompound()), args[2], args[3], args[1]);
					sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Item added"));
					return;	
				}
				if(args[0].equals("get"))
				{
					if(args.length == 1)
					{
						sender.sendMessage(new TextComponentString(TextFormatting.GOLD + "Available LootBoxes:"));	
						for(LootBox lootbox : LootBoxConfigManager.lst_LootBoxes)
						{
							sender.sendMessage(new TextComponentString(TextFormatting.GREEN + lootbox.GetIdentifier()));							
						}					
						return;
					}
					if(args.length > 2)
					{
						sender.sendMessage(new TextComponentString(TextFormatting.RED + "Too many arguments"));						
						return;
					}
					for(LootBox lootbox : LootBoxConfigManager.lst_LootBoxes)
					{
						if(args[1].equals(lootbox.GetIdentifier()))
						{
							ItemStack finalItem = new ItemStack(PixelmonItems.gift); //new ItemStack(PixelUtilitiesAdditions.boxBlock);
							finalItem.setTagCompound(new NBTTagCompound());
												
							NBTTagCompound tags = finalItem.getTagCompound();
							//tags.setString("SpriteName", "pixelmon:items/gift");
							tags.setTag("display", new NBTTagCompound());
							tags.getCompoundTag("display").setString("Name", TextFormatting.GOLD + lootbox.GetIdentifier());
							tags.setString("identifier", lootbox.GetIdentifier());						
							
							NBTTagList loreList = new NBTTagList();
							loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Rightclick to open"));
							tags.getCompoundTag("display").setTag("Lore", loreList);
							
							finalItem.setTagCompound(tags);
							
							if(!player.inventory.addItemStackToInventory(finalItem))
							{
								World w = player.getEntityWorld();
								EntityItem item = player.entityDropItem(finalItem, 1);
								item.setOwner(player.getName());
								

//								for (Entity ent : w.loadedEntityList)
//								{
//									if(ent instanceof EntityItem)
//									{
//										EntityItem itemz = (EntityItem) ent;
//										NBTTagCompound nbt = new NBTTagCompound();
//										itemz.writeToNBT(nbt);
//										MinecraftServer.getServer().getConfigurationManager().sendChatMsg(new TextComponentString(nbt.toString()));
//									}			
//								}
								//w.spawnEntityInWorld(new EntityItem(w, player.lastTickPosX, player.lastTickPosY, player.lastTickPosZ, finalItem));
								
							}
							sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Got lootbox: " + lootbox.GetIdentifier()));	
							return;
						}
					}
					sender.sendMessage(new TextComponentString(TextFormatting.RED + "LootBox not found"));	
	
					return;
				}
			}
		}
		else
		{
			sender.sendMessage(new TextComponentString(TextFormatting.RED + "You don't have permission to use this command"));
			return;
		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender) 
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index) {
		// TODO Auto-generated method stub
		return false;
	}

}
