package com.varijon.tinies.MiscFeatures;

import java.util.HashMap;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;

public class AfkHandler 
{
	int counter = 0;
	MinecraftServer server;

	HashMap<String, AfkPlayer> userList;
	
	public AfkHandler()
	{
		server = FMLCommonHandler.instance().getMinecraftServerInstance();
		userList = new HashMap<String, AfkPlayer>();
	}
	
	@SubscribeEvent
	public void onPlayerJoin (PlayerLoggedInEvent event)
	{
		EntityPlayerMP targetPlayer = (EntityPlayerMP) event.player;
		userList.put(targetPlayer.getUniqueID().toString(), new AfkPlayer(targetPlayer.rotationYaw, targetPlayer.rotationPitch, System.currentTimeMillis()));
	}
	
	@SubscribeEvent
	public void onWorldTick (WorldTickEvent event)
	{
		if(counter == 100)
		{
			if(Main.afkKick == 0)
			{
				return;
			}
			for(EntityPlayerMP player : server.getPlayerList().getPlayers())
			{
				AfkPlayer afkplayer = userList.get(player.getUniqueID().toString());
				if(afkplayer != null)
				{
					if(afkplayer.getLastYaw() != player.rotationYaw && afkplayer.getLastPitch() != player.rotationPitch)
					{
						afkplayer.setLastPitch(player.rotationPitch);
						afkplayer.setLastYaw(player.rotationYaw);
						afkplayer.lastHeadMovement = System.currentTimeMillis();
						continue;
					}
					if(((afkplayer.getLastHeadMovement() / 1000)+ Main.afkKick) - (System.currentTimeMillis()/1000) <= 0)
					{
						if(!player.canUseCommand(4, "miscfeatures.afkexempt"))
						{
							String modMessage = Main.afkKickMessage.replaceAll("&", "\u00A7");
							player.connection.disconnect(new TextComponentString(modMessage));
						}
					}
				}
			}
			counter = 0;
			return;
		}
		counter++;
	}
}
