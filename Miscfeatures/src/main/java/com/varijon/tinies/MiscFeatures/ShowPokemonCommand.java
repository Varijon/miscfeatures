package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

public class ShowPokemonCommand implements ICommand {

	private List aliases;

	public HashMap<String, Long> pokemonShowCooldown = new HashMap<String, Long>();
	
	public ShowPokemonCommand()
	{
	   this.aliases = new ArrayList();
	   this.aliases.add("showpokemon");
	   this.aliases.add("brag");
	}
	
	@Override
	public int compareTo(ICommand arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "showpokemon";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		// TODO Auto-generated method stub
		return "showpokemon [#] <player>";
	}

	@Override
	public List<String> getAliases() {
		// TODO Auto-generated method stub
		return this.aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException 
	{
		if(sender.canUseCommand(4, "miscfeatures.showpokemon"))
		{
			if(args.length == 0)
			{
				sender.sendMessage(new TextComponentString(TextFormatting.RED + "Usage: /showpokemon [#] <player>"));							
				return;
			}
			if(sender instanceof EntityPlayerMP)
			{
				EntityPlayerMP player = (EntityPlayerMP) sender;
				try
				{
					if(NumberUtils.isNumber(args[0]))
					{
						int slotNumber = Integer.parseInt(args[0]);
						ItemStack item = Util.getItem(slotNumber, player);
						if(item != null)
						{
							if(args.length == 2)
							{
								for(EntityPlayerMP otherPlayer : server.getPlayerList().getPlayers())
								{
									if(otherPlayer.getName().equalsIgnoreCase(args[1]))
									{
										TextComponentTranslation chatTransOther = new TextComponentTranslation("", new Object());
										chatTransOther.appendSibling(new TextComponentString(TextFormatting.GRAY + "[" + TextFormatting.AQUA + player.getName() + TextFormatting.GRAY + "] shows" + TextFormatting.GOLD + " you" + TextFormatting.GRAY + " their: "));
										chatTransOther.appendSibling(item.getTextComponent());
										otherPlayer.sendMessage(chatTransOther);
										
										TextComponentTranslation chatTransSender = new TextComponentTranslation("", new Object());
										chatTransSender.appendSibling(new TextComponentString(TextFormatting.GRAY + "[" + TextFormatting.AQUA + "You" + TextFormatting.GRAY + "] showed " + TextFormatting.GOLD + otherPlayer.getName() + TextFormatting.GRAY + " your: "));
										chatTransSender.appendSibling(item.getTextComponent());
										player.sendMessage(chatTransSender);
										return;
									}
								}
								sender.sendMessage(new TextComponentString(TextFormatting.RED + "Player not found!"));								
							}
							else
							{
								if(pokemonShowCooldown.containsKey(player.getUniqueID().toString()))
								{
									long secondsLeft = ((pokemonShowCooldown.get(player.getUniqueID().toString())/1000)+ Main.pokemonShowCooldown) - (System.currentTimeMillis()/1000);
									if(secondsLeft>0) 
									{
										sender.sendMessage(new TextComponentString(TextFormatting.RED + "You can show everyone your pokemon again in: " + secondsLeft + " seconds"));
										return;
									}
								}
								TextComponentTranslation chatTrans = new TextComponentTranslation("", new Object());
								chatTrans.appendSibling(new TextComponentString(TextFormatting.GRAY + "[" + TextFormatting.AQUA + player.getName() + TextFormatting.GRAY + "] shows" + TextFormatting.GOLD + " everyone" + TextFormatting.GRAY + " their: "));
								chatTrans.appendSibling(item.getTextComponent());
								server.getPlayerList().sendMessage(chatTrans);
								pokemonShowCooldown.put(player.getUniqueID().toString(), System.currentTimeMillis());
								return;
							}
						}
						else
						{
							sender.sendMessage(new TextComponentString(TextFormatting.RED + "Party slot is empty!"));
						}
					}
					else
					{
						sender.sendMessage(new TextComponentString(TextFormatting.RED + "Usage: /showpokemon [#] <player>"));
						return;
					}
				}
				catch (Exception ex)
				{
					
				}
			}
			return;
		}
		else
		{
			sender.sendMessage(new TextComponentString(TextFormatting.RED + "You don't have permission to use this command"));
			return;
		}

	}
	

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender) 
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index) {
		// TODO Auto-generated method stub
		return false;
	}

}
