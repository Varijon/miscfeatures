package com.varijon.tinies.MiscFeatures;

import java.util.Optional;

import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class SpeciesTokenHandler 
{		
	@SubscribeEvent
	public void onPlayerInteract(PlayerInteractEvent.RightClickItem event)
	{
		EntityPlayerMP player = (EntityPlayerMP) event.getEntityPlayer();
		String pokemonName = CheckForSpeciesToken(player.getHeldItem(EnumHand.MAIN_HAND));
		if(pokemonName != null)
		{
			if(player.getHeldItem(EnumHand.MAIN_HAND).getCount() >= 4)
			{
				EntityPixelmon pokemonToGive = (EntityPixelmon)PixelmonEntityList.createEntityByName(pokemonName, player.getServerWorld());
				boolean isShiny = CheckIfShinyToken(player.getHeldItem(EnumHand.MAIN_HAND));				
				
				Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
				if (!optPlayerStorage.isPresent()) 
				{
			        return;
			    }
				
				if(isShiny)
				{
					pokemonToGive.setIsShiny(true);
				}
				
				PlayerStorage playerStorage = (PlayerStorage) optPlayerStorage.get();
				playerStorage.addToParty(pokemonToGive);
				
				MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
				
				PixelmonStorage.pokeBallManager.savePlayer(server, playerStorage);
				if(isShiny)
				{
					player.sendMessage(new TextComponentString(TextFormatting.GREEN + "Obtained a " + TextFormatting.GOLD + "Shiny " + TextFormatting.RED + pokemonToGive.getPokemonName() + TextFormatting.GREEN + "!"));
				}
				else
				{				
					player.sendMessage(new TextComponentString(TextFormatting.GREEN + "Obtained a " + TextFormatting.RED + pokemonToGive.getPokemonName() + TextFormatting.GREEN + "!"));				
				}
				
				if(player.getHeldItem(EnumHand.MAIN_HAND).getCount() > 4)
				{
					player.getHeldItem(EnumHand.MAIN_HAND).setCount(player.getHeldItem(EnumHand.MAIN_HAND).getCount() - 4);
				}
				else
				{
					player.inventory.removeStackFromSlot(player.inventory.currentItem);
				}
				player.inventoryContainer.detectAndSendChanges();
			}
			else
			{
				int remaining = 4 - player.getHeldItem(EnumHand.MAIN_HAND).getCount();
				player.sendMessage(new TextComponentString(TextFormatting.RED + "You need " + TextFormatting.GOLD + remaining + TextFormatting.RED + " more token(s)!"));
			}

		}

	}
	
	public String CheckForSpeciesToken(ItemStack item)
	{
		if(item != null)
		{
			NBTTagCompound nbt = item.getTagCompound();
			if(nbt != null)
			{
				if(nbt.hasKey("speciesToken"))
				{
					return nbt.getString("speciesToken");
				}
			}
		}
		return null;
	}
	public boolean CheckIfShinyToken(ItemStack item)
	{
		if(item != null)
		{
			NBTTagCompound nbt = item.getTagCompound();
			if(nbt != null)
			{
				if(nbt.hasKey("shinyToken"))
				{
					return nbt.getBoolean("shinyToken");
				}
			}
		}
		return false;
	}
}
