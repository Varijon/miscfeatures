package com.varijon.tinies.MiscFeatures;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import net.minecraft.nbt.NBTTagCompound;

public class DeletedPokemonFile 
{
	public static void initConfig()
	{
		String basefolder = new File("").getAbsolutePath();
        String source = basefolder + "\\config\\deletedpixelmon";
		File dir = new File(source);
		String source2 = basefolder + "\\config\\deletedpixelmon\\pixelmon";
		File dir2 = new File(source2);
		if(dir.exists())
		{
			if(dir.listFiles().length > 0)
			{
				
			}
			else
			{
				writeFile("This file keeps track of what people delete");
			}
		}
		else
		{
			dir.mkdirs();
			//dir2.mkdirs();
			writeFile("This file keeps track of what people delete");
		}
	}
	public static void writeFile(String text)
	{
		String basefolder = new File("").getAbsolutePath();
        String source = basefolder + "\\config\\deletedpixelmon";
		String fileName = source + "\\deletedpixelmonlog.txt";

        try 
        {
            FileWriter fileWriter = new FileWriter(fileName,true);

            // Always wrap FileWriter in BufferedWriter.
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            // Note that write() does not automatically
            // append a newline character.
            bufferedWriter.append(text);
            bufferedWriter.newLine();

            bufferedWriter.close();
        }
        catch(IOException ex) 
        {
            System.out.println("Error writing to file '" + fileName + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        }
	}
	public static void writeNBTFile(NBTTagCompound tags, String date)
	{
		String basefolder = new File("").getAbsolutePath();
        String source = basefolder + "\\config\\deletedpixelmon\\pixelmon";
		String fileName = source + "\\" + date + tags.getString("OwnerUUID") + "  " + tags.getLong("UUIDLeast") + ".json";

        try 
        {
            FileWriter fileWriter = new FileWriter(fileName);

            // Always wrap FileWriter in BufferedWriter.
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            // Note that write() does not automatically
            // append a newline character.
            bufferedWriter.write(tags.toString());

            bufferedWriter.close();
        }
        catch(IOException ex) 
        {
            //System.out.println("Error writing to file '" + fileName + "'");
            // Or we could just do this:
            ex.printStackTrace();
        }
	}
}
