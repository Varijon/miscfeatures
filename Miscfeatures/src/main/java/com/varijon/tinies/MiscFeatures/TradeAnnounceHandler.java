package com.varijon.tinies.MiscFeatures;

import com.pixelmonmod.pixelmon.api.events.PixelmonTradeEvent;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class TradeAnnounceHandler 
{
	@SubscribeEvent
	public void onPixelTrade(PixelmonTradeEvent event)
	{
		NBTTagCompound pixelmon1 = event.pokemon1;
		NBTTagCompound pixelmon2 = event.pokemon2;
		EntityPlayerMP player1 = (EntityPlayerMP) event.player1;
		EntityPlayerMP player2 = (EntityPlayerMP) event.player2;	
		MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
		
		boolean showMessage = false;
		StringBuilder sb = new StringBuilder();
		
		sb.append(TextFormatting.WHITE + "[" + TextFormatting.RED + "Trade" + TextFormatting.WHITE + "] ");
		sb.append(TextFormatting.LIGHT_PURPLE + player1.getName() + TextFormatting.GRAY + " traded their ");
		if(pixelmon1.getBoolean("IsShiny"))
		{
			sb.append(TextFormatting.GOLD + "Shiny ");
			showMessage = true;
		}
		if(EnumPokemon.legendaries.contains(pixelmon1.getString("Name")))
		{
//			sb.append(TextFormatting.AQUA + "LEGENDARY ");
			showMessage = true;
		}
		sb.append(TextFormatting.AQUA + pixelmon1.getString("Name") + " ");
		sb.append(TextFormatting.GRAY + "to ");
		
		sb.append(TextFormatting.LIGHT_PURPLE + player2.getName() + TextFormatting.GRAY + " for their ");
		if(pixelmon2.getBoolean("IsShiny"))
		{
			sb.append(TextFormatting.GOLD + "Shiny ");
			showMessage = true;
		}
		if(EnumPokemon.legendaries.contains(pixelmon2.getString("Name")))
		{
//			sb.append(TextFormatting.AQUA + "LEGENDARY ");
			showMessage = true;
		}
		sb.append(TextFormatting.AQUA + pixelmon2.getString("Name") + TextFormatting.GRAY + "!");
		
		if(showMessage)
		{
			server.getPlayerList().sendMessage(new TextComponentString(sb.toString()));			
		}
	}
}
