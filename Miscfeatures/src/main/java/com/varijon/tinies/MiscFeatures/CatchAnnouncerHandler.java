package com.varijon.tinies.MiscFeatures;

import com.pixelmonmod.pixelmon.api.events.CaptureEvent;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pokeballs.EntityPokeBall;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.enums.items.EnumPokeballs;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;


public class CatchAnnouncerHandler 
{
	@SubscribeEvent
	public void onPixelmonCatch(CaptureEvent.SuccessfulCapture event)
	{
		EntityPixelmon pixelmon = event.getPokemon();
		EntityPlayerMP player = (EntityPlayerMP) event.player;
		EntityPokeBall pokeball = event.pokeball;
		MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
		
		boolean showMessage = false;
		StringBuilder sb = new StringBuilder();
		
		sb.append(TextFormatting.WHITE + "[" + TextFormatting.RED + "Oh Yes" + TextFormatting.WHITE + "] ");
		sb.append(TextFormatting.LIGHT_PURPLE + player.getName() + TextFormatting.GRAY + " has caught a ");
		if(pixelmon.getIsShiny())
		{
			sb.append(TextFormatting.GOLD + "Shiny ");
			showMessage = true;
		}
		if(EnumPokemon.legendaries.contains(pixelmon.getPokemonName()))
		{
			showMessage = true;
		}
		String anString = "a";
		if(pokeball.getType() == EnumPokeballs.PokeBall.UltraBall)
		{
			anString = "an";
		}
		sb.append(TextFormatting.AQUA + pixelmon.getPokemonName() + TextFormatting.GRAY + " using " + anString + " " + pokeball.getType().getItem().getLocalizedName() + "!");
				
		if(showMessage)
		{
			server.getPlayerList().sendMessage(new TextComponentString(sb.toString()));			
		}
	}
}
