package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.comm.packetHandlers.SendExtraData;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerExtraData;
import com.pixelmonmod.pixelmon.storage.PlayerExtras;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class ExtrasCommand implements ICommand {

	private List aliases;
	public ExtrasCommand()
	{
	   this.aliases = new ArrayList();
	   this.aliases.add("getextras");
	}
	
	@Override
	public int compareTo(ICommand arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "getextras";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		// TODO Auto-generated method stub
		return "getextras [nosash|sash|sashrainbow|nohat|fez|monocle|tophat|fedora]";
	}

	@Override
	public List<String> getAliases() {
		// TODO Auto-generated method stub
		return this.aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException 
	{
		if(sender.canUseCommand(4, "miscfeatures.getextras"))
		{
			if(args.length == 0 || args.length > 1)
			{
				sender.sendMessage(new TextComponentString(TextFormatting.RED + "Usage: /getextras [nosash|sash|sashrainbow|nohat|fez|monocle|tophat|fedora]"));						
				return;
			}
			if(sender instanceof EntityPlayerMP)
			{
				if(args[0].equals("nosash"))
				{
					EntityPlayerMP player = (EntityPlayerMP) sender;
					
						Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
						if (!optPlayerStorage.isPresent()) 
						{
					        return;
					    }
						PlayerStorage ps = (PlayerStorage) optPlayerStorage.get();
						PlayerExtraData data = ps.getExtraData();
						data.sashEnabled = false;
						data.robeEnabled = false;
						data.sashEnabled = false;
						sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "No longer wearing a sash"));
						
						PixelmonStorage.save(player);
						
						Pixelmon.network.sendToAll(new SendExtraData(data));
						
					return;
				}
				if(args[0].equals("sash"))
				{
					EntityPlayerMP player = (EntityPlayerMP) sender;
					
						Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
						if (!optPlayerStorage.isPresent()) 
						{
					        return;
					    }
						PlayerStorage ps = (PlayerStorage) optPlayerStorage.get();
						PlayerExtraData data = ps.getExtraData();
						data.sashEnabled = true;
						data.robeEnabled = false;
						data.sashEnabled = true;
						sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Now wearing a sash"));
						
						PixelmonStorage.save(player);
						
						Pixelmon.network.sendToAll(new SendExtraData(data));
						
					return;
				}
				if(args[0].equals("rainbowsash"))
				{
					EntityPlayerMP player = (EntityPlayerMP) sender;
					
						Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
						if (!optPlayerStorage.isPresent()) 
						{
					        return;
					    }
						PlayerStorage ps = (PlayerStorage) optPlayerStorage.get();
						PlayerExtraData data = ps.getExtraData();
						data.sashEnabled = true;
						data.robeEnabled = true;
						data.sashEnabled = true;
						sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Now wearing a rainbow sash"));
						
						PixelmonStorage.save(player);
						
						Pixelmon.network.sendToAll(new SendExtraData(data));
						
					return;
				}
				if(args[0].equals("nohat"))
				{
					EntityPlayerMP player = (EntityPlayerMP) sender;
					
						Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
						if (!optPlayerStorage.isPresent()) 
						{
					        return;
					    }
						PlayerStorage ps = (PlayerStorage) optPlayerStorage.get();
						PlayerExtraData data = ps.getExtraData();
						data.isSupport = false;
						data.removeHat();
						sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "No longer wearing a hat"));
						
						PixelmonStorage.save(player);
						
						Pixelmon.network.sendToAll(new SendExtraData(data));
						
					return;
				}
				if(args[0].equals("fez"))
				{
					EntityPlayerMP player = (EntityPlayerMP) sender;
					
						Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
						if (!optPlayerStorage.isPresent()) 
						{
					        return;
					    }
						PlayerStorage ps = (PlayerStorage) optPlayerStorage.get();
						PlayerExtraData data = ps.getExtraData();
						data.isSupport = true;
						data.setHatType(PlayerExtras.HatType.FEZ);
						sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Now wearing a fez"));
						
						PixelmonStorage.save(player);
						
						Pixelmon.network.sendToAll(new SendExtraData(data));
						
					return;
				}
				if(args[0].equals("monocle"))
				{
					EntityPlayerMP player = (EntityPlayerMP) sender;
					
						Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
						if (!optPlayerStorage.isPresent()) 
						{
					        return;
					    }
						PlayerStorage ps = (PlayerStorage) optPlayerStorage.get();
						PlayerExtraData data = ps.getExtraData();
						data.isSupport = true;
						data.setHatType(PlayerExtras.HatType.getFromId(3));
						sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Now wearing a monocle"));
						
						PixelmonStorage.save(player);
						
						Pixelmon.network.sendToAll(new SendExtraData(data));
						
					return;
				}
				if(args[0].equals("tophat"))
				{
					EntityPlayerMP player = (EntityPlayerMP) sender;
					
						Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
						if (!optPlayerStorage.isPresent()) 
						{
					        return;
					    }
						PlayerStorage ps = (PlayerStorage) optPlayerStorage.get();
						PlayerExtraData data = ps.getExtraData();
						data.isSupport = true;
						data.setHatType(PlayerExtras.HatType.TOP_HAT);
						sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Now wearing a tophat"));
						
						PixelmonStorage.save(player);
						
						Pixelmon.network.sendToAll(new SendExtraData(data));
						
					return;
				}
				if(args[0].equals("fedora"))
				{
					EntityPlayerMP player = (EntityPlayerMP) sender;
					
						Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
						if (!optPlayerStorage.isPresent()) 
						{
					        return;
					    }
						PlayerStorage ps = (PlayerStorage) optPlayerStorage.get();
						PlayerExtraData data = ps.getExtraData();
						data.isSupport = true;
						data.setHatType(PlayerExtras.HatType.FEDORA);
						sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Now wearing a fedora"));
						
						PixelmonStorage.save(player);
						
						Pixelmon.network.sendToAll(new SendExtraData(data));
						
					return;
				}
				sender.sendMessage(new TextComponentString(TextFormatting.RED + "Usage: /getextras [nosash|sash|sashrainbow|nohat|fez|monocle|tophat|fedora]"));
				
			}
			return;
		}
		else
		{
			sender.sendMessage(new TextComponentString(TextFormatting.RED + "You don't have permission to use this command"));
			return;
		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender) 
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index) {
		// TODO Auto-generated method stub
		return false;
	}
	
//	private String buildTellRaw()
//	{
//		StringBuilder sb = new StringBuilder();
//		
//	}
	
}
