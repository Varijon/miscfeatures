package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.pixelmonmod.pixelmon.config.PixelmonBlocks;
import com.pixelmonmod.pixelmon.config.PixelmonItems;
import com.pixelmonmod.pixelmon.config.PixelmonItemsTools;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.network.play.server.SPacketTitle;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.item.ItemTossEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class MagicPickaxeHandler 
{
	Random rng;
	
	//0.30 was okay
	double EXPCONSTANT = 0.30;
	int LEVEL_REQ_EFF1 = 10;
	int LEVEL_REQ_EFF2 = 20;
	int LEVEL_REQ_EFF3 = 30;
	int LEVEL_REQ_EFF4 = 50;
	
	int LEVEL_REQ_FOR1 = 15;
	int LEVEL_REQ_FOR2 = 25;
	int LEVEL_REQ_FOR3 = 40;
	
	int LEVEL_REQ_STONE = 5;
	int LEVEL_REQ_ALUMINIUM = 15;
	int LEVEL_REQ_IRON = 20;
	int LEVEL_REQ_AMETHYST= 25;
	int LEVEL_REQ_SAPPHIRE = 30;
	int LEVEL_REQ_RUBY = 35;
	int LEVEL_REQ_CRYSTAL = 40;
	int LEVEL_REQ_DIAMOND = 45;
	int LEVEL_REQ_FIRESTONE = 50;
	int LEVEL_REQ_WATERSTONE = 55;
	
	int LEVEL_REQ_FIRESHARD = 10;
	int LEVEL_REQ_WATERSHARD = 10;
	int LEVEL_REQ_LEAFSHARD = 22;
	int LEVEL_REQ_MOONSHARD = 27;
	int LEVEL_REQ_THUNDERSHARD = 32;
	int LEVEL_REQ_SUNSHARD = 35;
	int LEVEL_REQ_SHINYSHARD = 45;
	int LEVEL_REQ_DAWNSHARD = 55;
	int LEVEL_REQ_DUSKSHARD = 55;
	
	int LEVEL_REQ_SWITCH = 35;
	
	int EXP_COBBLE = 1;
	int EXP_STONE = 1;
	int EXP_OBSIDIAN = 15;
	int EXP_COAL = 5;
	int EXP_IRON = 10;
	int EXP_GOLD = 40;
	int EXP_REDSTONE = 8;
	int EXP_LAPIS = 5;
	int EXP_DIAMOND = 50;
	int EXP_EMERALD = 35;
	int EXP_AMETHYST = 25;
	int EXP_RUBY = 25;
	int EXP_CRYSTAL = 25;
	int EXP_SAPPHIRE = 25;
	int EXP_SILICON = 30;
	int EXP_BAUXITE = 30;
	int EXP_FIRESTONE = 25;
	int EXP_WATERSTONE = 15;
	int EXP_DUSKSTONE = 25;
	int EXP_DAWNSTONE = 25;
	int EXP_LEAFSTONE = 25;
	int EXP_THUNDERSTONE = 25;
	
	int XMINPOS = -419;
	int XMAXPOS = 413;
	int ZMINPOS = -188;
	int ZMAXPOS = 638;
	
	public MagicPickaxeHandler()
	{
		rng = new Random();
	}
	
	
	@SubscribeEvent
	public void onBlockHarvest(HarvestDropsEvent event)
	{
		if(event.getHarvester() == null || event.getState().getBlock() == Blocks.REDSTONE_WIRE || event.getState().getBlock() == Blocks.COBBLESTONE)
		{
			return;
		}
//		if(event.getPos().getX() < 413 && event.getPos().getX() > -419)
//		{
//			if(event.getPos().getZ() < 638 && event.getPos().getZ() > -188)
//			{
//				return;
//			}
//		}
		if(CheckForMagicPickaxe(event.getHarvester().getHeldItem(EnumHand.MAIN_HAND)))
		{
			ItemStack pickaxe = event.getHarvester().getHeldItem(EnumHand.MAIN_HAND);
			if(!MagicPickaxeHasInitialized(pickaxe))
			{
				pickaxe.setTagCompound(InitializeMagicPickaxe(pickaxe.getTagCompound()));
			}
			IncrementExp(pickaxe, GetExpFromDrops(event.getDrops(),pickaxe), event.getHarvester());
			ItemStack checkPickaxe = ApplyMaterials(pickaxe);
			if(checkPickaxe != pickaxe)
			{
				event.getHarvester().setHeldItem(EnumHand.MAIN_HAND,checkPickaxe);				
			}
			ApplyEnchants(pickaxe);
			UpdateDisplay(pickaxe);
			List<ItemStack> newDrops = EditDrops(event.getDrops(), pickaxe);
			event.getDrops().clear();
			event.getDrops().addAll(newDrops);
		}
	}
	
	public List<ItemStack> EditDrops(List<ItemStack> dropsList, ItemStack pickaxe)
	{
		NBTTagCompound nbt = pickaxe.getTagCompound();
		int level = nbt.getInteger("PickaxeLevel");
		ArrayList<ItemStack> newDropList = new ArrayList<ItemStack>();
		for(ItemStack item : dropsList)
		{
			double oreOddsMulti = GetOreMultiplier(item);
//			System.out.println("Fire " + nbt.getInteger("PickaxeFireShard") * oreOddsMulti);
//			System.out.println("Water " + nbt.getInteger("PickaxeWaterShard") * oreOddsMulti);		
//			System.out.println("Leaf " + nbt.getInteger("PickaxeLeafShard") * oreOddsMulti);	
//			System.out.println("Thunder " + nbt.getInteger("PickaxeThunderShard") * oreOddsMulti);	
//			System.out.println("Sun " + nbt.getInteger("PickaxeSunShard") * oreOddsMulti);	
//			System.out.println("Shiny " + nbt.getInteger("PickaxeShinyShard") * oreOddsMulti);	
//			System.out.println("Dawn " + nbt.getInteger("PickaxeDawnShard") * oreOddsMulti);
//			System.out.println("Dusk " + nbt.getInteger("PickaxeDuskShard") * oreOddsMulti);

			if(item.getItem() == Item.getItemFromBlock(Blocks.COBBLESTONE) || item.getItem() == Item.getItemFromBlock(Blocks.STONE) || oreOddsMulti == 0)
			{
				newDropList.add(item);
				continue;
			}
			if(level >= LEVEL_REQ_FIRESHARD)
			{
				if(rng.nextInt(10000) <= nbt.getInteger("PickaxeFireShard") * oreOddsMulti)
				{				
					newDropList.add(new ItemStack(PixelmonItems.fireStoneShard));
				}
			}
			if(level >= LEVEL_REQ_WATERSHARD)
			{
				if(rng.nextInt(10000) <= nbt.getInteger("PickaxeWaterShard") * oreOddsMulti)
				{				
					newDropList.add(new ItemStack(PixelmonItems.waterStoneShard));			
				}
			}
			if(level >= LEVEL_REQ_LEAFSHARD)
			{
				if(rng.nextInt(10000) <= nbt.getInteger("PickaxeLeafShard") * oreOddsMulti)
				{				
					newDropList.add(new ItemStack(PixelmonItems.leafStoneShard));				
				}
			}
			if(level >= LEVEL_REQ_MOONSHARD)
			{
				if(rng.nextInt(10000) <= nbt.getInteger("PickaxeMoonShard") * oreOddsMulti)
				{				
					newDropList.add(new ItemStack(PixelmonItems.moonStoneShard));				
				}
			}
			if(level >= LEVEL_REQ_THUNDERSHARD)
			{
				if(rng.nextInt(10000) <= nbt.getInteger("PickaxeThunderShard") * oreOddsMulti)
				{				
					newDropList.add(new ItemStack(PixelmonItems.thunderStoneShard));				
				}
			}
			if(level >= LEVEL_REQ_SUNSHARD)
			{
				if(rng.nextInt(10000) <= nbt.getInteger("PickaxeSunShard") * oreOddsMulti)
				{				
					newDropList.add(new ItemStack(PixelmonItems.sunStoneShard));				
				}
			}
			if(level >= LEVEL_REQ_SHINYSHARD)
			{
				if(rng.nextInt(10000) <= nbt.getInteger("PickaxeShinyShard") * oreOddsMulti)
				{				
					newDropList.add(new ItemStack(PixelmonItems.shinyStoneShard));				
				}
			}
			if(level >= LEVEL_REQ_DAWNSHARD)
			{
				if(rng.nextInt(10000) <= nbt.getInteger("PickaxeDawnShard") * oreOddsMulti)
				{				
					newDropList.add(new ItemStack(PixelmonItems.dawnStoneShard));					
				}
			}
			if(level >= LEVEL_REQ_DUSKSHARD)
			{
				if(rng.nextInt(10000) <= nbt.getInteger("PickaxeDuskShard") * oreOddsMulti)
				{				
					newDropList.add(new ItemStack(PixelmonItems.duskStoneShard));			
				}
			}
			if(item.getItem() == Item.getItemFromBlock(Blocks.IRON_ORE))
			{
				newDropList.add(new ItemStack(Items.IRON_INGOT));
				continue;
			}
			if(item.getItem() == Item.getItemFromBlock(Blocks.GOLD_ORE))
			{
				newDropList.add(new ItemStack(Items.GOLD_INGOT));
				continue;
			}
			if(item.getItem() == Item.getItemFromBlock(PixelmonBlocks.siliconOre))
			{
				newDropList.add(new ItemStack(PixelmonItems.siliconItem));
				continue;
			}
			if(item.getItem() == Item.getItemFromBlock(PixelmonBlocks.bauxite))
			{
				newDropList.add(new ItemStack(PixelmonItems.aluminiumIngot));
				continue;
			}
			newDropList.add(item);
		}
		return newDropList;
	}
	
	public int GetExpFromDrops(List<ItemStack> dropsList, ItemStack pickaxe)
	{
		int expTotal = 0;
		NBTTagCompound nbt = pickaxe.getTagCompound();
		int level = nbt.getInteger("PickaxeLevel");
		for(ItemStack item : dropsList)
		{
			if(item.getItem() == Item.getItemFromBlock(Blocks.COBBLESTONE) && level < LEVEL_REQ_EFF2)
			{
				expTotal += EXP_COBBLE;
			}
			if(item.getItem() == Item.getItemFromBlock(Blocks.STONE) && level < LEVEL_REQ_EFF2)
			{
				expTotal += EXP_STONE;
			}
			if(item.getItem() == Item.getItemFromBlock(Blocks.OBSIDIAN))
			{
				expTotal += EXP_OBSIDIAN;
			}
			if(item.getItem() == Item.getItemFromBlock(Blocks.IRON_ORE))
			{
				expTotal += EXP_IRON;
			}
			if(item.getItem() == Item.getItemFromBlock(Blocks.GOLD_ORE))
			{
				expTotal += EXP_GOLD;
			}
			if(item.getItem() == Item.getItemFromBlock(PixelmonBlocks.siliconOre))
			{
				expTotal += EXP_SILICON;
			}
			if(item.getItem() == Item.getItemFromBlock(PixelmonBlocks.bauxite))
			{
				expTotal += EXP_BAUXITE;
			}
			if(item.getItem() == Items.COAL)
			{
				expTotal += EXP_COAL;
			}
			if(item.getItem() == Items.REDSTONE)
			{
				expTotal += EXP_REDSTONE;
			}
			if(item.getItem() == Items.DYE && item.getItemDamage() == 4)
			{
				expTotal += EXP_LAPIS;
			}
			if(item.getItem() == Items.DIAMOND)
			{
				expTotal += EXP_DIAMOND;
			}
			if(item.getItem() == Items.EMERALD)
			{
				expTotal += EXP_EMERALD;
			}
			if(item.getItem() == PixelmonItems.amethyst)
			{
				expTotal += EXP_AMETHYST;
			}
			if(item.getItem() == PixelmonItems.ruby)
			{
				expTotal += EXP_RUBY;
			}
			if(item.getItem() == PixelmonItems.sapphire)
			{
				expTotal += EXP_SAPPHIRE;
			}
			if(item.getItem() == PixelmonItems.crystal)
			{
				expTotal += EXP_CRYSTAL;
			}
			if(item.getItem() == PixelmonItems.fireStoneShard)
			{
				expTotal += EXP_FIRESTONE;
			}
			if(item.getItem() == PixelmonItems.waterStoneShard)
			{
				expTotal += EXP_WATERSTONE;
			}
			if(item.getItem() == PixelmonItems.leafStoneShard)
			{
				expTotal += EXP_LEAFSTONE;
			}
			if(item.getItem() == PixelmonItems.duskStoneShard)
			{
				expTotal += EXP_DUSKSTONE;
			}
			if(item.getItem() == PixelmonItems.dawnStoneShard)
			{
				expTotal += EXP_DAWNSTONE;
			}
			if(item.getItem() == PixelmonItems.thunderStoneShard)
			{
				expTotal += EXP_THUNDERSTONE;
			}
		}
		return expTotal;
	}
	
	public void IncrementExp(ItemStack pickaxe, int expAmount, EntityPlayer player)
	{
		NBTTagCompound nbt = pickaxe.getTagCompound();
		
		int oldExp = nbt.getInteger("PickaxeExp");
		int levelRequirement = nbt.getInteger("PickaxeLevelupExp");
		int pickaxeLevel = nbt.getInteger("PickaxeLevel");
		
		int newExp = oldExp + expAmount;
		
		if(newExp >= levelRequirement)
		{
			newExp = newExp - levelRequirement;
			nbt.setInteger("PickaxeLevel", pickaxeLevel + 1);
			nbt.setInteger("PickaxeLevelupExp", (int) Math.pow(((double) (pickaxeLevel + 1) / EXPCONSTANT), 2));
			
			ApplyShardLevelup(pickaxe);
			
			SendLevelupMessage(player, pickaxeLevel + 1);
		}

		nbt.setInteger("PickaxeExp", newExp);
		
		pickaxe.setTagCompound(nbt);
		return;
	}
	
	public void ApplyShardLevelup(ItemStack pickaxe)
	{
		NBTTagCompound nbt = pickaxe.getTagCompound();
		int level = nbt.getInteger("PickaxeLevel");

		if(level >= LEVEL_REQ_FIRESHARD)
		{		
			nbt.setInteger("PickaxeFireShard", 5 + (level * 5 - LEVEL_REQ_FIRESHARD * 5));
		}
		if(level >= LEVEL_REQ_WATERSHARD)
		{
			nbt.setInteger("PickaxeWaterShard", 5 + (level * 5 - LEVEL_REQ_WATERSHARD * 5));
		}
		if(level >= LEVEL_REQ_LEAFSHARD)
		{
			nbt.setInteger("PickaxeLeafShard", 5 + (level * 5 - LEVEL_REQ_LEAFSHARD * 5));
		}
		if(level >= LEVEL_REQ_MOONSHARD)
		{
			nbt.setInteger("PickaxeMoonShard", 5 + (level * 5 - LEVEL_REQ_MOONSHARD * 5));
		}
		if(level >= LEVEL_REQ_THUNDERSHARD)
		{
			nbt.setInteger("PickaxeThunderShard", 5 + (level * 5 - LEVEL_REQ_THUNDERSHARD * 5));
		}
		if(level >= LEVEL_REQ_SUNSHARD)
		{
			nbt.setInteger("PickaxeSunShard", 5 + (level * 5 - LEVEL_REQ_SUNSHARD * 5));
		}
		if(level >= LEVEL_REQ_SHINYSHARD)
		{
			nbt.setInteger("PickaxeShinyShard", 5 + (level * 5 - LEVEL_REQ_SHINYSHARD * 5));
		}
		if(level >= LEVEL_REQ_DAWNSHARD)
		{
			nbt.setInteger("PickaxeDawnShard", 5 + (level * 5 - LEVEL_REQ_DAWNSHARD * 5));
		}
		if(level >= LEVEL_REQ_DUSKSHARD)
		{
			nbt.setInteger("PickaxeDuskShard", 5 + (level * 5 - LEVEL_REQ_DUSKSHARD * 5));
		}
	}
	
	public ItemStack changePickaxe(ItemStack pickaxe, Item type)
	{
		ItemStack newPickaxe = new ItemStack(type,1);
		newPickaxe.setTagCompound(pickaxe.getTagCompound());
		
		return newPickaxe;
	}
	
	public ItemStack ApplyMaterials(ItemStack pickaxe)
	{
		NBTTagCompound nbt = pickaxe.getTagCompound();
		ItemStack newPickaxe = pickaxe;
		
		int pickaxeLevel = nbt.getInteger("PickaxeLevel");
		
		if(pickaxeLevel >= LEVEL_REQ_STONE && pickaxe.getItem() == Items.WOODEN_PICKAXE)
		{
			newPickaxe = changePickaxe(pickaxe, Items.STONE_PICKAXE);
		}
		if(pickaxeLevel >= LEVEL_REQ_ALUMINIUM && pickaxe.getItem() == Items.STONE_PICKAXE)
		{
			newPickaxe = changePickaxe(pickaxe, PixelmonItemsTools.pickaxeAluminium);
		}
		if(pickaxeLevel >= LEVEL_REQ_IRON && pickaxe.getItem() == PixelmonItemsTools.pickaxeAluminium)
		{
			newPickaxe = changePickaxe(pickaxe, Items.IRON_PICKAXE);
		}
		if(pickaxeLevel >= LEVEL_REQ_AMETHYST && pickaxe.getItem() == Items.IRON_PICKAXE)
		{
			newPickaxe = changePickaxe(pickaxe, PixelmonItemsTools.amethystPickaxeItem);
		}
		if(pickaxeLevel >= LEVEL_REQ_SAPPHIRE && pickaxe.getItem() == PixelmonItemsTools.amethystPickaxeItem)
		{
			newPickaxe = changePickaxe(pickaxe, PixelmonItemsTools.sapphirePickaxeItem);
		}
		if(pickaxeLevel >= LEVEL_REQ_RUBY && pickaxe.getItem() == PixelmonItemsTools.sapphirePickaxeItem)
		{
			newPickaxe = changePickaxe(pickaxe, PixelmonItemsTools.rubyPickaxeItem);
		}
		if(pickaxeLevel >= LEVEL_REQ_CRYSTAL && pickaxe.getItem() == PixelmonItemsTools.rubyPickaxeItem)
		{
			newPickaxe = changePickaxe(pickaxe, PixelmonItemsTools.crystalPickaxeItem);
		}
		if(pickaxeLevel >= LEVEL_REQ_DIAMOND && pickaxe.getItem() == PixelmonItemsTools.crystalPickaxeItem)
		{
			newPickaxe = changePickaxe(pickaxe, Items.DIAMOND_PICKAXE);
		}
		if(pickaxeLevel >= LEVEL_REQ_FIRESTONE && pickaxe.getItem() == Items.DIAMOND_PICKAXE)
		{
			newPickaxe = changePickaxe(pickaxe, PixelmonItemsTools.firestonePickaxeItem);
		}
		if(pickaxeLevel >= LEVEL_REQ_WATERSTONE && pickaxe.getItem() == PixelmonItemsTools.firestonePickaxeItem)
		{
			newPickaxe = changePickaxe(pickaxe, PixelmonItemsTools.waterstonePickaxeItem);
		}
		return newPickaxe;
	}
	
	public void ApplyEnchants(ItemStack pickaxe)
	{
		NBTTagCompound nbt = pickaxe.getTagCompound();
		int pickaxeLevel = nbt.getInteger("PickaxeLevel");

		nbt.setTag("ench", new NBTTagList());	
		
		if(pickaxeLevel >= LEVEL_REQ_EFF1 && pickaxeLevel < LEVEL_REQ_EFF2)
		{
			pickaxe.addEnchantment(Enchantment.getEnchantmentByID(32), 1);
		}
		if(pickaxeLevel >= LEVEL_REQ_EFF2 && pickaxeLevel < LEVEL_REQ_EFF3)
		{
			pickaxe.addEnchantment(Enchantment.getEnchantmentByID(32), 2);
		}
		if(pickaxeLevel >= LEVEL_REQ_EFF3 && pickaxeLevel < LEVEL_REQ_EFF4)
		{
			pickaxe.addEnchantment(Enchantment.getEnchantmentByID(32), 3);
		}
		if(pickaxeLevel >= LEVEL_REQ_EFF4)
		{
			pickaxe.addEnchantment(Enchantment.getEnchantmentByID(32), 4);
		}
		if(pickaxeLevel >= LEVEL_REQ_FOR1 && pickaxeLevel < LEVEL_REQ_FOR2 && nbt.getString("PickaxeMode").equals("Fortune"))
		{
			pickaxe.addEnchantment(Enchantment.getEnchantmentByID(35), 1);
		}
		if(pickaxeLevel >= LEVEL_REQ_FOR2 && pickaxeLevel < LEVEL_REQ_FOR3 && nbt.getString("PickaxeMode").equals("Fortune"))
		{
			pickaxe.addEnchantment(Enchantment.getEnchantmentByID(35), 2);
		}
		if(pickaxeLevel >= LEVEL_REQ_FOR3 && nbt.getString("PickaxeMode").equals("Fortune"))
		{
			pickaxe.addEnchantment(Enchantment.getEnchantmentByID(35), 3);
		}
		if(nbt.getString("PickaxeMode") == "Silk")
		{
			pickaxe.addEnchantment(Enchantment.getEnchantmentByID(33), 1);			
		}
		return;
	}
	
	public void UpdateDisplay(ItemStack pickaxe)
	{
		NBTTagCompound nbt = pickaxe.getTagCompound();
		
		int exp = nbt.getInteger("PickaxeExp");
		int pickaxeLevel = nbt.getInteger("PickaxeLevel");
	    int pickaxeLevelExp = nbt.getInteger("PickaxeLevelupExp");
		
		nbt.setTag("display", new NBTTagCompound());
		nbt.getCompoundTag("display").setString("Name", TextFormatting.GOLD + "Magic Pickaxe");
		NBTTagList loreList = new NBTTagList();
		loreList.appendTag(new NBTTagString(TextFormatting.GREEN + "Level: " + TextFormatting.AQUA + pickaxeLevel));
		loreList.appendTag(new NBTTagString(GetExpBarString(nbt.getInteger("PickaxeExp"), pickaxeLevelExp)));
		loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Exp: " + exp + "/" + pickaxeLevelExp));
		
		if(pickaxeLevel >= LEVEL_REQ_FIRESHARD)
		{
			loreList.appendTag(new NBTTagString(""));
			loreList.appendTag(new NBTTagString(TextFormatting.GOLD + "Fire Shard: " + ((double)nbt.getInteger("PickaxeFireShard") / 100) + "%"));
		}
		if(pickaxeLevel >= LEVEL_REQ_WATERSHARD)
		{
			loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Water Shard: " + ((double)nbt.getInteger("PickaxeWaterShard") / 100) + "%"));
		}
		if(pickaxeLevel >= LEVEL_REQ_LEAFSHARD)
		{
			loreList.appendTag(new NBTTagString(TextFormatting.DARK_GREEN + "Leaf Shard: " + ((double)nbt.getInteger("PickaxeLeafShard") / 100) + "%"));
		}
		if(pickaxeLevel >= LEVEL_REQ_MOONSHARD)
		{
			loreList.appendTag(new NBTTagString(TextFormatting.DARK_GRAY + "Moon Shard: " + ((double)nbt.getInteger("PickaxeMoonShard") / 100) + "%"));
		}
		if(pickaxeLevel >= LEVEL_REQ_THUNDERSHARD)
		{
			loreList.appendTag(new NBTTagString(TextFormatting.YELLOW + "Thunder Shard: " + ((double)nbt.getInteger("PickaxeThunderShard") / 100) + "%"));
		}
		if(pickaxeLevel >= LEVEL_REQ_SUNSHARD)
		{
			loreList.appendTag(new NBTTagString(TextFormatting.RED + "Sun Shard: " + ((double)nbt.getInteger("PickaxeSunShard") / 100) + "%"));
		}
		if(pickaxeLevel >= LEVEL_REQ_SHINYSHARD)
		{
			loreList.appendTag(new NBTTagString(TextFormatting.WHITE + "Shiny Shard: " + ((double)nbt.getInteger("PickaxeShinyShard") / 100) + "%"));
		}
		if(pickaxeLevel >= LEVEL_REQ_DAWNSHARD)
		{
			loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Dawn Shard: " + ((double) nbt.getInteger("PickaxeDawnShard") / 100) + "%"));
		}
		if(pickaxeLevel >= LEVEL_REQ_DUSKSHARD)
		{
			loreList.appendTag(new NBTTagString(TextFormatting.DARK_PURPLE + "Dusk Shard: " + ((double)nbt.getInteger("PickaxeDuskShard") / 100) + "%"));
		}
		if(pickaxeLevel >= LEVEL_REQ_SWITCH)
		{
			loreList.appendTag(new NBTTagString(""));
			loreList.appendTag(new NBTTagString(TextFormatting.GREEN + "Right-click and Sneak to toggle mode"));
		}
		
		nbt.getCompoundTag("display").setTag("Lore", loreList);
		pickaxe.setTagCompound(nbt);
		return;
	}
	
	public NBTTagCompound InitializeMagicPickaxe(NBTTagCompound nbt)
	{
		nbt.setInteger("PickaxeLevel", 1);
		nbt.setInteger("PickaxeExp", 0);
		nbt.setInteger("PickaxeLevelupExp", (int) Math.pow(((double) 1 / EXPCONSTANT), 2));
		nbt.setInteger("PickaxeFireShard", 0);
		nbt.setInteger("PickaxeWaterShard", 0);
		nbt.setInteger("PickaxeLeafShard", 0);
		nbt.setInteger("PickaxeMoonShard", 0);
		nbt.setInteger("PickaxeThunderShard", 0);
		nbt.setInteger("PickaxeSunShard", 0);
		nbt.setInteger("PickaxeShinyShard", 0);
		nbt.setInteger("PickaxeDawnShard", 0);
		nbt.setInteger("PickaxeDuskShard", 0);
		nbt.setString("PickaxeMode", "Fortune");
		nbt.setTag("display", new NBTTagCompound());
		nbt.getCompoundTag("display").setString("Name", TextFormatting.GOLD + "Magic Pickaxe");

		NBTTagList loreList = new NBTTagList();
		loreList.appendTag(new NBTTagString(TextFormatting.GREEN + "Level: " + TextFormatting.AQUA + nbt.getInteger("PickaxeLevel")));
		loreList.appendTag(new NBTTagString(GetExpBarString(nbt.getInteger("PickaxeExp"), nbt.getInteger("PickaxeLevelupExp"))));
		loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Exp: " + nbt.getInteger("PickaxeExp") + "/" + nbt.getInteger("PickaxeLevelupExp")));
		
		nbt.getCompoundTag("display").setTag("Lore", loreList);
		return nbt;
	}
	
	public String GetExpBarString(int currentExp, int maxExp)
	{
		int whitePipes = (int) (((double)currentExp / (double)maxExp) * (double)50);
		String pipes = TextFormatting.WHITE + "";
		for(int x = 0; x < whitePipes; x++)
		{
			pipes += "|";
		}
		pipes += TextFormatting.DARK_GRAY;
		for(int x = 0; x < (50 - whitePipes); x++)
		{
			pipes += "|";
		}
		return pipes;
	}
	
	public void SendLevelupMessage(EntityPlayer player, int level)
	{
		EntityPlayerMP playerMP = (EntityPlayerMP) player;
		
		TextComponentString subTitle = new TextComponentString("");
		if(level == LEVEL_REQ_STONE)
		{
			subTitle.appendText(TextFormatting.GOLD + "Upgraded to Stone    ");
		}
		if(level == LEVEL_REQ_ALUMINIUM)
		{
			subTitle.appendText(TextFormatting.GOLD + "Upgraded to Aluminium    ");
		}
		if(level == LEVEL_REQ_IRON)
		{
			subTitle.appendText(TextFormatting.GOLD + "Upgraded to Iron    ");
		}
		if(level == LEVEL_REQ_AMETHYST)
		{
			subTitle.appendText(TextFormatting.GOLD + "Upgraded to Amethyst    ");
		}
		if(level == LEVEL_REQ_SAPPHIRE)
		{
			subTitle.appendText(TextFormatting.GOLD + "Upgraded to Sapphire    ");
		}
		if(level == LEVEL_REQ_RUBY)
		{
			subTitle.appendText(TextFormatting.GOLD + "Upgraded to Ruby    ");
		}
		if(level == LEVEL_REQ_CRYSTAL)
		{
			subTitle.appendText(TextFormatting.GOLD + "Upgraded to Crystal    ");
		}
		if(level == LEVEL_REQ_DIAMOND)
		{
			subTitle.appendText(TextFormatting.GOLD + "Upgraded to Diamond    ");
		}
		if(level == LEVEL_REQ_FIRESTONE)
		{
			subTitle.appendText(TextFormatting.GOLD + "Upgraded to Fire Stone    ");
		}
		if(level == LEVEL_REQ_WATERSTONE)
		{
			subTitle.appendText(TextFormatting.GOLD + "Upgraded to Water Stone    ");
		}		
		if(level == LEVEL_REQ_EFF1)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Efficiency I   ");
		}
		if(level == LEVEL_REQ_EFF2)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Efficiency II   ");
		}
		if(level == LEVEL_REQ_EFF3)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Efficiency III   ");
		}
		if(level == LEVEL_REQ_EFF4)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Efficiency IV   ");
		}
		if(level == LEVEL_REQ_FOR1)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Fortune I   ");
		}
		if(level == LEVEL_REQ_FOR2)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Fortune II   ");
		}
		if(level == LEVEL_REQ_FOR3)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Fortune III   ");
		}
		if(level == LEVEL_REQ_FIRESHARD)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Fire Hunter   ");
		}
		if(level == LEVEL_REQ_WATERSHARD)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Water Hunter   ");
		}
		if(level == LEVEL_REQ_LEAFSHARD)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Leaf Hunter   ");
		}
		if(level == LEVEL_REQ_MOONSHARD)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Moon Hunter   ");
		}
		if(level == LEVEL_REQ_THUNDERSHARD)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Thunder Hunter   ");
		}
		if(level == LEVEL_REQ_SUNSHARD)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Sun Hunter   ");
		}
		if(level == LEVEL_REQ_SHINYSHARD)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Shiny Hunter   ");
		}
		if(level == LEVEL_REQ_DAWNSHARD)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Dawn Hunter   ");
		}
		if(level == LEVEL_REQ_DUSKSHARD)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Dusk Hunter   ");
		}
		if(level == LEVEL_REQ_SWITCH)
		{
			subTitle.appendText(TextFormatting.GREEN + "+" + TextFormatting.YELLOW +"Mode Switch   ");
		}

		SPacketTitle packetTimes = new SPacketTitle(5, 30, 30);
		SPacketTitle packetTitle = new SPacketTitle(SPacketTitle.Type.TITLE, new TextComponentString(TextFormatting.BOLD + "" + TextFormatting.YELLOW + "Pickaxe lvl" + TextFormatting.GOLD + level + TextFormatting.YELLOW + "!"));
		SPacketTitle packetSubtitle = new SPacketTitle(SPacketTitle.Type.SUBTITLE, subTitle);

		playerMP.connection.sendPacket(packetTimes);
		playerMP.connection.sendPacket(packetTitle);
		playerMP.connection.sendPacket(packetSubtitle);
		
		
//		
//		playerMP.sendPacket(packetTitle);
//		playerMP.playerNetServerHandler.sendPacket(packetSubtitle);
		
	}
	
	public double GetOreMultiplier(ItemStack item)
	{
		if(item.getItem() == Item.getItemFromBlock(Blocks.OBSIDIAN))
		{
			return 1.0 + ((double)EXP_OBSIDIAN / 10);
		}
		if(item.getItem() == Item.getItemFromBlock(Blocks.IRON_ORE))
		{
			return 1.0 + ((double)EXP_IRON / 10);
		}
		if(item.getItem() == Item.getItemFromBlock(Blocks.GOLD_ORE))
		{
			return 1.0 + ((double)EXP_GOLD / 10);
		}
		if(item.getItem() == Item.getItemFromBlock(PixelmonBlocks.siliconOre))
		{
			return 1.0 + ((double)EXP_SILICON / 10);
		}
		if(item.getItem() == Item.getItemFromBlock(PixelmonBlocks.bauxite))
		{
			return 1.0 + ((double)EXP_BAUXITE / 10);
		}
		if(item.getItem() == Items.COAL)
		{
			return 1.0 + ((double)EXP_COAL / 20);
		}
		if(item.getItem() == Items.REDSTONE)
		{
			return 1.0 + ((double)EXP_REDSTONE / 25);
		}
		if(item.getItem() == Items.DYE && item.getItemDamage() == 4)
		{
			return 1.0 + ((double)EXP_LAPIS / 20);
		}
		if(item.getItem() == Items.DIAMOND)
		{
			return 1.0 + ((double)EXP_DIAMOND / 8);
		}
		if(item.getItem() == Items.EMERALD)
		{
			return 1.0 + ((double)EXP_EMERALD / 10);
		}
		if(item.getItem() == PixelmonItems.amethyst)
		{
			return 1.0 + ((double)EXP_AMETHYST / 8);
		}
		if(item.getItem() == PixelmonItems.ruby)
		{
			return 1.0 + ((double)EXP_RUBY / 8);
		}
		if(item.getItem() == PixelmonItems.sapphire)
		{
			return 1.0 + ((double)EXP_SAPPHIRE / 8);
		}
		if(item.getItem() == PixelmonItems.crystal)
		{
			return 1.0 + ((double)EXP_CRYSTAL / 8);
		}
		if(item.getItem() == PixelmonItems.fireStoneShard)
		{
			return 1.0 + ((double)EXP_FIRESTONE / 8);
		}
		if(item.getItem() == PixelmonItems.waterStoneShard)
		{
			return 1.0 + ((double)EXP_WATERSTONE / 10);
		}
		if(item.getItem() == PixelmonItems.leafStoneShard)
		{
			return 1.0 + ((double)EXP_LEAFSTONE / 8);
		}
		if(item.getItem() == PixelmonItems.duskStoneShard)
		{
			return 1.0 + ((double)EXP_DUSKSTONE / 8);
		}
		if(item.getItem() == PixelmonItems.dawnStoneShard)
		{
			return 1.0 + ((double)EXP_DAWNSTONE / 8);
		}
		
		return 0;
	}
	
	public boolean CheckForMagicPickaxe(ItemStack item)
	{
		if(item != null)
		{
			NBTTagCompound nbt = item.getTagCompound();
			if(nbt != null)
			{
				if(nbt.hasKey("isMagicPickaxe"))
				{
					return true;
				}
			}
		}
		return false;
	}
	public boolean MagicPickaxeHasInitialized(ItemStack item)
	{
		if(item != null)
		{
			NBTTagCompound nbt = item.getTagCompound();
			if(nbt != null)
			{
				if(nbt.hasKey("PickaxeLevel"))
				{
					return true;
				}
			}
		}
		return false;
	}
	@SubscribeEvent
	public void onItemDrop(ItemTossEvent event)
	{
		if(event.getEntityItem() != null)
		{
			EntityItem item = event.getEntityItem();
			if(CheckForMagicPickaxe(item.getItem()))
			{
				event.getEntityItem().setEntityInvulnerable(true);
			}
		}
	}
	
	@SubscribeEvent
	public void onPlayerInteract(PlayerInteractEvent.RightClickItem event)
	{
//		if(event.action == Action.RIGHT_CLICK_BLOCK || event.action == Action.RIGHT_CLICK_AIR)
//		{
			if(!event.getEntityPlayer().isSneaking())
			{
				return;
			}
			if(CheckForMagicPickaxe(event.getEntityPlayer().getHeldItem(EnumHand.MAIN_HAND)))
			{
				ItemStack pickaxe = event.getEntityPlayer().getHeldItem(EnumHand.MAIN_HAND);
				if(MagicPickaxeHasInitialized(pickaxe))
				{
					NBTTagCompound nbt = pickaxe.getTagCompound();
					if(nbt.hasKey("PickaxeMode") && nbt.getInteger("PickaxeLevel") >= LEVEL_REQ_SWITCH)
					{
						EntityPlayerMP playerMP = (EntityPlayerMP) event.getEntityPlayer();
						if(nbt.getString("PickaxeMode").equals("Silk"))
						{
							nbt.setString("PickaxeMode", "Fortune");

							SPacketTitle packetTimes = new SPacketTitle(5, 15, 15);
							SPacketTitle packetTitle = new SPacketTitle(SPacketTitle.Type.TITLE, new TextComponentString(TextFormatting.BOLD + "" + TextFormatting.GOLD + "Fortune " + TextFormatting.YELLOW + "Mode"));

							playerMP.connection.sendPacket(packetTimes);
							playerMP.connection.sendPacket(packetTitle);
							ApplyEnchants(pickaxe);
							return;
						}
						if(nbt.getString("PickaxeMode").equals("Fortune"))
						{
							nbt.setString("PickaxeMode", "Silk");
							SPacketTitle packetTimes = new SPacketTitle(5, 15, 15);
							SPacketTitle packetTitle = new SPacketTitle(SPacketTitle.Type.TITLE, new TextComponentString(TextFormatting.BOLD + "" + TextFormatting.GREEN + "Silk Touch " + TextFormatting.YELLOW + "Mode"));
		
							playerMP.connection.sendPacket(packetTimes);					
							playerMP.connection.sendPacket(packetTitle);
							ApplyEnchants(pickaxe);
							return;
						}
					}
				}
			}
//		}
	}
	
}


