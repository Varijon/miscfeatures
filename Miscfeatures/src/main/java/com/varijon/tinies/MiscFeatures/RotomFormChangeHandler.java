package com.varijon.tinies.MiscFeatures;

import java.util.Random;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.comm.EnumUpdateType;
import com.pixelmonmod.pixelmon.comm.packetHandlers.OpenReplaceMoveScreen;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Moveset;
import com.pixelmonmod.pixelmon.storage.PlayerNotLoadedException;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.EntityInteract;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class RotomFormChangeHandler 
{
	Random rng;
	
	public RotomFormChangeHandler()
	{
		rng = new Random();
	}
	
	@SubscribeEvent
	public void onPlayerInteract(EntityInteract event) throws PlayerNotLoadedException
	{
		if(event.getTarget() instanceof EntityPixelmon)
		{
			if(event.getHand() != EnumHand.MAIN_HAND)
			{
				return;
			}
			EntityPixelmon pixelmon = (EntityPixelmon) event.getTarget();
			EntityPlayerMP player = (EntityPlayerMP) event.getEntityPlayer();
			
			
			if(!pixelmon.belongsTo(player) && !pixelmon.isInRanchBlock)
			{
				return;
			}
			
			if(player.getHeldItem(EnumHand.MAIN_HAND) != null)
			{
				ItemStack item = player.getHeldItem(EnumHand.MAIN_HAND);
				NBTTagCompound nbt = item.getTagCompound();
				if(nbt != null)
				{
					if(nbt.hasKey("RotomItem"))
					{
						event.setCanceled(true);
						if(!pixelmon.getPokemonName().equals("Rotom"))
						{
							player.sendMessage(new TextComponentString(TextFormatting.RED + "This item only works Rotom!"));
							return;
						}
						//updatetransformed
						//form0 = normal, form1 = heat, form2 = wash, form3 = frost, form4 = fan, form5 = mow
						if(nbt.getInteger("RotomItem") == 0)
						{
							pixelmon.loadMoveset();
							if(pixelmon.getForm() == 0)
							{
								player.sendMessage(new TextComponentString(TextFormatting.RED + "Rotom is already Normal form!"));
								return;
							}
							else
							{
								pixelmon.setForm(0);
								Attack attackToRemove = null;
								Moveset pixelmonMoves = pixelmon.getMoveset();
								for(Attack att : pixelmonMoves)
								{
									if(att.baseAttack.getUnLocalizedName().equals("Hydro Pump"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Overheat"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Blizzard"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Air Slash"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Leaf Storm"))
									{
										attackToRemove = att;
									}
								}
								if(attackToRemove != null)
								{
									pixelmonMoves.remove(attackToRemove);
								}
								if(pixelmonMoves.isEmpty())
								{
									pixelmonMoves.add(new Attack("ThunderShock"));
								}
								pixelmon.setMoveset(pixelmonMoves);
						        pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });
								pixelmon.updateTransformed();
								player.sendMessage(new TextComponentString(TextFormatting.YELLOW + "Rotom was changed to Normal form!"));
							}
						}
						if(nbt.getInteger("RotomItem") == 1)
						{
							if(pixelmon.getForm() == 1)
							{
								player.sendMessage(new TextComponentString(TextFormatting.RED + "Rotom is already Heat form!"));
								return;
							}
							else
							{
								Attack attackToRemove = null;
								Moveset pixelmonMoves = pixelmon.getMoveset();
								for(Attack att : pixelmonMoves)
								{
									if(att.baseAttack.getUnLocalizedName().equals("Hydro Pump"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Blizzard"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Air Slash"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Leaf Storm"))
									{
										attackToRemove = att;
									}
								}
								if(attackToRemove != null)
								{
									pixelmonMoves.remove(attackToRemove);
								}
								pixelmon.setMoveset(pixelmonMoves);
						        pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });								
								
								pixelmon.setForm(1);
								Attack moveToLearn = new Attack("Overheat");
								if (pixelmon.getMoveset().size() >= 4)
						        {
									Pixelmon.network.sendTo(new OpenReplaceMoveScreen(pixelmon.getPokemonId(), moveToLearn.baseAttack.attackIndex, 0, pixelmon.getLvl().getLevel()),player);
						        }
								else
								{
									pixelmon.getMoveset().add(moveToLearn);
							        player.sendMessage(new TextComponentString(TextFormatting.GRAY + pixelmon.getPokemonName() + " learned " + moveToLearn.baseAttack.getLocalizedName() + "!"));
								}
								pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });
								pixelmon.updateTransformed();
								player.sendMessage(new TextComponentString(TextFormatting.YELLOW + "Rotom was changed to Heat form!"));
							}
						}
						if(nbt.getInteger("RotomItem") == 2)
						{
							if(pixelmon.getForm() == 2)
							{
								player.sendMessage(new TextComponentString(TextFormatting.RED + "Rotom is already Wash form!"));
								return;
							}
							else
							{
								Attack attackToRemove = null;
								Moveset pixelmonMoves = pixelmon.getMoveset();
								for(Attack att : pixelmonMoves)
								{
									if(att.baseAttack.getUnLocalizedName().equals("Overheat"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Blizzard"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Air Slash"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Leaf Storm"))
									{
										attackToRemove = att;
									}
								}
								if(attackToRemove != null)
								{
									pixelmonMoves.remove(attackToRemove);
								}
								pixelmon.setMoveset(pixelmonMoves);
						        pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });								
								
								pixelmon.setForm(2);
								Attack moveToLearn = new Attack("Hydro Pump");
								if (pixelmon.getMoveset().size() >= 4)
						        {
									Pixelmon.network.sendTo(new OpenReplaceMoveScreen(pixelmon.getPokemonId(), moveToLearn.baseAttack.attackIndex, 0, pixelmon.getLvl().getLevel()),player);
						        }
								else
								{
									pixelmon.getMoveset().add(moveToLearn);
							        player.sendMessage(new TextComponentString(TextFormatting.GRAY + pixelmon.getPokemonName() + " learned " + moveToLearn.baseAttack.getLocalizedName() + "!"));
								}						        
								pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });
								pixelmon.updateTransformed();
								player.sendMessage(new TextComponentString(TextFormatting.YELLOW + "Rotom was changed to Wash form!"));
							}
						}
						if(nbt.getInteger("RotomItem") == 3)
						{
							if(pixelmon.getForm() == 3)
							{
								player.sendMessage(new TextComponentString(TextFormatting.RED + "Rotom is already Frost form!"));
								return;
							}
							else
							{
								Attack attackToRemove = null;
								Moveset pixelmonMoves = pixelmon.getMoveset();
								for(Attack att : pixelmonMoves)
								{
									if(att.baseAttack.getUnLocalizedName().equals("Overheat"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Hydro Pump"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Air Slash"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Leaf Storm"))
									{
										attackToRemove = att;
									}
								}
								if(attackToRemove != null)
								{
									pixelmonMoves.remove(attackToRemove);
								}
								pixelmon.setMoveset(pixelmonMoves);
						        pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });								
								
								pixelmon.setForm(3);
								Attack moveToLearn = new Attack("Blizzard");
								if (pixelmon.getMoveset().size() >= 4)
						        {
									Pixelmon.network.sendTo(new OpenReplaceMoveScreen(pixelmon.getPokemonId(), moveToLearn.baseAttack.attackIndex, 0, pixelmon.getLvl().getLevel()),player);
						        }
								else
								{
									pixelmon.getMoveset().add(moveToLearn);
							        player.sendMessage(new TextComponentString(TextFormatting.GRAY + pixelmon.getPokemonName() + " learned " + moveToLearn.baseAttack.getLocalizedName() + "!"));
								}
								pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });
								pixelmon.updateTransformed();
								player.sendMessage(new TextComponentString(TextFormatting.YELLOW + "Rotom was changed to Frost form!"));
							}
						}
						if(nbt.getInteger("RotomItem") == 4)
						{
							if(pixelmon.getForm() == 4)
							{
								player.sendMessage(new TextComponentString(TextFormatting.RED + "Rotom is already Fan form!"));
								return;
							}
							else
							{
								Attack attackToRemove = null;
								Moveset pixelmonMoves = pixelmon.getMoveset();
								for(Attack att : pixelmonMoves)
								{
									if(att.baseAttack.getUnLocalizedName().equals("Overheat"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Hydro Pump"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Blizzard"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Leaf Storm"))
									{
										attackToRemove = att;
									}
								}
								if(attackToRemove != null)
								{
									pixelmonMoves.remove(attackToRemove);
								}
								pixelmon.setMoveset(pixelmonMoves);
						        pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });								
								
								pixelmon.setForm(4);
								Attack moveToLearn = new Attack("Air Slash");
								if (pixelmon.getMoveset().size() >= 4)
						        {
									Pixelmon.network.sendTo(new OpenReplaceMoveScreen(pixelmon.getPokemonId(), moveToLearn.baseAttack.attackIndex, 0, pixelmon.getLvl().getLevel()),player);
						        }
								else
								{
									pixelmon.getMoveset().add(moveToLearn);
							        player.sendMessage(new TextComponentString(TextFormatting.GRAY + pixelmon.getPokemonName() + " learned " + moveToLearn.baseAttack.getLocalizedName() + "!"));
								}
								pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });
								pixelmon.updateTransformed();
								player.sendMessage(new TextComponentString(TextFormatting.YELLOW + "Rotom was changed to Fan form!"));
							}
						}
						if(nbt.getInteger("RotomItem") == 5)
						{
							if(pixelmon.getForm() == 5)
							{
								player.sendMessage(new TextComponentString(TextFormatting.RED + "Rotom is already Mow form!"));
								return;
							}
							else
							{
								Attack attackToRemove = null;
								Moveset pixelmonMoves = pixelmon.getMoveset();
								for(Attack att : pixelmonMoves)
								{
									if(att.baseAttack.getUnLocalizedName().equals("Overheat"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Hydro Pump"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Blizzard"))
									{
										attackToRemove = att;
									}
									if(att.baseAttack.getUnLocalizedName().equals("Air Slash"))
									{
										attackToRemove = att;
									}
								}
								if(attackToRemove != null)
								{
									pixelmonMoves.remove(attackToRemove);
								}
								pixelmon.setMoveset(pixelmonMoves);
						        pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });								
								
								pixelmon.setForm(5);
								Attack moveToLearn = new Attack("Leaf Storm");
								if (pixelmon.getMoveset().size() >= 4)
						        {
									Pixelmon.network.sendTo(new OpenReplaceMoveScreen(pixelmon.getPokemonId(), moveToLearn.baseAttack.attackIndex, 0, pixelmon.getLvl().getLevel()),player);
						        }
								else
								{
									pixelmon.getMoveset().add(moveToLearn);
							        player.sendMessage(new TextComponentString(TextFormatting.GRAY + pixelmon.getPokemonName() + " learned " + moveToLearn.baseAttack.getLocalizedName() + "!"));
								}
								pixelmon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });
								pixelmon.updateTransformed();
								player.sendMessage(new TextComponentString(TextFormatting.YELLOW + "Rotom was changed to Mow form!"));
							}
						}
					}
				}
			}
		}
	}
}
