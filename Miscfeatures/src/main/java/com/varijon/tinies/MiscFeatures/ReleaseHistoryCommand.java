package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.List;

import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
import com.pixelmonmod.pixelmon.config.PixelmonItems;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Moveset;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

public class ReleaseHistoryCommand implements ICommand {

	private List aliases;
	public ReleaseHistoryCommand()
	{
	   this.aliases = new ArrayList();
	   this.aliases.add("releasehistory");
	}
	
	@Override
	public int compareTo(ICommand arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "releasehistory";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		// TODO Auto-generated method stub
		return "releasehistory";
	}

	@Override
	public List<String> getAliases() {
		// TODO Auto-generated method stub
		return this.aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException 
	{
		if(sender.canUseCommand(4, "miscfeatures.releasehistory"))
		{
			if(args.length == 0)
			{
				sender.sendMessage(new TextComponentString(TextFormatting.RED + "Usage: /releasehistory [player] <pokemon>"));
				sender.sendMessage(new TextComponentString(TextFormatting.RED + "If <pokemon> is left out, it will list everything"));							
				return;
			}
			if(sender instanceof EntityPlayerMP)
			{
				EntityPlayerMP player = (EntityPlayerMP) sender;
				if(args.length == 1)
				{
					for(EntityPlayerMP targetPlayer : server.getPlayerList().getPlayers())
					{
						if(targetPlayer.getName().equalsIgnoreCase(args[0]))
						{
							if(DeletedPixelmonHandler.deletedPixelmon.containsKey(targetPlayer.getUniqueID().toString()))
							{
								int counter = 1;
								player.sendMessage(new TextComponentString(TextFormatting.GOLD + "Showing release history for " + TextFormatting.RED + targetPlayer.getName() + TextFormatting.GOLD + ":"));
								for(DeletedPixelmon delPixelmon : DeletedPixelmonHandler.deletedPixelmon.get(targetPlayer.getUniqueID().toString()))
								{
									TextComponentTranslation chatTrans = new TextComponentTranslation("", new Object());
									chatTrans.appendSibling(new TextComponentString(TextFormatting.GREEN + "" + counter + ". "));
									chatTrans.appendSibling(getItem(delPixelmon.GetPokemonData(), player).getTextComponent());
									chatTrans.appendSibling(new TextComponentString(TextFormatting.YELLOW + " at"));
									chatTrans.appendSibling(new TextComponentString(TextFormatting.GREEN + " " + delPixelmon.GetTimeStamp()));
									player.sendMessage(chatTrans);
									counter++;
								}
								return;
							}							
							sender.sendMessage(new TextComponentString(TextFormatting.RED + targetPlayer.getName() + " has no recent released pokemon!"));
							return;
						}
					}
					sender.sendMessage(new TextComponentString(TextFormatting.RED + "Player not found!"));
					return;
				}
				if(args.length == 2)
				{
					for(EntityPlayerMP targetPlayer : server.getPlayerList().getPlayers())
					{
						if(targetPlayer.getName().equalsIgnoreCase(args[0]))
						{
							if(DeletedPixelmonHandler.deletedPixelmon.containsKey(targetPlayer.getUniqueID().toString()))
							{
								int counter = 1;
								player.sendMessage(new TextComponentString(TextFormatting.GOLD + "Showing release history for " + TextFormatting.RED + targetPlayer.getName() + TextFormatting.GOLD + " and " + TextFormatting.RED + args[1] + TextFormatting.GOLD + ":"));
								for(DeletedPixelmon delPixelmon : DeletedPixelmonHandler.deletedPixelmon.get(targetPlayer.getUniqueID().toString()))
								{
									if(delPixelmon.GetPokemonData().getString("Name").equalsIgnoreCase(args[1]))
									{
										TextComponentTranslation chatTrans = new TextComponentTranslation("", new Object());
										chatTrans.appendSibling(new TextComponentString(TextFormatting.GREEN + "" + counter + ". "));
										chatTrans.appendSibling(getItem(delPixelmon.GetPokemonData(), player).getTextComponent());
										chatTrans.appendSibling(new TextComponentString(TextFormatting.YELLOW + " at"));
										chatTrans.appendSibling(new TextComponentString(TextFormatting.GREEN + " " + delPixelmon.GetTimeStamp()));
										player.sendMessage(chatTrans);
									}
									counter++;
								}
								return;
							}							
							sender.sendMessage(new TextComponentString(TextFormatting.RED + "No pokemon found for " + targetPlayer.getName()));
							return;
						}
					}
					sender.sendMessage(new TextComponentString(TextFormatting.RED + "Player not found!"));
					return;
				}
			}
			sender.sendMessage(new TextComponentString(TextFormatting.RED + "Usage: /releasehistory [player] <pokemon>"));			
			return;
		}
		else
		{
			sender.sendMessage(new TextComponentString(TextFormatting.RED + "You don't have permission to use this command"));
			return;
		}

	}


	public ItemStack getItem(NBTTagCompound pixelmonData, EntityPlayerMP player)
	{
				
		EntityPixelmon pixelmon = (EntityPixelmon) PixelmonEntityList.createEntityFromNBT(pixelmonData, player.world);
		if (pixelmon != null)
		{
			String pixelmonName = pixelmon.getPokemonName();
			String pixelmonAbility = pixelmon.getAbility().getName();
			String pixelmonGender = pixelmon.gender.name();
			String pixelmonGrowth = pixelmon.getGrowth().name();
			String pixelmonNature = pixelmon.getNature().name();
			int pixelmonFriendship = pixelmon.friendship.getFriendship();
			int pixelmonLevel = pixelmon.getLvl().getLevel(); 
			String pixelmonShiny = getShiny(pixelmon.getIsShiny());
			
			if(pixelmon.isEgg)
			{
				pixelmonName += " Egg";
				pixelmonAbility = "???";
				pixelmonGender = "???";
				pixelmonGrowth = "???";
				pixelmonNature = "???";
				pixelmonShiny = "???";
			}
								
			int pixelmonIVHP = pixelmon.stats.ivs.HP;
			int pixelmonIVAttack = pixelmon.stats.ivs.Attack;
			int pixelmonIVDefense = pixelmon.stats.ivs.Defence;
			int pixelmonIVSpAtt = pixelmon.stats.ivs.SpAtt;
			int pixelmonIVSpDef = pixelmon.stats.ivs.SpDef;
			int pixelmonIVSpeed = pixelmon.stats.ivs.Speed;
			
			int pixelmonEVHP = pixelmon.stats.evs.hp;
			int pixelmonEVAttack = pixelmon.stats.evs.attack;
			int pixelmonEVDefense = pixelmon.stats.evs.defence;
			int pixelmonEVSpAtt = pixelmon.stats.evs.specialAttack;
			int pixelmonEVSpDef = pixelmon.stats.evs.specialDefence;
			int pixelmonEVSpeed = pixelmon.stats.evs.speed;
			
			
			int pixelmonStatsHP = pixelmon.stats.hp;
			int pixelmonStatsAttack = pixelmon.stats.attack;
			int pixelmonStatsDefense = pixelmon.stats.defence;
			int pixelmonStatsSpAtt = pixelmon.stats.specialAttack;
			int pixelmonStatsSpDef = pixelmon.stats.specialDefence;
			int pixelmonStatsSpeed = pixelmon.stats.speed;
			
			Moveset pixelmonMoves = pixelmon.getMoveset();

			ItemStack item = new ItemStack(PixelmonItems.itemPixelmonSprite, 1);
			item.setTagCompound(new NBTTagCompound());
									
			
			NBTTagCompound tags = item.getTagCompound();
			tags.setTag("display", new NBTTagCompound());
			
			String dexNumber = pixelmon.baseStats.nationalPokedexNumber + "";
			if(dexNumber.length() == 2)
			{
				dexNumber = "0" + dexNumber;
			}
			if(dexNumber.length() == 1)
			{
				dexNumber = "00" + dexNumber;
			}
			if(!pixelmon.isEgg)
			{
				if(pixelmon.getIsShiny())
				{
					tags.setString("SpriteName", "pixelmon:sprites/shinypokemon/" + dexNumber);						
				}
				else
				{
					tags.setString("SpriteName", "pixelmon:sprites/pokemon/" + dexNumber);
				}
			}
			else
			{
				tags.setString("SpriteName", "pixelmon:sprites/eggs/egg1");
			}
			tags.getCompoundTag("display").setString("Name", TextFormatting.GOLD + pixelmonName);
			
			NBTTagList loreList = new NBTTagList();
			
			loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Nature: " + TextFormatting.GREEN + pixelmonNature));
			loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Ability: " + TextFormatting.GREEN + pixelmonAbility));
			loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Level: " + TextFormatting.GREEN + pixelmonLevel));
			loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Gender: " + TextFormatting.GREEN + pixelmonGender));
			loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Shiny: " + TextFormatting.YELLOW + pixelmonShiny));
			loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Size: " + TextFormatting.GREEN + pixelmonGrowth));
			loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Friendship: " + TextFormatting.GREEN + pixelmonFriendship));
			
			loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Stats:"));
			loreList.appendTag(new NBTTagString(TextFormatting.LIGHT_PURPLE + "HP: IV: " + TextFormatting.GREEN + pixelmonIVHP + TextFormatting.LIGHT_PURPLE + " EV: " + TextFormatting.GREEN + pixelmonEVHP + TextFormatting.LIGHT_PURPLE + " Stat: " + TextFormatting.GREEN + pixelmonStatsHP));
			loreList.appendTag(new NBTTagString(TextFormatting.RED + "Atk: IV: " + TextFormatting.GREEN + pixelmonIVAttack + TextFormatting.RED + " EV: " + TextFormatting.GREEN + pixelmonEVAttack + TextFormatting.RED + " Stat: " + TextFormatting.GREEN + pixelmonStatsAttack));
			loreList.appendTag(new NBTTagString(TextFormatting.GOLD + "Def: IV: " + TextFormatting.GREEN + pixelmonIVDefense + TextFormatting.GOLD + " EV: " + TextFormatting.GREEN + pixelmonEVDefense + TextFormatting.GOLD + " Stat: " + TextFormatting.GREEN + pixelmonStatsDefense));
			loreList.appendTag(new NBTTagString(TextFormatting.DARK_PURPLE + "SpAtt: IV: " + TextFormatting.GREEN + pixelmonIVSpAtt + TextFormatting.DARK_PURPLE + " EV: " + TextFormatting.GREEN + pixelmonEVSpAtt + TextFormatting.DARK_PURPLE + " Stat: " + TextFormatting.GREEN + pixelmonStatsSpAtt));
			loreList.appendTag(new NBTTagString(TextFormatting.YELLOW + "SpDef: IV: " + TextFormatting.GREEN + pixelmonIVSpDef + TextFormatting.YELLOW + " EV: " + TextFormatting.GREEN + pixelmonEVSpDef + TextFormatting.YELLOW + " Stat: " + TextFormatting.GREEN + pixelmonStatsSpDef));
			loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Speed: IV: " + TextFormatting.GREEN + pixelmonIVSpeed + TextFormatting.AQUA + " EV: " + TextFormatting.GREEN + pixelmonEVSpeed + TextFormatting.AQUA + " Stat: " + TextFormatting.GREEN + pixelmonStatsSpeed));
			
			loreList.appendTag(new NBTTagString(TextFormatting.DARK_PURPLE + "Moves:"));

			if(!pixelmon.isEgg)
			{
				for(Attack move : pixelmonMoves)
				{
					loreList.appendTag(new NBTTagString(TextFormatting.GREEN + move.baseAttack.getLocalizedName()));
				}
			}
			else
			{
				loreList.appendTag(new NBTTagString(TextFormatting.GREEN + "???"));
			}
			
			tags.getCompoundTag("display").setTag("Lore", loreList);
						
			item.setTagCompound(tags);
			return item;
		}
		else
		{
			return null;
		}
	}

	public String getShiny(boolean shinyID)
	{
		String returnShiny = "";
		
		if(shinyID)
		{
			returnShiny = "Yes";			
		}
		else
		{
			returnShiny = "No";
		}
		
		return returnShiny;
	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender) 
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index) {
		// TODO Auto-generated method stub
		return false;
	}

}
