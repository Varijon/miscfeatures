package com.varijon.tinies.MiscFeatures;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.pixelmonmod.pixelmon.api.events.EconomyEvent;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EconomyLogHandler 
{
	
	public EconomyLogHandler()
	{
	}
	
	@SubscribeEvent(receiveCanceled = true)
	public void onEconomyTransaction(EconomyEvent.PostTransactionEvent event)
	{
		String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		String dateTime = new SimpleDateFormat("HH:mm:ss").format(new Date());
		int change = event.newBalance - event.oldBalance;
		String entry = dateTime + ", Player: " + event.player.getName() + ", Currency Change: " + change + ", New Balance: " + (event.newBalance);
		writeFile(entry, date);
	}
	
	public static void initConfig()
	{
		String basefolder = new File("").getAbsolutePath();
        String source = basefolder + "\\economylog";
		File dir = new File(source);
		if(!dir.exists())
		{
			dir.mkdirs();
		}
		else
		{
			dir.mkdirs();
		}
	}
	public void writeFile(String text, String filename)
	{
		String basefolder = new File("").getAbsolutePath();
        String source = basefolder + "\\economylog";
		String fileName = source + "\\" + filename + ".txt";

        try 
        {
            FileWriter fileWriter = new FileWriter(fileName,true);

            // Always wrap FileWriter in BufferedWriter.
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            // Note that write() does not automatically
            // append a newline character.
            bufferedWriter.append(text);
            bufferedWriter.newLine();

            bufferedWriter.close();
        }
        catch(IOException ex) 
        {
            System.out.println("Error writing to file '" + fileName + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        }
	}
}
