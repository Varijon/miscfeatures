package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;

import net.minecraft.nbt.NBTTagCompound;

public class LootItem 
{
	private ArrayList<NBTTagCompound> itemData;
	private float rarity;
	private int lowAmount;
	private int highAmount;
	
	public LootItem(ArrayList<NBTTagCompound> pItemData, float pRarity, int pLowAmount, int pHighAmount)
	{
		itemData = pItemData;
		rarity = pRarity;
		lowAmount = pLowAmount;
		highAmount = pHighAmount;
	}
	
	public ArrayList<NBTTagCompound> GetItemData()
	{
		return itemData;
	}
	
	public float GetRarity()
	{
		return rarity;
	}
	public int GetLowAmount()
	{
		return lowAmount;
	}
	public int GetHighAmount()
	{
		return highAmount;
	}
}
