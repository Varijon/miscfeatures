package com.varijon.tinies.MiscFeatures;

public class LegendSpawn 
{
	long timeSpawned;
	String pokemonName;
	String UUID;
	
	public LegendSpawn(long datetime, String pokemonName, String uuid)
	{
		this.timeSpawned = datetime;
		this.pokemonName = pokemonName;
		this.UUID = uuid;
	}
	
	public long getTimeSpawned() 
	{
		return timeSpawned;
	}
	public void setTimeSpawned(long timeSpawned) 
	{
		this.timeSpawned = timeSpawned;
	}
	public String getPokemonName() 
	{
		return pokemonName;
	}
	public void setPokemonName(String pokemonName) 
	{
		this.pokemonName = pokemonName;
	}
	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}


	
}
