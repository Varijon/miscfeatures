package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;

public class LootBox 
{
	private String identifier;
	private ArrayList<LootItem> lst_Items;
	
	public LootBox()
	{
		lst_Items = new ArrayList<LootItem>();
	}
	
	public String GetIdentifier()
	{
		return identifier;
	}
	public void SetIdentifier(String pIdentifier)
	{
		identifier = pIdentifier;
	}
		
	public ArrayList<LootItem> GetItems()
	{
		return lst_Items;
	}
	
	public void AddItem(LootItem pItem)
	{
		lst_Items.add(pItem);
	}
}
