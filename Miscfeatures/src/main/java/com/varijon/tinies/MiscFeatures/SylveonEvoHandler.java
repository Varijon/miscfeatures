package com.varijon.tinies.MiscFeatures;

import java.util.Random;

import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.evolution.Evolution;
import com.pixelmonmod.pixelmon.enums.EnumType;
import com.pixelmonmod.pixelmon.storage.PlayerNotLoadedException;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.EntityInteract;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class SylveonEvoHandler 
{
	Random rng;
	
	public SylveonEvoHandler()
	{
		rng = new Random();
	}
	
	@SubscribeEvent
	public void onPlayerInteract(EntityInteract event) throws PlayerNotLoadedException
	{
		if(event.getTarget() instanceof EntityPixelmon)
		{
			if(event.getHand() != EnumHand.MAIN_HAND)
			{
				return;
			}
			EntityPixelmon pixelmon = (EntityPixelmon) event.getTarget();
			EntityPlayerMP player = (EntityPlayerMP) event.getEntityPlayer();
			
			
			if(!pixelmon.belongsTo(player) && !pixelmon.isInRanchBlock)
			{
				return;
			}
			
			if(player.getHeldItem(EnumHand.MAIN_HAND) != null)
			{
				ItemStack item = player.getHeldItem(EnumHand.MAIN_HAND);
				NBTTagCompound nbt = item.getTagCompound();
				if(nbt != null)
				{
					if(nbt.hasKey("isSylveonItem"))
					{
						event.setCanceled(true);
						if(!pixelmon.getPokemonName().equals("Eevee"))
						{
							player.sendMessage(new TextComponentString(TextFormatting.RED + "This item only works Eevee!"));
							return;
						}
						if(pixelmon.friendship.getFriendship() >= 220)
						{
							boolean hasFairyMove = false;
							for(Attack att : pixelmon.getMoveset())
							{
								if(att.baseAttack.attackType == EnumType.Fairy)
								{
									hasFairyMove = true;
								}
							}
							if(hasFairyMove)
							{
								Evolution sylveonEvo = null;
								for(Evolution evo : pixelmon.baseStats.evolutions)
								{
									if(evo.to.name.equals("Sylveon"))
									{
										sylveonEvo = evo;
									}
								}
								pixelmon.startEvolution(sylveonEvo);
								if(player.getHeldItem(EnumHand.MAIN_HAND).getCount() > 1)
								{
									player.getHeldItem(EnumHand.MAIN_HAND).setCount(player.getHeldItem(EnumHand.MAIN_HAND).getCount() - 1);
								}
								else
								{
									player.inventory.removeStackFromSlot(player.inventory.currentItem);
								}
								player.inventoryContainer.detectAndSendChanges();
							}
							else
							{
								player.sendMessage(new TextComponentString(TextFormatting.RED + "This item requires Eevee to have a Fairy move!"));								
							}
						}
						else
						{
							player.sendMessage(new TextComponentString(TextFormatting.RED + "This item requires Eevee to have 220+ Happiness!"));
							return;
						}
					}
				}
			}
//			if(Block.getBlockFromItem(item.getItem()) != null)
//			{
//				if(Block.getBlockFromItem(item.getItem()) == PixelUtilitiesAdditions.boxBlock)
//				{
//					NBTTagCompound nbt = item.getTagCompound();
//					if(nbt != null)
//					{
//						if(nbt.hasKey("identifier"))
//						{
		}
	}
}
