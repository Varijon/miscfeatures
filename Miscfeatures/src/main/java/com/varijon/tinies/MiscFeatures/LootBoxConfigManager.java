package com.varijon.tinies.MiscFeatures;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.ArrayList;

import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;

public class LootBoxConfigManager 
{

	public static ArrayList<LootBox> lst_LootBoxes;
	
	public static void initConfig()
	{
		String basefolder = new File("").getAbsolutePath();
        String source = basefolder + "\\config\\LootBox";
		File dir = new File(source);
		if(dir.exists())
		{
			if(dir.listFiles().length > 0)
			{
				
			}
			else
			{
				writeDefaultFile();
			}
		}
		else
		{
			dir.mkdirs();
			writeDefaultFile();
		}
	}
	
	public static void loadConfig() throws NBTException
	{
		lst_LootBoxes = new ArrayList<LootBox>();
		String basefolder = new File("").getAbsolutePath();
        String source = basefolder + "\\config\\LootBox";
        File dir = new File(source);
        
        File[] configFiles = dir.listFiles();
        
        for(File configFile : configFiles)
        {
        	// This will reference one line at a time
            String line = null;
            
            String fileName = configFile.getAbsolutePath();
            
            try 
            {
                // FileReader reads text files in the default encoding.
                //FileReader fileReader =  new FileReader(fileName);

            	Reader reader = new InputStreamReader(new FileInputStream(fileName), "utf-8");
            	BufferedReader bufferedReader = new BufferedReader(reader);
            	
                // Always wrap FileReader in BufferedReader.
                //BufferedReader bufferedReader = new BufferedReader(fileReader);
                
            	//String lootBoxName = configFile.getName().replace(".txt", "");
            	LootBox lootbox = new LootBox();
            	lootbox.SetIdentifier(configFile.getName().replace(".txt", ""));
                int count = 0;
                ArrayList<NBTTagCompound> itemData = new ArrayList<NBTTagCompound>();
                int lowQuantity = 0;
                int highQuantity = 0;
                float rarity = 0;
                boolean lookMoreItemData = true;
            	
                while((line = bufferedReader.readLine()) != null) 
                {
                	if(line.split("=")[0].equals("itemNBT") && lookMoreItemData)
                	{
                		itemData.add((NBTTagCompound) JsonToNBT.getTagFromJson(line.split("=")[1]));
                		//count++;
                	}
                	if(line.split("=")[0].equals("quantity"))
                	{
                		lookMoreItemData = false;
                		String s = line.split("=")[1];
                		if(s.split("-").length > 1)
                		{
                			lowQuantity = Integer.parseInt(s.split("-")[0]);
                			highQuantity = Integer.parseInt(s.split("-")[1]);
                		}
                		else
                		{
                			lowQuantity = Integer.parseInt(s);
                			highQuantity = Integer.parseInt(s);
                		}
                		count++;
                	}
                	if(line.split("=")[0].equals("rarity"))
                	{
                		rarity = Float.parseFloat(line.split("=")[1].replace("%", ""));
                		count++;
                	}
                	if(count == 2)
                	{
                		count = 0;
                		lootbox.AddItem(new LootItem(itemData, rarity, lowQuantity, highQuantity));
                		lookMoreItemData = true;
                		itemData = new ArrayList<NBTTagCompound>();
                	}
                }
                lst_LootBoxes.add(lootbox);

                // Always close files.
                bufferedReader.close();         
            }
            catch(FileNotFoundException ex) 
            {
                System.out.println("Unable to open file '" + fileName + "'");                
            }
            catch(IOException ex) 
            {
                System.out.println("Error reading file '" + fileName + "'");         
            }
//            for(LootBox lb : lst_LootBoxes)
//            {
//            	System.out.println("Lootbox name: " + lb.GetIdentifier());
//            	for(LootItem li : lb.GetItems())
//            	{
//            		System.out.println("LootItems: " + li.GetItemData());
//            	}
//            }
        }
        
        
	}
	
	private static void writeDefaultFile()
	{
		String basefolder = new File("").getAbsolutePath();
        String source = basefolder + "\\config\\LootBox";
		String fileName = source + "\\defaultbox.txt";

        try 
        {
            //FileWriter fileWriter = new FileWriter(fileName);

            // Always wrap FileWriter in BufferedWriter.
            //BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(
        			new FileOutputStream(fileName,true), "UTF8"));
            
            // Note that write() does not automatically
            // append a newline character.
            bufferedWriter.write("itemNBT={id:\"minecraft:dirt\",tag:{display:{Name:\"Special Dirt\"}}}");
            bufferedWriter.newLine();
            bufferedWriter.write("quantity=1-4");
            bufferedWriter.newLine();
            bufferedWriter.write("rarity=1.0");

            bufferedWriter.close();
        }
        catch(IOException ex) 
        {
            System.out.println("Error writing to file '" + fileName + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        }
	}
	public static void WriteNewItem(NBTTagCompound itemData, String quantity, String rarity, String lootBox)
	{
		String basefolder = new File("").getAbsolutePath();
        String source = basefolder + "\\config\\LootBox";
		String fileName = source + "\\" + lootBox + ".txt";

        try 
        {
            //FileWriter fileWriter = new FileWriter(fileName,true);

            // Always wrap FileWriter in BufferedWriter.
            //BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(
        			new FileOutputStream(fileName,true), "UTF8"));
            
            // Note that write() does not automatically
            // append a newline character.
            bufferedWriter.newLine();
            bufferedWriter.append("itemNBT=" + itemData.toString());
            bufferedWriter.newLine();
            bufferedWriter.append("quantity=" + quantity);
            bufferedWriter.newLine();
            bufferedWriter.append("rarity=" + rarity);

            bufferedWriter.close();
        }
        catch(IOException ex) 
        {
            System.out.println("Error writing to file '" + fileName + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        }
	}
	
	public static LootBox GetFromIdentifier(String pIdentifier)
	{
		for(LootBox lootbox : lst_LootBoxes)
		{
			if(lootbox.GetIdentifier().equals(pIdentifier))
			{
				return lootbox;
			}
		}
		return null;
	}
}
