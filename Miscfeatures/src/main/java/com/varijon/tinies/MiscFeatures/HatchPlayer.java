package com.varijon.tinies.MiscFeatures;

public class HatchPlayer 
{
	double lastX;
	double lastZ;
	int eggSteps;
	
	public HatchPlayer(double lastX, double lastZ)
	{
		this.lastX = lastX;
		this.lastZ = lastZ;
	}

	public double getLastX() {
		return lastX;
	}

	public void setLastX(double LastX) {
		this.lastX = LastX;
	}

	public double getLastZ() {
		return lastZ;
	}

	public void setLastZ(double lastZ) {
		this.lastZ = lastZ;
	}
	
	public int getEggSteps(){
		return this.eggSteps;
	}

	public void incrementEggSteps() {
		this.eggSteps++;
	}

	public void resetEggSteps() {
		this.eggSteps = 0;
		
	}
	
}
