//package com.varijon.tinies.MiscFeatures;
//
//import java.io.File;
//import java.io.IOException;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.net.URLConnection;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.UUID;
//
//import com.pixelmonmod.pixelmon.Pixelmon;
//import com.pixelmonmod.pixelmon.PixelmonMethods;
//import com.pixelmonmod.pixelmon.api.PixelmonApi;
//import com.pixelmonmod.pixelmon.battles.BattleRegistry;
//import com.pixelmonmod.pixelmon.battles.attacks.Attack;
//import com.pixelmonmod.pixelmon.battles.attacks.AttackBase;
//import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
//import com.pixelmonmod.pixelmon.battles.controller.participants.BattleParticipant;
//import com.pixelmonmod.pixelmon.blocks.PixelmonBlock;
//import com.pixelmonmod.pixelmon.blocks.ranch.RanchBounds;
//import com.pixelmonmod.pixelmon.comm.PixelmonData;
//import com.pixelmonmod.pixelmon.comm.PixelmonMovesetData;
//import com.pixelmonmod.pixelmon.comm.PixelmonPlayerTracker;
//import com.pixelmonmod.pixelmon.comm.packetHandlers.BossDropPacket;
//import com.pixelmonmod.pixelmon.comm.packetHandlers.npc.NPCLearnMove;
//import com.pixelmonmod.pixelmon.config.PixelmonBlocks;
//import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
//import com.pixelmonmod.pixelmon.config.PixelmonItems;
//import com.pixelmonmod.pixelmon.database.DatabaseMoves;
//import com.pixelmonmod.pixelmon.database.DatabaseStats;
//import com.pixelmonmod.pixelmon.entities.npcs.NPCShopkeeper;
//import com.pixelmonmod.pixelmon.entities.npcs.NPCTutor;
//import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopItem;
//import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopItemWithVariation;
//import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopkeeperData;
//import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
//import com.pixelmonmod.pixelmon.items.PixelmonItem;
//import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
//import com.pixelmonmod.pixelmon.storage.PlayerNotLoadedException;
//import com.pixelmonmod.pixelmon.storage.PlayerStorage;
//import com.pixelmonmod.pixelmon.storage.PokeballManager;
//
//import net.minecraft.command.CommandException;
//import net.minecraft.command.ICommand;
//import net.minecraft.command.ICommandSender;
//import net.minecraft.command.server.CommandMessageRaw;
//import net.minecraft.entity.player.EntityPlayer;
//import net.minecraft.entity.player.EntityPlayerMP;
//import net.minecraft.inventory.Container;
//import net.minecraft.inventory.ContainerChest;
//import net.minecraft.inventory.IInventory;
//import net.minecraft.item.ItemAnvilBlock;
//import net.minecraft.item.ItemStack;
//import net.minecraft.nbt.NBTTagCompound;
//import net.minecraft.server.MinecraftServer;
//import net.minecraft.tileentity.TileEntityChest;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.util.text.TextComponentString;
//import net.minecraft.util.text.TextFormatting;
//import net.minecraft.util.IChatComponent;
//import net.minecraftforge.common.MinecraftForge;
//import net.minecraftforge.event.entity.EntityEvent.EnteringChunk;
//
//public class ResetPixelmonCurrencyCommand implements ICommand {
//
//	private List aliases;
//	public ResetPixelmonCurrencyCommand()
//	{
//	   this.aliases = new ArrayList();
//	   this.aliases.add("resetpokecurrency");
//	}
//	
//	@Override
//	public int compareTo(ICommand arg0) {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	@Override
//	public String getCommandName() {
//		// TODO Auto-generated method stub
//		return "resetpokecurrency";
//	}
//
//	@Override
//	public String getCommandUsage(ICommandSender sender) {
//		// TODO Auto-generated method stub
//		return "resetpokecurrency";
//	}
//
//	@Override
//	public List<String> getCommandAliases() {
//		// TODO Auto-generated method stub
//		return this.aliases;
//	}
//
//	@Override
//	public void processCommand(ICommandSender sender, String[] args) throws CommandException 
//	{
//		if(sender.canCommandSenderUseCommand(4, "miscfeatures.resetpokecurrency"))
//		{
//			if(args.length > 0)
//			{
//				sender.addChatMessage(new TextComponentString(TextFormatting.RED + "Usage: /resetpokecurrency"));						
//				return;
//			}
//			
//			new Thread(new EditPixelmonStorageRunnable()).start();
////			File[] pixelmonPlayers = PixelmonStorage.pokeBallManager.getFileList();
////			
////			int totalfiles = pixelmonPlayers.length;
////			int progress = 0;
////			
////			for(File pixelFile : pixelmonPlayers)
////			{
////				try {
////					PlayerStorage ps = PixelmonStorage.pokeBallManager.getPlayerStorageFileOffline(UUID.fromString(pixelFile.getName().replace(".pk", "")), pixelFile);
////					ps.setCurrency(0);
////					PixelmonStorage.pokeBallManager.savePlayer(ps);
////					progress++;
////					if(progress % 50 == 0)
////					{
////						System.out.println("Progress: " + progress + "/" + totalfiles);
////					}
////				} catch (PlayerNotLoadedException e) {
////					// TODO Auto-generated catch block
////					e.printStackTrace();
////				}
////			}
//			return;
//		}
//		else
//		{
//			sender.addChatMessage(new TextComponentString(TextFormatting.RED + "You don't have permission to use this command"));
//			return;
//		}
//
//	}
//	
//	public class EditPixelmonStorageRunnable implements Runnable 
//	{
//	    public void run() 
//	    {
//	    	File[] pixelmonPlayers = PixelmonStorage.pokeBallManager.getFileList();
//			
//			int totalfiles = pixelmonPlayers.length;
//			int progress = 0;
//			
//			for(File pixelFile : pixelmonPlayers)
//			{
//				try {
//					PlayerStorage ps = PixelmonStorage.pokeBallManager.getPlayerStorageFileOffline(UUID.fromString(pixelFile.getName().replace(".pk", "")), pixelFile);
//					ps.setCurrency(0);
//					PixelmonStorage.pokeBallManager.savePlayer(ps);
//					progress++;
//					if(progress % 50 == 0)
//					{
//						System.out.println("Progress: " + progress + "/" + totalfiles);
//					}
//				} catch (PlayerNotLoadedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//	    }
//	}
//
//	@Override
//	public boolean canCommandSenderUseCommand(ICommandSender sender) {
//		// TODO Auto-generated method stub
//		return true;
//	}
//
//	@Override
//	public List<String> addTabCompletionOptions(ICommandSender sender,
//			String[] args, BlockPos pos) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public boolean isUsernameIndex(String[] args, int index) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//	
////	private String buildTellRaw()
////	{
////		StringBuilder sb = new StringBuilder();
////		
////	}
//	
//}
