package com.varijon.tinies.MiscFeatures.magic;

import java.util.List;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.WorldServer;

public class SlowHomingBolt
{
	int duration;

	int maxDuration;
	Vec3d vec;
	Vec3d pos;
	boolean doneExisting;
	EntityLiving target;
	int soundCount;

	EntityPlayerMP player;
	
	public SlowHomingBolt(int duration, Vec3d vec, Vec3d pos, EntityPlayerMP player) 
	{
		this.duration = 0;
		this.maxDuration = duration;
		this.vec = vec;
		//Adding speed this way doesn't work well with colliding, skips mobs
		//this.vec = new Vec3d(vec.xCoord*3, vec.yCoord*3, vec.zCoord*3);
		this.pos = pos;
		this.player = player;
		doneExisting = false;
		target = null;
		soundCount = 0;
	}
	
	public void doTick()
	{
		//check if homing, else use a different particle.. homing doesn't use vec and makes it look odd
		player.getServerWorld().spawnParticle(EnumParticleTypes.END_ROD, pos.x, pos.y, pos.z, 5, 0.05, 0.05, 0.05, 0, new int[]{});
		Vec3d morePart = new Vec3d(vec.x/10,vec.y/10,vec.z/10);
		Vec3d extraPos = pos.add(morePart);
		for(int x = 0; x <= 10; x++)
		{
			player.getServerWorld().spawnParticle(EnumParticleTypes.END_ROD, extraPos.x, extraPos.y, extraPos.z, 5, 0.05, 0.05, 0.05, 0, new int[]{});
			extraPos = extraPos.add(morePart);
		}
		if(duration > 10 && target == null)
		{
			Vec3d checkPos = pos.add(vec);
			Vec3d minPos = checkPos.addVector(-12.5, -12.5, -12.5);
			Vec3d maxPos = checkPos.addVector(12.5, 12.5, 12.5);
			
			List<EntityLiving> listLiving = player.getServerWorld().getEntitiesWithinAABB(EntityLiving.class, new AxisAlignedBB(minPos.x, minPos.y, minPos.z, maxPos.x, maxPos.y, maxPos.z));
			
			double closest = 500;
			for(EntityLiving living : listLiving)
			{
				if(living.getPositionVector().distanceTo(checkPos) < closest)
				{
					closest = living.getPositionVector().distanceTo(checkPos);
					target = living;
				}
			}
		}
		if(duration >= maxDuration)
		{
			explodeFireball();
			doneExisting = true;

			List<EntityLiving> listSplashLiving = getSplashEntityLiving(player.getServerWorld());
			if(listSplashLiving != null)
			{
				if(!listSplashLiving.isEmpty())
				{
					for(EntityLiving entityliving : listSplashLiving)
					{
						entityliving.attackEntityFrom(DamageSource.causePlayerDamage(player), 5f);
						//entityliving.setFire(5);
					}
				}
			}
		}
		if(checkCollidedWithBlock(player.getServerWorld()))
		{
			// lol this makes it bounce straight back
			
			explodeFireball();
			doneExisting = true;

			List<EntityLiving> listSplashLiving = getSplashEntityLiving(player.getServerWorld());
			if(listSplashLiving != null)
			{
				if(!listSplashLiving.isEmpty())
				{
					for(EntityLiving entityliving : listSplashLiving)
					{
						entityliving.attackEntityFrom(DamageSource.causePlayerDamage(player), 5f);
						//entityliving.setFire(5);
					}
				}
			}
		}
		List<EntityLiving> listLiving = checkCollidedWithEntityLiving(player.getServerWorld());
		if(listLiving != null)
		{
			if(!listLiving.isEmpty())
			{
				for(EntityLiving entityliving : listLiving)
				{
					entityliving.attackEntityFrom(DamageSource.causePlayerDamage(player), 12f);
					//entityliving.setFire(5);
				}
				doneExisting = true;
				explodeFireball();

				List<EntityLiving> listSplashLiving = getSplashEntityLiving(player.getServerWorld());
				if(listSplashLiving != null)
				{
					if(!listSplashLiving.isEmpty())
					{
						for(EntityLiving entityliving : listSplashLiving)
						{
							entityliving.attackEntityFrom(DamageSource.causePlayerDamage(player), 5f);
							//entityliving.setFire(5);
						}
					}
				}
			}
		}
		duration++;
		if(target != null)
		{
			Vec3d tempPos = target.getPositionVector();
			tempPos.addVector(0, 1, 0);
			pos = pos.subtract(pos.subtract(tempPos).normalize());
		}
		else
		{
			pos = pos.add(vec);			
		}
		
	}
	public void doSound()
	{
		if(soundCount == 40)
		{
			player.getServerWorld().playSound(null, new BlockPos(pos.x, pos.y, pos.z), SoundEvents.ENTITY_CAT_AMBIENT, SoundCategory.PLAYERS, 1f, 1f);	
			soundCount = 0;
		}
		soundCount++;
	}
	
	public void explodeFireball()
	{		
		player.getServerWorld().spawnParticle(EnumParticleTypes.END_ROD, pos.x, pos.y, pos.z, 50, 1, 1, 1, 0, new int[]{});
		doneExisting = true;

		player.getServerWorld().playSound(null, new BlockPos(pos.x, pos.y, pos.z), SoundEvents.ENTITY_FIREWORK_BLAST_FAR, SoundCategory.PLAYERS, 2f, 1f);	
	}
	
	public boolean checkCollidedWithBlock(WorldServer world)
	{
		//check each side for a block, then inverse the correct vector
		Vec3d checkPos = pos.add(vec);
		if(world.getBlockState(new BlockPos(checkPos.x, checkPos.y, checkPos.z)).getBlock() != Blocks.AIR)
		{
			return true;
		}
		return false;
	}
	
	public List<EntityLiving> checkCollidedWithEntityLiving(WorldServer world)
	{
		Vec3d checkPos = pos.add(vec);
		Vec3d minPos = checkPos.addVector(-0.2, -0.2, -0.2);
		Vec3d maxPos = checkPos.addVector(0.2, 0.2, 0.2);
		List<EntityLiving> listLiving = world.getEntitiesWithinAABB(EntityLiving.class, new AxisAlignedBB(minPos.x, minPos.y, minPos.z, maxPos.x, maxPos.y, maxPos.z));
		return listLiving;
	}
	
	public List<EntityLiving> getSplashEntityLiving(WorldServer world)
	{
		Vec3d checkPos = pos.add(vec);
		Vec3d minPos = checkPos.addVector(-1.5, -1.5, -1.5);
		Vec3d maxPos = checkPos.addVector(1.5, 1.5, 1.5);
		List<EntityLiving> listLiving = world.getEntitiesWithinAABB(EntityLiving.class, new AxisAlignedBB(minPos.x, minPos.y, minPos.z, maxPos.x, maxPos.y, maxPos.z));
		return listLiving;
	}
	
	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
	public Vec3d getPos() {
		return pos;
	}

	public void setPos(Vec3d pos) {
		this.pos = pos;
	}


	public boolean isDoneExisting() {
		return doneExisting;
	}

	public void setDoneExisting(boolean doneExisting) {
		this.doneExisting = doneExisting;
	}
}
