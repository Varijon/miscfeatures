package com.varijon.tinies.MiscFeatures;

import java.text.DecimalFormat;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.network.play.server.SPacketChat;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ChatType;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;

public class SuperiorElytraHandler 
{
	int counter = 0;
	MinecraftServer server;
	
	public SuperiorElytraHandler()
	{
		server = FMLCommonHandler.instance().getMinecraftServerInstance();
	}

	@SubscribeEvent
	public void onWorldTick (WorldTickEvent event)
	{
		if(event.phase != Phase.END)
		{
			return;
		}
		if(!event.world.getWorldInfo().getWorldName().equals("world"))
		{
			return;
		}
		if(counter == 1)
		{
			for(EntityPlayerMP targetPlayer : server.getPlayerList().getPlayers())
			{
				ItemStack elytra = GetSuperiorElytra(targetPlayer.getArmorInventoryList());
				if(elytra != null)
				{
					NBTTagCompound nbt = elytra.getTagCompound();
					float maxFlightTime = nbt.getFloat("flightTime");
					float flightAcceleration = nbt.getFloat("flightAcceleration");
					float flightTimeRecovery = nbt.getFloat("flightRecovery");
					String flightParticle = nbt.getString("flightParticle");
					float flightTime = nbt.getFloat("flightDuration");
					double partOffset = nbt.getDouble("partOffset");
					double partSpeed = nbt.getDouble("partSpeed");
					int[] extraValue = nbt.getIntArray("extraValue");
					int partCount = nbt.getInteger("partCount");
					boolean depthDiver = nbt.getBoolean("depthDiver");
					
					if(targetPlayer.isInWater() && depthDiver && targetPlayer.isElytraFlying())
					{
						flightAcceleration *= 10;
						targetPlayer.getServerWorld().spawnParticle(EnumParticleTypes.WATER_BUBBLE, targetPlayer.posX, targetPlayer.posY, targetPlayer.posZ, 10, 0.5, 0.5, 0.5, 0, new int[]{});

					}
					
					if(targetPlayer.isElytraFlying() && targetPlayer.isSneaking())
					{
						if(flightTime > maxFlightTime)
						{
							flightTime = maxFlightTime;
						}
						if(flightTime > 0)
						{
							float yaw = targetPlayer.rotationYaw;
							float pitch = targetPlayer.rotationPitch;
							float f = flightAcceleration;
							double motionX = (double)(-MathHelper.sin(yaw / 180.0F * (float)Math.PI) * MathHelper.cos(pitch / 180.0F * (float)Math.PI) * f);
							double motionZ = (double)(MathHelper.cos(yaw / 180.0F * (float)Math.PI) * MathHelper.cos(pitch / 180.0F * (float)Math.PI) * f);
							double motionY = (double)(-MathHelper.sin((pitch) / 180.0F * (float)Math.PI) * f);
							targetPlayer.addVelocity(motionX, motionY, motionZ);
							targetPlayer.velocityChanged = true;
							//flightTimeCount.put(targetPlayer.getUniqueID().toString(), flightTime - 1);
							flightTime -= 0.05F;
							nbt.setFloat("flightDuration", flightTime);
							
							targetPlayer.getServerWorld().spawnParticle(EnumParticleTypes.getByName(flightParticle), targetPlayer.posX, targetPlayer.posY, targetPlayer.posZ, partCount, partOffset, partOffset, partOffset, partSpeed, extraValue);

						}
					}
					if(!targetPlayer.onGround && targetPlayer.isSneaking() && !targetPlayer.isElytraFlying() && targetPlayer.rotationPitch < -30)
					{
						if(GetElytraLaunchBoots(targetPlayer.getArmorInventoryList()) && !targetPlayer.capabilities.isFlying)
						{
							if(flightTime > maxFlightTime)
							{
								flightTime = maxFlightTime;
							}
							float amountToTake = (maxFlightTime / 100f) * 30f;
							if(flightTime > amountToTake)
							{
								if(targetPlayer.isInWater() && depthDiver)
								{
									flightAcceleration /=10;
								}
								
								float yaw = targetPlayer.rotationYaw;
								float pitch = targetPlayer.rotationPitch;
								float f = flightAcceleration * 15;
								double motionX = (double)(-MathHelper.sin(yaw / 180.0F * (float)Math.PI) * MathHelper.cos(pitch / 180.0F * (float)Math.PI) * f);
								double motionZ = (double)(MathHelper.cos(yaw / 180.0F * (float)Math.PI) * MathHelper.cos(pitch / 180.0F * (float)Math.PI) * f);
								double motionY = (double)(-MathHelper.sin((pitch) / 180.0F * (float)Math.PI) * f);
								targetPlayer.addVelocity(motionX, motionY, motionZ);
								targetPlayer.velocityChanged = true;
								targetPlayer.onGround = false;
								targetPlayer.setElytraFlying();
								//flightTimeCount.put(targetPlayer.getUniqueID().toString(), flightTime - 1);
								nbt.setFloat("flightDuration", flightTime - amountToTake);
								
//								if(flightParticle.contains("rainbowReddust"))
//								{
//									double speed = 0.2;
//									flightParticle = "reddust";
//									targetPlayer.getServerWorld().spawnParticle(EnumParticleTypes.getByName(flightParticle), targetPlayer.posX, targetPlayer.posY, targetPlayer.posZ, 20, 0.5, 0.5, 0.5, speed, new int[]{});
//
//								}
//								else if(flightParticle.contains("rainbowMobSpell"))
//								{
//									double speed = 0.2;
//									flightParticle = "mobSpell";
//									targetPlayer.getServerWorld().spawnParticle(EnumParticleTypes.getByName(flightParticle), targetPlayer.posX, targetPlayer.posY, targetPlayer.posZ, 20, 1.0, 1.0, 1.0, speed, new int[]{});								
//								}
//								else if(flightParticle.contains("sandDust"))
//								{
//									double speed = 0;
//									flightParticle = "fallingdust";
//									targetPlayer.getServerWorld().spawnParticle(EnumParticleTypes.getByName(flightParticle), targetPlayer.posX, targetPlayer.posY, targetPlayer.posZ, 20, 0.7, 0.7, 0.7, speed, new int[]{80});								
//								}
//								else if(flightParticle.contains("rainbowMobSpellAmbient"))
//								{
//									double speed = 0.2;
//									flightParticle = "mobSpellAmbient";
//									targetPlayer.getServerWorld().spawnParticle(EnumParticleTypes.getByName(flightParticle), targetPlayer.posX, targetPlayer.posY, targetPlayer.posZ, 20, 1.0, 1.0, 1.0, speed, new int[]{});								
//								}
//								else
//								{
//									targetPlayer.getServerWorld().spawnParticle(EnumParticleTypes.getByName(flightParticle), targetPlayer.posX, targetPlayer.posY, targetPlayer.posZ, 20, 1, 1, 1, 0, new int[]{});
//								}

								targetPlayer.getServerWorld().spawnParticle(EnumParticleTypes.getByName(flightParticle), targetPlayer.posX, targetPlayer.posY, targetPlayer.posZ, 50, 1.5, 1.5, 1.5, partSpeed, extraValue);							
							}
						}
					}
					if(targetPlayer.onGround && flightTime < maxFlightTime)
					{
						nbt.setFloat("flightDuration", flightTime + flightTimeRecovery);						
					}
					if (targetPlayer.isInWater() && flightTime < maxFlightTime && depthDiver && targetPlayer.isElytraFlying())
					{
						nbt.setFloat("flightDuration", flightTime + (flightTimeRecovery / 10.0f));
					}
					if(flightTime < maxFlightTime)
					{
//						SPacketTitle packetTimes = new SPacketTitle(0, 5, 5);
//						SPacketTitle packetSubtitle = new SPacketTitle(SPacketTitle.Type.SUBTITLE, new TextComponentString(GetFlightBarString(flightTime, maxFlightTime)));
//						SPacketTitle packetTitle = new SPacketTitle(SPacketTitle.Type.TITLE, new TextComponentString(""));
//
//						targetPlayer.connection.sendPacket(packetTimes);
//						targetPlayer.connection.sendPacket(packetTitle);	
//						targetPlayer.connection.sendPacket(packetSubtitle);	
						
						SPacketChat chatPacket = new SPacketChat(new TextComponentString(GetFlightBarString(flightTime, maxFlightTime)),ChatType.GAME_INFO);
						targetPlayer.connection.sendPacket(chatPacket);													
					}
					elytra.setTagCompound(nbt);
				}
			}
			counter = 0;
			return;
		}
		counter++;
	}
	
	@SubscribeEvent
	public void onItemClick(PlayerInteractEvent.RightClickItem event)
	{
		EntityPlayerMP targetPlayer = (EntityPlayerMP) event.getEntityPlayer();
		ItemStack elytra = GetSuperiorElytra(targetPlayer.getArmorInventoryList());
		ItemStack book = targetPlayer.getHeldItem(EnumHand.MAIN_HAND);
		if(checkForParticleBook(event.getItemStack()))
		{
			if(elytra != null)
			{
				NBTTagCompound nbtElytra = elytra.getTagCompound();
				NBTTagCompound nbtBook = book.getTagCompound();
				
				if(nbtBook.hasKey("flightParticle"))
				{
					nbtElytra.setString("flightParticle",nbtBook.getString("flightParticle"));					
				}
				if(nbtBook.hasKey("partOffset"))
				{
					nbtElytra.setDouble("partOffset", nbtBook.getDouble("partOffset"));					
				}
				if(nbtBook.hasKey("partSpeed"))
				{
					nbtElytra.setDouble("partSpeed",  nbtBook.getDouble("partSpeed"));					
				}
				if(nbtBook.hasKey("partCount"))
				{
					nbtElytra.setInteger("partCount", nbtBook.getInteger("partCount"));					
				}
				if(nbtBook.hasKey("depthDiver"))
				{
					nbtElytra.setBoolean("depthDiver", nbtBook.getBoolean("depthDiver"));					
				}
				if(nbtBook.hasKey("flightUpgradePercent"))
				{
					float newMaxFlight = nbtElytra.getFloat("flightTime") * ((nbtBook.getFloat("flightUpgradePercent") / 100.0f) + 1f);
					nbtElytra.setFloat("flightTime", newMaxFlight);					
				}
				if(nbtBook.hasKey("flightRecoverUpgradePercent"))
				{
					float newMaxFlight = nbtElytra.getFloat("flightRecovery") * ((nbtBook.getFloat("flightRecoverUpgradePercent") / 100.0f) + 1f);
					nbtElytra.setFloat("flightRecovery", newMaxFlight);					
				}
				if(nbtBook.hasKey("extraValue"))
				{
					nbtElytra.setIntArray("extraValue", nbtBook.getIntArray("extraValue"));					
				}
				
	
				if(targetPlayer.getHeldItem(EnumHand.MAIN_HAND).getCount() > 1)
				{
					targetPlayer.getHeldItem(EnumHand.MAIN_HAND).setCount(targetPlayer.getHeldItem(EnumHand.MAIN_HAND).getCount() - 1);
				}
				else
				{
					targetPlayer.inventory.removeStackFromSlot(targetPlayer.inventory.currentItem);
				}
				targetPlayer.inventoryContainer.detectAndSendChanges();
				elytra.setTagCompound(nbtElytra);
				
				NBTTagList loreList = new NBTTagList();
				
				DecimalFormat df = new DecimalFormat("###.##");
				
				loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Tier: " + TextFormatting.GRAY + nbtElytra.getString("flightTier")));
				loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Flight Time: " + TextFormatting.GRAY + df.format(nbtElytra.getFloat("flightTime"))));
				loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Flight Acceleration: " + TextFormatting.GRAY + nbtElytra.getFloat("flightAcceleration")));
				loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Flight Recovery: " + TextFormatting.GRAY + df.format(nbtElytra.getFloat("flightRecovery"))));
				loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Boost Particle: " + TextFormatting.GRAY + nbtElytra.getString("flightParticle")));
				loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Depth Diver: " + TextFormatting.GRAY + nbtElytra.getBoolean("depthDiver")));
				loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Particle Count: " + TextFormatting.GRAY + nbtElytra.getInteger("partCount")));
				loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Hold Sneak to boost while flying"));
				nbtElytra.getCompoundTag("display").setTag("Lore", loreList);
				
				elytra.setTagCompound(nbtElytra);
				targetPlayer.sendMessage(new TextComponentString(TextFormatting.GREEN + "Elytra updated"));
			}
			else
			{
				targetPlayer.sendMessage(new TextComponentString(TextFormatting.RED + "No compatible Elytra found in chest slot"));
			}
		}
	}
	
	public String GetFlightBarString(float flightTime, float maxFlightTime)
	{
		int whitePipes = (int) (((double)flightTime / (double)maxFlightTime) * (double)50);
		String pipes = TextFormatting.AQUA + "";
		for(int x = 0; x < whitePipes; x++)
		{
			pipes += "|";
		}
		pipes += TextFormatting.DARK_GRAY;
		for(int x = 0; x < (50 - whitePipes); x++)
		{
			pipes += "|";
		}
		return pipes;
	}
	

	public boolean checkForParticleBook(ItemStack item)
	{
		if(item != null)
		{
			NBTTagCompound nbt = item.getTagCompound();
			if(nbt != null)
			{
				if(nbt.hasKey("isParticleBook"))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean GetElytraLaunchBoots(Iterable<ItemStack> itemList)
	{
		if(itemList != null)
		{
			for (ItemStack item : itemList) 
			{
				if(item != null)
				{
					if(item.hasTagCompound())
					{
						NBTTagCompound nbt = item.getTagCompound();
						if(nbt.hasKey("isElytraLaunchBoots"))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public ItemStack GetSuperiorElytra(Iterable<ItemStack> itemList)
	{
		if(itemList != null)
		{
			for (ItemStack item : itemList) 
			{
				if(item != null)
				{
					if(item.hasTagCompound())
					{
						NBTTagCompound nbt = item.getTagCompound();
						if(nbt.hasKey("isSuperiorElytra"))
						{
							if(!nbt.hasKey("flightTime"))
							{
								nbt.setFloat("flightTime", 100F);
								item.setTagCompound(nbt);
							}
							if(!nbt.hasKey("flightAcceleration"))
							{
								nbt.setFloat("flightAcceleration", 0.0025F);
								item.setTagCompound(nbt);
							}
							if(!nbt.hasKey("flightRecovery"))
							{
								nbt.setFloat("flightRecovery", 0.1F);	
								item.setTagCompound(nbt);							
							}
							if(!nbt.hasKey("flightParticle"))
							{
								nbt.setString("flightParticle", "fireworksSpark");	
								item.setTagCompound(nbt);							
							}
							if(!nbt.hasKey("flightDuration"))
							{
								nbt.setFloat("flightDuration", 100F);	
								item.setTagCompound(nbt);							
							}
							if(!nbt.hasKey("flightTier"))
							{
								nbt.setString("flightTier", "Custom");	
								item.setTagCompound(nbt);							
							}
							if(!nbt.hasKey("partOffset"))
							{
								nbt.setDouble("partOffset", 1);	
								item.setTagCompound(nbt);							
							}
							if(!nbt.hasKey("partSpeed"))
							{
								nbt.setDouble("partSpeed", 0);	
								item.setTagCompound(nbt);							
							}
							if(!nbt.hasKey("partCount"))
							{
								nbt.setInteger("partCount", 5);	
								item.setTagCompound(nbt);							
							}
							if(!nbt.hasKey("depthDiver"))
							{
								nbt.setBoolean("depthDiver", false);	
								item.setTagCompound(nbt);							
							}
							if(!nbt.hasKey("extraValue"))
							{
								nbt.setIntArray("extraValue", new int[]{});	
								item.setTagCompound(nbt);							
							}
							if(!nbt.hasKey("display"))
							{
								nbt.setTag("display", new NBTTagCompound());
								nbt.getCompoundTag("display").setString("Name", TextFormatting.GOLD + "Boosting Elytra");
								item.setTagCompound(nbt);		
							}
							if(!nbt.getCompoundTag("display").hasKey("Lore"))
							{
								NBTTagList loreList = new NBTTagList();

								DecimalFormat df = new DecimalFormat("###.##");
								
								loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Tier: " + TextFormatting.GRAY + nbt.getString("flightTier")));
								loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Flight Time: " + TextFormatting.GRAY + df.format(nbt.getFloat("flightTime"))));
								loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Flight Acceleration: " + TextFormatting.GRAY + nbt.getFloat("flightAcceleration")));
								loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Flight Recovery: " + TextFormatting.GRAY + df.format(nbt.getFloat("flightRecovery"))));
								loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Boost Particle: " + TextFormatting.GRAY + nbt.getString("flightParticle")));
								loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Particle Count: " + TextFormatting.GRAY + nbt.getInteger("partCount")));
								loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Depth Diver: " + TextFormatting.GRAY + nbt.getBoolean("depthDiver")));
								loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Hold Sneak to boost while flying"));
								nbt.getCompoundTag("display").setTag("Lore", loreList);
								item.setTagCompound(nbt);
							}
							return item;
						}
					}
				}
			}
		}
		return null;
	}
}
