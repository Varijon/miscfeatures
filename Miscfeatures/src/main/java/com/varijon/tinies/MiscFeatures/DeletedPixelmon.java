package com.varijon.tinies.MiscFeatures;

import net.minecraft.nbt.NBTTagCompound;

public class DeletedPixelmon 
{
	private NBTTagCompound pokemonData;
	private String timestamp;
	
	public DeletedPixelmon(NBTTagCompound pPokemonData, String pTimeStamp)
	{
		pokemonData = pPokemonData;
		timestamp = pTimeStamp;
	}
	
	public NBTTagCompound GetPokemonData()
	{
		return pokemonData;
	}
	
	public String GetTimeStamp()
	{
		return timestamp;
	}
}
