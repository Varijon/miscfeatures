package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class IVCommand implements ICommand {

	private List aliases;
	public IVCommand()
	{
	   this.aliases = new ArrayList();
	   this.aliases.add("iv");
	   this.aliases.add("ivs");
	}
	
	@Override
	public int compareTo(ICommand arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "iv [#]";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		// TODO Auto-generated method stub
		return "iv [#]";
	}

	@Override
	public List<String> getAliases() {
		// TODO Auto-generated method stub
		return this.aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException 
	{
		if(sender.canUseCommand(4, "miscfeatures.IVs"))
		{
			if(args.length == 0)
			{
				sender.sendMessage(new TextComponentString(TextFormatting.RED + "Usage: /iv [#]"));						
				return;
			}
			if(sender instanceof EntityPlayerMP)
			{
				EntityPlayerMP player = (EntityPlayerMP) sender;
				
				try
				{
					Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
					if (!optPlayerStorage.isPresent()) 
					{
				        return;
				    }
					PlayerStorage playerStorage = (PlayerStorage) optPlayerStorage.get();
					int partyNumber = Integer.parseInt(args[0]) - 1;
					
					EntityPixelmon pixelmon = playerStorage.getPokemon(playerStorage.getIDFromPosition(partyNumber), player.getEntityWorld());
					if (pixelmon != null)
					{
						int pixelmonIVHP = pixelmon.stats.ivs.HP;
						int pixelmonIVAttack = pixelmon.stats.ivs.Attack;
						int pixelmonIVDefense = pixelmon.stats.ivs.Defence;
						int pixelmonIVSpAtt = pixelmon.stats.ivs.SpAtt;
						int pixelmonIVSpDef = pixelmon.stats.ivs.SpDef;
						int pixelmonIVSpeed = pixelmon.stats.ivs.Speed;
						
						sender.sendMessage(new TextComponentString(TextFormatting.GREEN + pixelmon.getPokemonName()+ " " + TextFormatting.YELLOW + "IVs:"));
						sender.sendMessage(new TextComponentString(TextFormatting.GRAY + "HP: " + colorIV(pixelmonIVHP) + pixelmonIVHP));
						sender.sendMessage(new TextComponentString(TextFormatting.GRAY + "Att: " + colorIV(pixelmonIVAttack) + pixelmonIVAttack));
						sender.sendMessage(new TextComponentString(TextFormatting.GRAY + "Def: " + colorIV(pixelmonIVDefense) + pixelmonIVDefense));
						sender.sendMessage(new TextComponentString(TextFormatting.GRAY + "SpAtt: " + colorIV(pixelmonIVSpAtt) + pixelmonIVSpAtt));
						sender.sendMessage(new TextComponentString(TextFormatting.GRAY + "SpDef: " + colorIV(pixelmonIVSpDef) + pixelmonIVSpDef));
						sender.sendMessage(new TextComponentString(TextFormatting.GRAY + "Speed: " + colorIV(pixelmonIVSpeed) + pixelmonIVSpeed));
						
						int combinedIV = pixelmonIVHP + pixelmonIVAttack + pixelmonIVDefense + pixelmonIVSpAtt + pixelmonIVSpDef + pixelmonIVSpeed;
											
						sender.sendMessage(new TextComponentString(TextFormatting.GOLD + "Total: " + TextFormatting.YELLOW + combinedIV + "/186 or " + Math.round(((float)combinedIV / 186.0 * 100.0)) + "%"));
					}
					else
					{
						sender.sendMessage(new TextComponentString(TextFormatting.RED + "Party slot is empty!"));						
					}
				}
				catch(Exception ex)
				{
					//System.out.println(ex.getStackTrace());
				}
			}
			return;
		}
		else
		{
			sender.sendMessage(new TextComponentString(TextFormatting.RED + "You don't have permission to use this command"));
			return;
		}

	}
	
	public TextFormatting colorIV(int pIV)
	{
		if(pIV < 6)
		{
			return TextFormatting.DARK_RED;
		}
		if(pIV < 11)
		{
			return TextFormatting.RED;
		}
		if(pIV < 16)
		{
			return TextFormatting.GOLD;
		}
		if(pIV < 23)
		{
			return TextFormatting.YELLOW;
		}
		if(pIV < 31)
		{
			return TextFormatting.GREEN;
		}
		if(pIV == 31)
		{
			return TextFormatting.AQUA;
		}
		return TextFormatting.GRAY;
	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender) 
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index) {
		// TODO Auto-generated method stub
		return false;
	}

}
