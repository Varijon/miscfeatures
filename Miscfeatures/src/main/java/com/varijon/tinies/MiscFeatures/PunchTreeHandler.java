//package com.varijon.tinies.MiscFeatures;
//
//import java.util.ArrayList;
//
//import net.minecraft.block.Block;
//import net.minecraft.entity.item.EntityFallingBlock;
//import net.minecraft.entity.player.EntityPlayer;
//import net.minecraft.entity.player.EntityPlayerMP;
//import net.minecraft.init.Blocks;
//import net.minecraft.item.ItemStack;
//import net.minecraft.nbt.NBTTagCompound;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.util.text.TextComponentString;
//import net.minecraft.util.text.TextFormatting;
//import net.minecraft.util.MathHelper;
//import net.minecraft.world.World;
//import net.minecraftforge.event.entity.player.EntityInteractEvent;
//import net.minecraftforge.event.entity.player.PlayerInteractEvent;
//import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;
//import net.minecraftforge.event.world.BlockEvent;
//import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
//
//import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
//import com.pixelmonmod.pixelmon.config.PixelmonItems;
//import com.pixelmonmod.pixelmon.entities.npcs.NPCChatting;
//import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
//import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
//import com.pixelmonmod.pixelmon.storage.PlayerNotLoadedException;
//import com.pixelmonmod.pixelmon.storage.PlayerStorage;
//
//public class PunchTreeHandler 
//{
//	ArrayList<BlockPos> lst_ScheduleFall;
//	ArrayList<EntityFallingBlock> lst_FallingBlock;
//	@SubscribeEvent
//	public void onPlayerInteract(PlayerInteractEvent event) throws PlayerNotLoadedException
//	{
//		if(event.action == Action.LEFT_CLICK_BLOCK)
//		{
//			EntityPlayerMP player = (EntityPlayerMP) event.entityPlayer;
//			World w = player.getEntityWorld();
//			Block b = w.getBlockState(event.pos).getBlock();
//			if(player.getHeldItem() == null || b.equals(Blocks.log))
//			{
//				lst_ScheduleFall = new ArrayList<BlockPos>();
//				lst_FallingBlock = new ArrayList<EntityFallingBlock>();
//				
//				//counter = 0;
//				
//				for(int x = -1; x < 2; x++)
//				{
//					for(int z = -1; z < 2; z++)
//					{
//						for(int y = -1; y < 2; y++)
//						{
//							BlockPos loc = event.pos.add(x, y, z);
//							
//							if(w.getBlockState(loc).getBlock().equals(Blocks.log) || w.getBlockState(loc).getBlock().equals(Blocks.leaves))
//							{
//								if(!lst_ScheduleFall.contains(loc))
//								{
//									EntityFallingBlock fall = new EntityFallingBlock(w, loc.getX(), loc.getY(), loc.getZ(), w.getBlockState(loc));
//									fall.fallTime = 1;
//									lst_FallingBlock.add(fall);
//									w.setBlockToAir(loc);
//									
//									doFallingBlockStuff(w,loc);
//									
//									lst_ScheduleFall.add(loc);
//								}
//							}
//																
//						}
//					}
//				}
//				launchTree(w,player);
//			}
//			
//		}
//	}
//	public void doFallingBlockStuff(World w, BlockPos loc)
//	{
//		for(int x = -1; x < 2; x++)
//		{
//			for(int z = -1; z < 2; z++)
//			{
//				for(int y = -1; y < 2; y++)
//				{
//					BlockPos locNew = loc.add(x, y, z);
//					
//					if(w.getBlockState(locNew).getBlock().equals(Blocks.log) || w.getBlockState(locNew).getBlock().equals(Blocks.leaves))
//					{
//						if(!lst_ScheduleFall.contains(locNew))
//						{
//							EntityFallingBlock fall = new EntityFallingBlock(w, locNew.getX(), locNew.getY(), locNew.getZ(), w.getBlockState(locNew));
//							fall.fallTime = 1;
//							lst_FallingBlock.add(fall);
//							w.setBlockToAir(locNew);
//							
//							doFallingBlockStuff(w,locNew);
//
//							
//							lst_ScheduleFall.add(locNew);
//						}
//					}
//														
//				}
//			}
//		}
//	}
//	
//	public void launchTree(World w, EntityPlayer p)
//	{
//		System.out.println(lst_FallingBlock.size());
//		for(EntityFallingBlock block : lst_FallingBlock)
//		{
//			float yaw = p.rotationYaw;
//			float pitch = p.rotationPitch;
//			float f = 1.0F;
//			double motionX = (double)(-MathHelper.sin(yaw / 180.0F * (float)Math.PI) * MathHelper.cos(pitch / 180.0F * (float)Math.PI) * f);
//			double motionZ = (double)(MathHelper.cos(yaw / 180.0F * (float)Math.PI) * MathHelper.cos(pitch / 180.0F * (float)Math.PI) * f);
//			double motionY = (double)(-MathHelper.sin((pitch) / 180.0F * (float)Math.PI) * f);
//			block.addVelocity(motionX, motionY, motionZ);
//			
//			w.spawnEntityInWorld(block);
//		}
//	}
//}
