package com.varijon.tinies.MiscFeatures;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.varijon.tinies.MiscFeatures.magic.FireballProjectileHandler;
import com.varijon.tinies.MiscFeatures.magic.HomingBoltHandler;
import com.varijon.tinies.MiscFeatures.magic.MagicWandHandler;

import net.minecraft.nbt.NBTException;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

@Mod(modid="miscfeatures", version="0.1", acceptableRemoteVersions="*")
public class Main
{
	public static String MODID = "modid";
	public static String VERSION = "version";
	
	public static String[] allowedMounts;
	public static String[] allowedFlying;
	public static String[] allowedSurf;
	public static String test;
	public static int pokemonShowCooldown;
	public static int afkKick;
	public static int lootSpawnSeconds;
	public static String afkKickMessage;
		
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		Configuration config = new Configuration(e.getSuggestedConfigurationFile());
		config.load();
		Property propertyExtraMounts = config.get("config", "ExtraMounts", new String[]{"Volcarona"});
		Property propertyAllowedFlying = config.get("config", "AllowFlying", new String[]{"Metapod"});
		Property propertyAllowedSurf = config.get("config", "AllowSurf", new String[]{"Vaporeon"});
		Property propertyPokemonShowCooldown = config.get("config", "PokemonShowCooldownSeconds", 300);
		Property propertyAfkKick = config.get("config", "AFKKickSeconds", 3600);
		Property propertyLootSpawnSeconds = config.get("config", "LootSpawnSeconds", 900);
		Property propertyAfkKickMessage = config.get("config", "AFKKickMessage", "&9Kicked for being AFK for 1 hour");
		allowedMounts = propertyExtraMounts.getStringList();
		allowedFlying = propertyAllowedFlying.getStringList();
		allowedSurf = propertyAllowedSurf.getStringList();
		pokemonShowCooldown = propertyPokemonShowCooldown.getInt();
		afkKick = propertyAfkKick.getInt();
		lootSpawnSeconds = propertyLootSpawnSeconds.getInt();
		afkKickMessage = propertyAfkKickMessage.getString();
		
//		if (config.hasChanged()) 
//		{
			config.save();
//		}
		DeletedPokemonFile.initConfig();
		EconomyLogHandler.initConfig();
		LootBoxConfigManager.initConfig();
		try {
			LootBoxConfigManager.loadConfig();
		} catch (NBTException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//		event.player.playerNetServerHandler.sendPacket(new S45PacketTitle);
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e)
	{		
		MinecraftForge.EVENT_BUS.register(new CustomMountHandler());
		MinecraftForge.EVENT_BUS.register(new ItemizeHandler());
//		MinecraftForge.EVENT_BUS.register(new ExploitPreventionHandler());
		MinecraftForge.EVENT_BUS.register(new StopSuffocateHandler());
		MinecraftForge.EVENT_BUS.register(new LootBoxHandler());
		MinecraftForge.EVENT_BUS.register(new TutorBookHandler());
//		MinecraftForge.EVENT_BUS.register(new IceSkateHandler());
		MinecraftForge.EVENT_BUS.register(new MagicPickaxeHandler());
		MinecraftForge.EVENT_BUS.register(new LastLegendHandler());
		MinecraftForge.EVENT_BUS.register(new SuperiorElytraHandler());
		MinecraftForge.EVENT_BUS.register(new PixelmonDeletionHandler());
		MinecraftForge.EVENT_BUS.register(new MagicWandHandler());
		MinecraftForge.EVENT_BUS.register(new HomingBoltHandler());
		MinecraftForge.EVENT_BUS.register(new FireballProjectileHandler());
		MinecraftForge.EVENT_BUS.register(new PokemonTokenHandler());
		MinecraftForge.EVENT_BUS.register(new ChestCommandHandler());
		MinecraftForge.EVENT_BUS.register(new InfoBoardHandler());
		MinecraftForge.EVENT_BUS.register(new EVScanner());
		MinecraftForge.EVENT_BUS.register(new CustomEggHatchHandler());
//		MinecraftForge.EVENT_BUS.register(new AfkHandler());
//		MinecraftForge.EVENT_BUS.register(new TreeBerryHandler());
		MinecraftForge.EVENT_BUS.register(new PokelootSpawnHandler());
//		MinecraftForge.EVENT_BUS.register(new GolfBallHandler());
		MinecraftForge.EVENT_BUS.register(new PortalBlockHandler());
		MinecraftForge.EVENT_BUS.register(new EventPokemonSpawn());
		MinecraftForge.EVENT_BUS.register(new PixelmonStatueHandler());
		MinecraftForge.EVENT_BUS.register(new SylveonEvoHandler());
		MinecraftForge.EVENT_BUS.register(new RotomFormChangeHandler());
//		MinecraftForge.EVENT_BUS.register(new MechAnvilExploitHandler());
		MinecraftForge.EVENT_BUS.register(new EasterEggHandler());
		MinecraftForge.EVENT_BUS.register(new EasterPokemonDefeatedHandler());
		MinecraftForge.EVENT_BUS.register(new SpeciesTokenHandler());
		MinecraftForge.EVENT_BUS.register(new MegaItemHandler());
		MinecraftForge.EVENT_BUS.register(new BossHandler());
		MinecraftForge.EVENT_BUS.register(new VoteKeyFragmentHandler());
//		MinecraftForge.EVENT_BUS.register(new MoveAnimationHandler());
//		MinecraftForge.EVENT_BUS.register(new DeflectHandler());
//		MinecraftForge.EVENT_BUS.register(new SilkChestHandler());
		Pixelmon.EVENT_BUS.register(new DeletedPixelmonHandler());
		Pixelmon.EVENT_BUS.register(new RarePokemonDefeatedHandler());
		Pixelmon.EVENT_BUS.register(new TradeAnnounceHandler());
		Pixelmon.EVENT_BUS.register(new CatchAnnouncerHandler());
		Pixelmon.EVENT_BUS.register(new EasterPokemonDefeatedHandler());
//		Pixelmon.EVENT_BUS.register(new ReleaseTokenHandler());
		Pixelmon.EVENT_BUS.register(new EconomyLogHandler());
		
		
//		GameRegistry.addRecipe(
//				new ItemStack(PixelUtilitiesAdditions.chairBlock),
//				"x  ",
//				"xy ",
//				"zz ",
//				'x', new ItemStack(Blocks.planks), 'y', new ItemStack(Blocks.wooden_slab), 'z', new ItemStack(Items.stick)
//				);
		//Arrowloose event, check charge.. fire fireworks?
		//BrewingRecipeRegistry.addRecipe(null, null, null)
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent e)
	{
		
	}

	 @EventHandler
	 public void serverLoad(FMLServerStartingEvent event)
	 {
		 
	   event.registerServerCommand(new EscapeBattleCommand());
	   event.registerServerCommand(new IVCommand());
	   event.registerServerCommand(new EVCommand());
	   event.registerServerCommand(new ItemizeCommand());
	   event.registerServerCommand(new ExtrasCommand());
	   event.registerServerCommand(new LastLegendSpawnCommand());
//	   event.registerServerCommand(new ResetPixelmonCurrencyCommand());
	   event.registerServerCommand(new LootBoxCommand());
	   event.registerServerCommand(new PokeMessageCommand());
	   event.registerServerCommand(new ShowPokemonCommand());
	   event.registerServerCommand(new ReleaseHistoryCommand());
	   event.registerServerCommand(new RestorePokemonCommand());
	   event.registerServerCommand(new InfoBoardCommand());
//	   event.registerServerCommand(new DeflectCommand());
	   //event.registerServerCommand(new AllBannedSkullCommand());
	 }
}