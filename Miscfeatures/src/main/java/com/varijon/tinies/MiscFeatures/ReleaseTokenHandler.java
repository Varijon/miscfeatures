package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.HashMap;

import com.pixelmonmod.pixelmon.api.enums.DeleteType;
import com.pixelmonmod.pixelmon.api.events.PixelmonDeletedEvent;
import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
import com.pixelmonmod.pixelmon.config.PixelmonItems;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ReleaseTokenHandler 
{
	public static HashMap<String, ArrayList<DeletedPixelmon>> deletedPixelmon;
	
	public ReleaseTokenHandler()
	{
		deletedPixelmon = new HashMap<String, ArrayList<DeletedPixelmon>>();
	}
	
	@SubscribeEvent
	public void onPixelmonDelete(PixelmonDeletedEvent event)
	{
		if(event.deleteType != DeleteType.COMMAND)
		{
			if(event.pokemon != null)
			{
				EntityPlayerMP player = event.player;
				
				EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(event.pokemon, player.getServerWorld());
				
				if(!pokemon.isEgg)
				{
					ItemStack item = new ItemStack(PixelmonItems.itemPixelmonSprite, 1);
					item.setTagCompound(new NBTTagCompound());
								
					String dexNumber = pokemon.baseStats.nationalPokedexNumber + "";
					if(dexNumber.length() == 2)
					{
						dexNumber = "0" + dexNumber;
					}
					if(dexNumber.length() == 1)
					{
						dexNumber = "00" + dexNumber;
					}
					if(dexNumber.equals("351") || dexNumber.equals("386"))
					{
						dexNumber += "-normal";
					}
					
					
					NBTTagCompound tags = item.getTagCompound();
					tags.setTag("display", new NBTTagCompound());
					
					if(pokemon.getIsShiny())
					{
						tags.setString("SpriteName", "pixelmon:sprites/shinypokemon/" + dexNumber);
					}
					else
					{
						tags.setString("SpriteName", "pixelmon:sprites/pokemon/" + dexNumber);						
					}
					tags.setString("speciesToken", pokemon.getPokemonName());
					tags.setBoolean("shinyToken", pokemon.getIsShiny());
					
					if(pokemon.getIsShiny())
					{
						tags.getCompoundTag("display").setString("Name", TextFormatting.GOLD + "Shiny " + pokemon.getPokemonName() + " Token");						
					}
					else
					{
						tags.getCompoundTag("display").setString("Name", TextFormatting.BLUE + pokemon.getPokemonName() + " Token");						
					}
					
					NBTTagList loreList = new NBTTagList();
					
					if(pokemon.getIsShiny())
					{
						loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Right-click with " + TextFormatting.RED + "4 " + TextFormatting.GRAY + "for a random " + TextFormatting.GOLD + "Shiny " + TextFormatting.RED + pokemon.getPokemonName()));
					}
					else
					{
						loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Right-click with " + TextFormatting.RED + "4 " + TextFormatting.GRAY + "for a random " + TextFormatting.RED + pokemon.getPokemonName()));
					}
					
					tags.getCompoundTag("display").setTag("Lore", loreList);
					
					item.setTagCompound(tags);
					
					if(!player.inventory.addItemStackToInventory(item))
					{
						World w = player.getEntityWorld();
						w.spawnEntity(new EntityItem(w, player.lastTickPosX, player.lastTickPosY, player.lastTickPosZ, item));
					}
					player.inventoryContainer.detectAndSendChanges();
				}
			}
		}
	}
}
