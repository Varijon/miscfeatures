//package com.varijon.tinies.MiscFeatures.MoveAnims;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Optional;
//
//import net.minecraft.entity.item.EntityFallingBlock;
//import net.minecraft.entity.item.EntityItem;
//import net.minecraft.entity.player.EntityPlayer;
//import net.minecraft.entity.player.EntityPlayerMP;
//import net.minecraft.init.Blocks;
//import net.minecraft.item.ItemStack;
//import net.minecraft.nbt.NBTTagCompound;
//import net.minecraft.nbt.NBTTagList;
//import net.minecraft.nbt.NBTTagString;
//import net.minecraft.server.MinecraftServer;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.util.text.TextComponentString;
//import net.minecraft.util.text.TextFormatting;
//import net.minecraft.world.World;
//import net.minecraftforge.common.MinecraftForge;
//import net.minecraftforge.event.entity.EntityEvent.EnteringChunk;
//import net.minecraftforge.event.entity.living.LivingHurtEvent;
//import net.minecraftforge.fml.common.FMLCommonHandler;
//import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
//import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;
//
//import com.pixelmonmod.pixelmon.Pixelmon;
//import com.pixelmonmod.pixelmon.api.enums.DeleteType;
//import com.pixelmonmod.pixelmon.api.events.PixelmonDeletedEvent;
//import com.pixelmonmod.pixelmon.battles.BattleRegistry;
//import com.pixelmonmod.pixelmon.battles.attacks.Attack;
//import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
//import com.pixelmonmod.pixelmon.battles.controller.log.BattleActionBase;
//import com.pixelmonmod.pixelmon.battles.controller.participants.BattleParticipant;
//import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
//import com.pixelmonmod.pixelmon.blocks.PixelmonBlock;
//import com.pixelmonmod.pixelmon.comm.packetHandlers.ExternalMove;
//import com.pixelmonmod.pixelmon.comm.packetHandlers.ExternalMove.Handler;
//import com.pixelmonmod.pixelmon.comm.packetHandlers.SetHeldItem;
//import com.pixelmonmod.pixelmon.config.PixelmonBlocks;
//import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
//import com.pixelmonmod.pixelmon.config.PixelmonItems;
//import com.pixelmonmod.pixelmon.entities.npcs.NPCChatting;
//import com.pixelmonmod.pixelmon.entities.npcs.NPCShopkeeper;
//import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
//import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
//import com.pixelmonmod.pixelmon.storage.PlayerNotLoadedException;
//import com.pixelmonmod.pixelmon.storage.PlayerStorage;
//
//public class MoveAnimationHandler 
//{
//
//	MinecraftServer server;
//	ArrayList<BattleControllerBase> battlesToCheck;
//	public MoveAnimationHandler()
//	{
//		server = FMLCommonHandler.instance().getMinecraftServerInstance();
//		battlesToCheck = new ArrayList<BattleControllerBase>();
//	}
//	
////	@SubscribeEvent
////	public void onEntityHurt(LivingHurtEvent event)
////	{
////		if(event.getEntityLiving() instanceof EntityPixelmon)
////		{
////			EntityPixelmon pixelmon = (EntityPixelmon) event.getEntityLiving();
////			if(pixelmon.battleController != null)
////			{
////				BattleControllerBase bc = pixelmon.battleController;
////				if(bc.playerNumber >= 1)
////				{
////					battlesToCheck.add(bc);
////				}
////			}
////		}
////	}
//	@SubscribeEvent
//	public void onWorldTick(WorldTickEvent event)
//	{
//		for(EntityPlayerMP otherPlayer : server.getPlayerList().getPlayerList())
//		{
//			BattleControllerBase bc = BattleRegistry.getBattle(otherPlayer);
//			if(bc != null)
//			{
//				if(!battlesToCheck.contains(bc))
//				{
//					battlesToCheck.add(bc);
//				}
//			}
//		}
//		if(!battlesToCheck.isEmpty())
//		{
//			for(BattleControllerBase bc : battlesToCheck)
//			{
//				for(BattleParticipant bp : bc.participants)
//				{
//					PixelmonWrapper pw = bp.controlledPokemon[0];
//					Attack att = bc.battleLog.getLastAttack(pw);
//					if(att != null)
//					{
//						System.out.println("Turn " + bc.battleTurn + " " + pw.pokemon.getPokemonName() + " " + att);
//					}
//				}
//			}
//			battlesToCheck.clear();
//		}
//	}
//	
//}
