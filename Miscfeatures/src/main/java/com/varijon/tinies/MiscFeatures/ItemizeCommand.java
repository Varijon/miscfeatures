package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.pixelmonmod.pixelmon.battles.BattleRegistry;
import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.config.PixelmonItems;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Moveset;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class ItemizeCommand implements ICommand {

	private List aliases;
	public ItemizeCommand()
	{
	   this.aliases = new ArrayList();
	   this.aliases.add("itemize");
	}
	
	@Override
	public int compareTo(ICommand arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "itemize [#]";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		// TODO Auto-generated method stub
		return "itemize [#]";
	}

	@Override
	public List<String> getAliases() {
		// TODO Auto-generated method stub
		return this.aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException 
	{
		if(sender.canUseCommand(4, "miscfeatures.itemize"))
		{
			if(args.length == 0)
			{
				sender.sendMessage(new TextComponentString(TextFormatting.RED + "Usage: /itemize [#]"));						
				return;
			}
			if(sender instanceof EntityPlayerMP)
			{
				EntityPlayerMP player = (EntityPlayerMP) sender;
				try
				{
					Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
					if (!optPlayerStorage.isPresent()) 
					{
				        return;
				    }
					PlayerStorage playerStorage = (PlayerStorage) optPlayerStorage.get();
					
					if(BattleRegistry.getBattle(player) != null)
					{
						sender.sendMessage(new TextComponentString(TextFormatting.RED + "Cannot itemize pokemon while in battle!"));
						return;
					}
					
					int partyNumber = Integer.parseInt(args[0]) - 1;
					
					EntityPixelmon pixelmon = playerStorage.getPokemon(playerStorage.getIDFromPosition(partyNumber), player.getEntityWorld());
					if (pixelmon != null)
					{
						if(playerStorage.countTeam() == 1 && !pixelmon.isEgg)
						{
							sender.sendMessage(new TextComponentString(TextFormatting.RED + "Cannot itemize your last pokemon!"));
							return;
						}
						
						String pixelmonName = pixelmon.getPokemonName();
						String pixelmonAbility = pixelmon.getAbility().getName();
						String pixelmonGender = pixelmon.gender.name();
						String pixelmonGrowth = pixelmon.getGrowth().name();
						String pixelmonNature = pixelmon.getNature().name();
						int pixelmonFriendship = pixelmon.friendship.getFriendship();
						int pixelmonLevel = pixelmon.getLvl().getLevel(); 
						String pixelmonShiny = getShiny(pixelmon.getIsShiny());
						
						if(pixelmon.isEgg)
						{
							pixelmonName += " Egg";
							pixelmonAbility = "???";
							pixelmonGender = "???";
							pixelmonGrowth = "???";
							pixelmonNature = "???";
							pixelmonShiny = "???";
						}
											
						int pixelmonIVHP = pixelmon.stats.ivs.HP;
						int pixelmonIVAttack = pixelmon.stats.ivs.Attack;
						int pixelmonIVDefense = pixelmon.stats.ivs.Defence;
						int pixelmonIVSpAtt = pixelmon.stats.ivs.SpAtt;
						int pixelmonIVSpDef = pixelmon.stats.ivs.SpDef;
						int pixelmonIVSpeed = pixelmon.stats.ivs.Speed;
						
						int pixelmonEVHP = pixelmon.stats.evs.hp;
						int pixelmonEVAttack = pixelmon.stats.evs.attack;
						int pixelmonEVDefense = pixelmon.stats.evs.defence;
						int pixelmonEVSpAtt = pixelmon.stats.evs.specialAttack;
						int pixelmonEVSpDef = pixelmon.stats.evs.specialDefence;
						int pixelmonEVSpeed = pixelmon.stats.evs.speed;
						
						
						int pixelmonStatsHP = pixelmon.stats.hp;
						int pixelmonStatsAttack = pixelmon.stats.attack;
						int pixelmonStatsDefense = pixelmon.stats.defence;
						int pixelmonStatsSpAtt = pixelmon.stats.specialAttack;
						int pixelmonStatsSpDef = pixelmon.stats.specialDefence;
						int pixelmonStatsSpeed = pixelmon.stats.speed;
						
						Moveset pixelmonMoves = pixelmon.getMoveset();

						ItemStack item = new ItemStack(PixelmonItems.itemPixelmonSprite, 1);
						item.setTagCompound(new NBTTagCompound());
												
						
						NBTTagCompound tags = item.getTagCompound();
						tags.setTag("display", new NBTTagCompound());
						
						String dexNumber = pixelmon.baseStats.nationalPokedexNumber + "";
						if(dexNumber.length() == 2)
						{
							dexNumber = "0" + dexNumber;
						}
						if(dexNumber.length() == 1)
						{
							dexNumber = "00" + dexNumber;
						}
						if(!pixelmon.isEgg)
						{
							if(pixelmon.getIsShiny())
							{
								tags.setString("SpriteName", "pixelmon:sprites/shinypokemon/" + dexNumber);						
							}
							else
							{
								tags.setString("SpriteName", "pixelmon:sprites/pokemon/" + dexNumber);
							}
						}
						else
						{
							tags.setString("SpriteName", "pixelmon:sprites/eggs/egg1");
						}
						tags.getCompoundTag("display").setString("Name", TextFormatting.GOLD + pixelmonName);
						
						NBTTagList loreList = new NBTTagList();
						
						loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Nature: " + TextFormatting.GREEN + pixelmonNature));
						loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Ability: " + TextFormatting.GREEN + pixelmonAbility));
						loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Level: " + TextFormatting.GREEN + pixelmonLevel));
						loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Gender: " + TextFormatting.GREEN + pixelmonGender));
						loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Shiny: " + TextFormatting.YELLOW + pixelmonShiny));
						loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Size: " + TextFormatting.GREEN + pixelmonGrowth));
						loreList.appendTag(new NBTTagString(TextFormatting.BLUE + "Friendship: " + TextFormatting.GREEN + pixelmonFriendship));
						
						loreList.appendTag(new NBTTagString(TextFormatting.GRAY + "Stats:"));
						loreList.appendTag(new NBTTagString(TextFormatting.LIGHT_PURPLE + "HP: IV: " + TextFormatting.GREEN + pixelmonIVHP + TextFormatting.LIGHT_PURPLE + " EV: " + TextFormatting.GREEN + pixelmonEVHP + TextFormatting.LIGHT_PURPLE + " Stat: " + TextFormatting.GREEN + pixelmonStatsHP));
						loreList.appendTag(new NBTTagString(TextFormatting.RED + "Atk: IV: " + TextFormatting.GREEN + pixelmonIVAttack + TextFormatting.RED + " EV: " + TextFormatting.GREEN + pixelmonEVAttack + TextFormatting.RED + " Stat: " + TextFormatting.GREEN + pixelmonStatsAttack));
						loreList.appendTag(new NBTTagString(TextFormatting.GOLD + "Def: IV: " + TextFormatting.GREEN + pixelmonIVDefense + TextFormatting.GOLD + " EV: " + TextFormatting.GREEN + pixelmonEVDefense + TextFormatting.GOLD + " Stat: " + TextFormatting.GREEN + pixelmonStatsDefense));
						loreList.appendTag(new NBTTagString(TextFormatting.DARK_PURPLE + "SpAtt: IV: " + TextFormatting.GREEN + pixelmonIVSpAtt + TextFormatting.DARK_PURPLE + " EV: " + TextFormatting.GREEN + pixelmonEVSpAtt + TextFormatting.DARK_PURPLE + " Stat: " + TextFormatting.GREEN + pixelmonStatsSpAtt));
						loreList.appendTag(new NBTTagString(TextFormatting.YELLOW + "SpDef: IV: " + TextFormatting.GREEN + pixelmonIVSpDef + TextFormatting.YELLOW + " EV: " + TextFormatting.GREEN + pixelmonEVSpDef + TextFormatting.YELLOW + " Stat: " + TextFormatting.GREEN + pixelmonStatsSpDef));
						loreList.appendTag(new NBTTagString(TextFormatting.AQUA + "Speed: IV: " + TextFormatting.GREEN + pixelmonIVSpeed + TextFormatting.AQUA + " EV: " + TextFormatting.GREEN + pixelmonEVSpeed + TextFormatting.AQUA + " Stat: " + TextFormatting.GREEN + pixelmonStatsSpeed));
						
						loreList.appendTag(new NBTTagString(TextFormatting.DARK_PURPLE + "Moves:"));

						if(!pixelmon.isEgg)
						{
							for(Attack move : pixelmonMoves)
							{
								loreList.appendTag(new NBTTagString(TextFormatting.GREEN + move.baseAttack.getLocalizedName()));
							}
						}
						else
						{
							loreList.appendTag(new NBTTagString(TextFormatting.GREEN + "???"));
						}
						
						tags.getCompoundTag("display").setTag("Lore", loreList);
						
						tags.setTag("pixelmonData", playerStorage.getNBT(playerStorage.getIDFromPosition(partyNumber)));

						playerStorage.changePokemonAndAssignID(partyNumber, null);
				        PixelmonStorage.pokeBallManager.savePlayer(server, playerStorage);
						
						item.setTagCompound(tags);
						
						
						if(!player.inventory.addItemStackToInventory(item))
						{
							World w = player.getEntityWorld();
							w.spawnEntity(new EntityItem(w, player.lastTickPosX, player.lastTickPosY, player.lastTickPosZ, item));
						}
						
						sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Itemized " + TextFormatting.RED + pixelmon.getPokemonName()));
											
					}
					else
					{
						sender.sendMessage(new TextComponentString(TextFormatting.RED + "Party slot is empty!"));						
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
			return;
		}
		else
		{
			sender.sendMessage(new TextComponentString(TextFormatting.RED + "You don't have permission to use this command"));
			return;
		}

	}

	public String getShiny(boolean shinyID)
	{
		String returnShiny = "";
		
		if(shinyID)
		{
			returnShiny = "Yes";			
		}
		else
		{
			returnShiny = "No";
		}
		
		return returnShiny;
	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender) 
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index) {
		// TODO Auto-generated method stub
		return false;
	}

}
