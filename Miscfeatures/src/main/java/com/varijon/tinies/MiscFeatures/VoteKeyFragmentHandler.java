package com.varijon.tinies.MiscFeatures;

import com.pixelmonmod.pixelmon.config.PixelmonItems;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class VoteKeyFragmentHandler 
{
	@SubscribeEvent
	public void onPlayerInteract(PlayerInteractEvent.RightClickItem event)
	{
		EntityPlayerMP player = (EntityPlayerMP) event.getEntityPlayer();
		if(CheckForVotekeyFragment(player.getHeldItem(EnumHand.MAIN_HAND)))
		{
			if(player.getHeldItem(EnumHand.MAIN_HAND).getCount() >= 3)
			{			
				player.sendMessage(new TextComponentString(TextFormatting.GREEN + "Obtained a" + TextFormatting.RED + " Vote Key " + TextFormatting.GREEN + "!"));				
				
				ItemStack finalItem = new ItemStack(PixelmonItems.itemPixelmonSprite);
				finalItem.setTagCompound(new NBTTagCompound());
									
				NBTTagCompound tags = finalItem.getTagCompound();
				tags.setString("SpriteName", "pixelmon:items/helditems/widelens");
				tags.setString("keyTag", "Vote Key");
				tags.setTag("display", new NBTTagCompound());
				tags.getCompoundTag("display").setString("Name", TextFormatting.RED + "Vote Key");					

				NBTTagList loreList = new NBTTagList();
				
				loreList.appendTag(new NBTTagString(TextFormatting.LIGHT_PURPLE + "* Use this to open a Voting Chest in Spawn"));
				loreList.appendTag(new NBTTagString(TextFormatting.LIGHT_PURPLE + "* Voting Keys are acquired by Voting"));
				tags.getCompoundTag("display").setTag("Lore", loreList);
				
				finalItem.addEnchantment(Enchantment.getEnchantmentByID(21), 1);
				finalItem.setTagCompound(tags);
				
				if(!player.inventory.addItemStackToInventory(finalItem))
				{
					World w = player.getEntityWorld();
					w.spawnEntity(new EntityItem(w, player.lastTickPosX, player.lastTickPosY, player.lastTickPosZ, finalItem));
				}
				
				if(player.getHeldItem(EnumHand.MAIN_HAND).getCount() > 3)
				{
					player.getHeldItem(EnumHand.MAIN_HAND).setCount(player.getHeldItem(EnumHand.MAIN_HAND).getCount() - 3);
				}
				else
				{
					player.inventory.removeStackFromSlot(player.inventory.currentItem);
				}
				player.inventoryContainer.detectAndSendChanges();
			}
			else
			{
				int remaining = 3 - player.getHeldItem(EnumHand.MAIN_HAND).getCount();
				player.sendMessage(new TextComponentString(TextFormatting.RED + "You need " + TextFormatting.GOLD + remaining + TextFormatting.RED + " more Vote Key fragment(s)!"));
			}

		}

	}
	
	public boolean CheckForVotekeyFragment(ItemStack item)
	{
		if(item != null)
		{
			NBTTagCompound nbt = item.getTagCompound();
			if(nbt != null)
			{
				if(nbt.hasKey("votekeyFragment"))
				{
					return true;
				}
			}
		}
		return false;
	}
}
