package com.varijon.tinies.MiscFeatures;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ChestCommandHandler 
{

	MinecraftServer server;
	
	public ChestCommandHandler() 
	{
		server = FMLCommonHandler.instance().getMinecraftServerInstance();
	}
	
	@SubscribeEvent
	public void onPlayerInteract(PlayerInteractEvent.RightClickBlock event)
	{
		EntityPlayerMP player = (EntityPlayerMP) event.getEntityPlayer();
		World w = player.getServerWorld();
		if(event.getWorld().getBlockState(event.getPos()).getBlock() == Blocks.CHEST)
		{
			TileEntity chest = w.getTileEntity(event.getPos());
			if(chest != null)
			{
				NBTTagCompound nbt = new NBTTagCompound();
				chest.writeToNBT(nbt);
				if(nbt.hasKey("ForgeData"))
				{
					NBTTagCompound forgeNBT = nbt.getCompoundTag("ForgeData");
					if(forgeNBT.hasKey("commands"))
					{
						event.setCanceled(true);
						if(forgeNBT.hasKey("keyTag"))
						{
							String keyTag = forgeNBT.getString("keyTag");
							if(!checkForKey(player.getHeldItem(EnumHand.MAIN_HAND),keyTag))
							{
								player.sendMessage(new TextComponentString(TextFormatting.RED + "This chest requires a " + keyTag + " to open!"));
								return;
							}
							if(player.getHeldItem(EnumHand.MAIN_HAND).getCount() > 1)
							{
								player.getHeldItem(EnumHand.MAIN_HAND).setCount(player.getHeldItem(EnumHand.MAIN_HAND).getCount() - 1);
							}
							else
							{
								player.inventory.removeStackFromSlot(player.inventory.currentItem);
							}
						}					
						NBTTagList commands = forgeNBT.getTagList("commands", Constants.NBT.TAG_STRING);
						for(int x = 0; x < commands.tagCount(); x++)
						{
							String modCommand = commands.getStringTagAt(x).replaceAll("@p", player.getName());
							server.getCommandManager().executeCommand(server, modCommand);
						}
						
						if(forgeNBT.hasKey("successMessage"))
						{
							String modMessage = forgeNBT.getString("successMessage").replaceAll("&", "\u00A7");
							player.sendMessage(new TextComponentString(modMessage));
						}
					}
				}
			}
		}
	}
	boolean checkForKey(ItemStack item, String keyTag)
	{
		if(item != null)
		{
			NBTTagCompound nbt = item.getTagCompound();
			if(nbt != null)
			{
				if(nbt.hasKey("keyTag"))
				{
					if(nbt.getString("keyTag").equals(keyTag))
					{
						return true;						
					}
				}
			}
		}
		return false;
	}
	
}
