package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.pixelmonmod.pixelmon.RandomHelper;
import com.pixelmonmod.pixelmon.config.PixelmonItems;
import com.pixelmonmod.pixelmon.config.PixelmonItemsHeld;
import com.pixelmonmod.pixelmon.config.PixelmonItemsTools;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class TreeBerryHandler 
{
	Random rng;
	
	Map<Item,Float> berryMap;
	ArrayList<Item> axeList;
	
	float COMMONBERRY = 0.001f;
	float UNCOMMONBERRY = 0.0005f;
	float RAREBERRY = 0.0003f;
	
	public TreeBerryHandler()
	{
		rng = new Random();
		axeList = new ArrayList<Item>();
		axeList.add(Items.WOODEN_PICKAXE);
		axeList.add(Items.IRON_AXE);
		axeList.add(Items.STONE_AXE);
		axeList.add(Items.GOLDEN_AXE);
		axeList.add(Items.DIAMOND_AXE);
		axeList.add(PixelmonItemsTools.amethystAxeItem);
		axeList.add(PixelmonItemsTools.axeAluminium);
		axeList.add(PixelmonItemsTools.crystalAxeItem);
		axeList.add(PixelmonItemsTools.dawnstoneAxeItem);
		axeList.add(PixelmonItemsTools.duskstoneAxeItem);
		axeList.add(PixelmonItemsTools.firestoneAxeItem);
		axeList.add(PixelmonItemsTools.leafstoneAxeItem);
		axeList.add(PixelmonItemsTools.moonstoneAxeItem);
		axeList.add(PixelmonItemsTools.rubyAxeItem);
		axeList.add(PixelmonItemsTools.sapphireAxeItem);
		axeList.add(PixelmonItemsTools.sunstoneAxeItem);
		axeList.add(PixelmonItemsTools.thunderstoneAxeItem);
		axeList.add(PixelmonItemsTools.waterstoneAxeItem);
		
		berryMap = new HashMap<Item, Float>();
		berryMap.put(PixelmonItemsHeld.oranBerry, 0.003f);
		berryMap.put(PixelmonItemsHeld.aspearBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.cheriBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.chestoBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.drashBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.pechaBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.persimBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.pumkinBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.rawstBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.tougaBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.yagoBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.iapapaBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.magoBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.aguavBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.figyBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.magoBerry, COMMONBERRY);
		berryMap.put(PixelmonItemsHeld.wikiBerry, COMMONBERRY);
		
		berryMap.put(PixelmonItemsHeld.enigmaBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.ginemaBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.leppaBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.babiriBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.chartiBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.chilanBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.chopleBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.cobaBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.colburBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.habanBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.kasibBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.kebiaBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.occaBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.passhoBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.payapaBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.rindoBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.roseliBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.shucaBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.tangaBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.wacanBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.yacheBerry, UNCOMMONBERRY);
		
		berryMap.put(PixelmonItemsHeld.apicotBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.custapBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.ganlonBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.lansatBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.liechiBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.micleBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.petayaBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.salacBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.starfBerry, UNCOMMONBERRY);
		
		berryMap.put(PixelmonItemsHeld.jabocaBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.rowapBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.keeBerry, UNCOMMONBERRY);
		berryMap.put(PixelmonItemsHeld.marangaBerry, UNCOMMONBERRY);
		
		berryMap.put(PixelmonItemsHeld.sitrusBerry, RAREBERRY);	
		berryMap.put(PixelmonItemsHeld.lumBerry, RAREBERRY);	
		berryMap.put(PixelmonItems.pomegBerry, RAREBERRY);
		berryMap.put(PixelmonItems.kelpsyBerry, RAREBERRY);
		berryMap.put(PixelmonItems.qualotBerry, RAREBERRY);
		berryMap.put(PixelmonItems.hondewBerry, RAREBERRY);
		berryMap.put(PixelmonItems.tamatoBerry, RAREBERRY);
		berryMap.put(PixelmonItems.grepaBerry, RAREBERRY);
	}
	
	@SubscribeEvent
	public void onBlockHarvest(HarvestDropsEvent event)
	{
		if(event.getState().getBlock() == Blocks.LEAVES || event.getState().getBlock() == Blocks.LEAVES2)
		{
			if(event.getHarvester() == null || event.getHarvester().getHeldItem(EnumHand.MAIN_HAND) == null)
			{
				List<ItemStack> newDrops = EditDrops(event.getDrops());
				event.getDrops().clear();
				event.getDrops().addAll(newDrops);
			}
			else
			{
				ItemStack tool = event.getHarvester().getHeldItem(EnumHand.MAIN_HAND);
				
				List<ItemStack> newDrops = EditDrops(event.getDrops(), tool, event.getFortuneLevel());
				event.getDrops().clear();
				event.getDrops().addAll(newDrops);
			}
		}
	}
	
	public List<ItemStack> EditDrops(List<ItemStack> dropsList, ItemStack tool, int fortuneLevel)
	{
		
		ArrayList<ItemStack> newDropList = new ArrayList<ItemStack>();
		boolean droppedLeaves = false;
		for(ItemStack item : dropsList)
		{
			newDropList.add(item);
			if(Block.getBlockFromItem(item.getItem()) == Blocks.LEAVES || Block.getBlockFromItem(item.getItem()) == Blocks.LEAVES2 )
			{
				droppedLeaves = true;
			}
		}
		if(!droppedLeaves)
		{
			if(axeList.contains(tool.getItem()))
			{
				newDropList.addAll(getBerries(fortuneLevel));
			}
			else
			{
				newDropList.addAll(getBerries(0));
			}
		}
		return newDropList;
	}
	public List<ItemStack> EditDrops(List<ItemStack> dropsList)
	{
		
		ArrayList<ItemStack> newDropList = new ArrayList<ItemStack>();
		boolean droppedLeaves = false;
		for(ItemStack item : dropsList)
		{
			newDropList.add(item);
			if(Block.getBlockFromItem(item.getItem()) == Blocks.LEAVES || Block.getBlockFromItem(item.getItem()) == Blocks.LEAVES2 )
			{
				droppedLeaves = true;
			}
		}
		if(!droppedLeaves)
		{
			newDropList.addAll(getBerries(0));
		}
		return newDropList;
	}
	
	private ArrayList<ItemStack> getBerries(int fortuneLevel)
	{
		ArrayList<ItemStack> berryList = new ArrayList<ItemStack>();
		
		for(Map.Entry<Item, Float> berry : berryMap.entrySet())
		{
			if(RandomHelper.getRandomChance(berry.getValue() * (float) (fortuneLevel + 1f)))
			{
				berryList.add(new ItemStack(berry.getKey(), 1));
			}
		}
		
		return berryList;
	}
	
}


