package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class InfoBoardCommand implements ICommand {

	private List aliases;
	public static HashMap<String, String> userModes = new HashMap<String, String>();
	public static HashMap<String, Scoreboard> userBoards = new HashMap<String, Scoreboard>();
	
	public InfoBoardCommand()
	{
	   this.aliases = new ArrayList();
	   this.aliases.add("infoboard");
	   userModes = new HashMap<String, String>();
	   userBoards = new HashMap<String, Scoreboard>();
	}
	
	@Override
	public int compareTo(ICommand arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "infoboard";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		// TODO Auto-generated method stub
		return "infoboard [mode]";
	}

	@Override
	public List<String> getAliases() {
		// TODO Auto-generated method stub
		return this.aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException 
	{
		if(sender.canUseCommand(4, "miscfeatures.infoboard"))
		{
			if(args.length == 0)
			{
				sender.sendMessage(new TextComponentString(TextFormatting.RED + "Usage: /infoboard [evs1-evs6|egg|off]"));						
				return;
			}
			if(sender instanceof EntityPlayerMP)
			{
				EntityPlayerMP player = (EntityPlayerMP) sender;
				

				if(args[0].equalsIgnoreCase("evs1"))
				{		
					userModes.put(player.getUniqueID().toString(), "evs1");
					sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Set infoboard mode to " + args[0]));
					return;
				}
				if(args[0].equalsIgnoreCase("evs2"))
				{
					userModes.put(player.getUniqueID().toString(), "evs2");	
					sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Set infoboard mode to " + args[0]));
					return;				
				}
				if(args[0].equalsIgnoreCase("evs3"))
				{
					userModes.put(player.getUniqueID().toString(), "evs3");	
					sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Set infoboard mode to " + args[0]));
					return;
				}
				if(args[0].equalsIgnoreCase("evs4"))
				{
					userModes.put(player.getUniqueID().toString(), "evs4");		
					sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Set infoboard mode to " + args[0]));
					return;				
				}
				if(args[0].equalsIgnoreCase("evs5"))
				{
					userModes.put(player.getUniqueID().toString(), "evs5");	
					sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Set infoboard mode to " + args[0]));
					return;
				}
				if(args[0].equalsIgnoreCase("evs6"))
				{
					userModes.put(player.getUniqueID().toString(), "evs6");	
					sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Set infoboard mode to " + args[0]));
					return;
				}
				if(args[0].equalsIgnoreCase("egg"))
				{
					userModes.put(player.getUniqueID().toString(), "egg");	
					sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Set infoboard mode to " + args[0]));
					return;
				}
				if(args[0].equalsIgnoreCase("player"))
				{
					userModes.put(player.getUniqueID().toString(), "player");	
					sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Set infoboard mode to " + args[0]));
					return;
				}
				if(args[0].equalsIgnoreCase("off"))
				{
					userModes.put(player.getUniqueID().toString(), "off");	
					sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Set infoboard mode to " + args[0]));
					return;
				}
				sender.sendMessage(new TextComponentString(TextFormatting.RED + "Invalid infoboard mode, type /infoboard for options "));
			}
			return;
		}
		else
		{
			sender.sendMessage(new TextComponentString(TextFormatting.RED + "You don't have permission to use this command"));
			return;
		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender) 
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index) {
		// TODO Auto-generated method stub
		return false;
	}
	

}
