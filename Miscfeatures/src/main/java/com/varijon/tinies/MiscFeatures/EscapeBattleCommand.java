package com.varijon.tinies.MiscFeatures;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.pixelmonmod.pixelmon.battles.BattleRegistry;
import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.battles.controller.participants.Spectator;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class EscapeBattleCommand implements ICommand {

	private List aliases;
	public EscapeBattleCommand()
	{
	   this.aliases = new ArrayList();
	   this.aliases.add("escape");
	   this.aliases.add("escapebattle");
	}
	
	@Override
	public int compareTo(ICommand arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "escape";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		// TODO Auto-generated method stub
		return "escape";
	}

	@Override
	public List<String> getAliases() {
		// TODO Auto-generated method stub
		return this.aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException 
	{
		if(sender.canUseCommand(4, "miscfeatures.escapebattle"))
		{
			BattleControllerBase battlecontrol = BattleRegistry.getBattle(sender.getName());
			if(battlecontrol != null)
			{
				for(Spectator spec : battlecontrol.spectators)
				{
					if(spec.getEntity().getName() == sender.getName())
					{
						sender.sendMessage(new TextComponentString(TextFormatting.RED + "Cannot end a battle when spectating!"));
						System.out.println(sender.getName() + " tried to /escape battle while spectating!");
						
						String modCommand = "tempban " + sender.getName() + " 10m Banned for 10 minutes, tried to end battle as spectator";
					
						server.getCommandManager().executeCommand(server, modCommand);
						return;
					}						
				}
				sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Battle ended."));			
				battlecontrol.endBattleWithoutXP();
			}	
			else
			{
				if(args.length == 1)
				{
					if(args[0].equals("kick"))
					{
						EntityPlayerMP player = (EntityPlayerMP) sender;
						PlayerStorage ps;
						Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(player);
						if (!optPlayerStorage.isPresent()) 
						{
					        return;
					    }
						ps = (PlayerStorage) optPlayerStorage.get();

						if(ps.guiOpened)
						{
							String modCommand = "kick " + player.getName() + " Kicked to end battle.";

							server.getCommandManager().executeCommand(server, modCommand);
							return;
						}
						else
						{
							sender.sendMessage(new TextComponentString(TextFormatting.RED + "No battle found and no battle screen open, this kick command only works in battle."));
							return;
						}
					}
				}
//				EntityPlayerMP player = (EntityPlayerMP) sender;
//				try {
//					PlayerStorage ps = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
//					if(ps.guiOpened)
//					{
//						System.out.println("guiopen");
//						Pixelmon.network.sendTo(new EndSpectate(), player);
//						Pixelmon.network.sendTo(new ExitBattle(), player);
//					}
//				} catch (PlayerNotLoadedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				sender.sendMessage(new TextComponentString(TextFormatting.RED + "Battle not found, try /escape kick to be kicked"));
			}
			return;
		}
		else
		{
			sender.sendMessage(new TextComponentString(TextFormatting.RED + "You don't have permission to use this command"));
			return;
		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender) 
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index) {
		// TODO Auto-generated method stub
		return false;
	}

}
