package com.varijon.tinies.MiscFeatures;

import com.pixelmonmod.pixelmon.RandomHelper;
import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.enums.EnumBossMode;
import com.pixelmonmod.pixelmon.enums.EnumMegaPokemon;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;

import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class BossHandler 
{
	
	public BossHandler()
	{
	}
	
	@SubscribeEvent
	public void onPixelmonSpawn(EntityJoinWorldEvent event)
	{	
		if(event.getEntity() instanceof EntityPixelmon)
		{
			EntityPixelmon pixelmon = (EntityPixelmon) event.getEntity();
			if(!pixelmon.hasOwner())
			{
				BattleControllerBase battle = pixelmon.battleController;
				if(pixelmon.battleController != null)
				{
					if(battle.isInBattle(pixelmon.getPixelmonWrapper()))
					{
						return;
					}
				}
				if(EnumMegaPokemon.getMega(pixelmon.getSpecies()) != null)
				{
					return;
				}
				if(EnumPokemon.legendaries.contains(pixelmon.getSpecies()))
				{
					if(pixelmon.isBossPokemon())
					{
						//maybe an unboss command instead
						pixelmon.setBoss(EnumBossMode.NotBoss);
						pixelmon.updateStats();
					}
					return;
				}
				if(pixelmon.getIsShiny())
				{
					return;
				}
				if(RandomHelper.getRandomNumberBetween(0, 40) == 1)
				{
					int randomNumber = RandomHelper.getRandomNumberBetween(0, 100);
					if(randomNumber <= 10)
					{
						pixelmon.setBoss(EnumBossMode.Ultimate);
						return;
					}
					if(randomNumber <= 40)
					{
						pixelmon.setBoss(EnumBossMode.Legendary);
						return;
					}
					if(randomNumber <= 70)
					{
						pixelmon.setBoss(EnumBossMode.Rare);
						return;
					}
					if(randomNumber <= 100)
					{
						pixelmon.setBoss(EnumBossMode.Uncommon);
						return;
					}
				}

			}
		}
	}
	
}
