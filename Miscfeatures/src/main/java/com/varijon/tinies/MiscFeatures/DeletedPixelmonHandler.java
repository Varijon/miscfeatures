package com.varijon.tinies.MiscFeatures;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.pixelmonmod.pixelmon.api.enums.DeleteType;
import com.pixelmonmod.pixelmon.api.events.PixelmonDeletedEvent;
import com.pixelmonmod.pixelmon.config.PixelmonBlocks;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class DeletedPixelmonHandler 
{
	public static HashMap<String, ArrayList<DeletedPixelmon>> deletedPixelmon;
	
	public DeletedPixelmonHandler()
	{
		deletedPixelmon = new HashMap<String, ArrayList<DeletedPixelmon>>();
	}
	
	@SubscribeEvent
	public void onPixelmonDelete(PixelmonDeletedEvent event)
	{
		if(event.deleteType != DeleteType.COMMAND)
		{
			//long startTime = System.nanoTime();
	
			String date = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
			String entry = date + " Player: " + event.player.getName() + " Pixelmon: " + event.pokemon.getString("Name");
			DeletedPokemonFile.writeFile(entry);
			//DeletedPokemonFile.writeNBTFile(event.player.getHeldItem().writeToNBT(new NBTTagCompound()), date);
	
			//long endTime = System.nanoTime();
			//event.player.addChatMessage(new TextComponentString("Took " + (endTime - startTime) + "ns or " + ((endTime - startTime) / 1000000) + "ms"));
	
	//		EntityPixelmon pixelmon = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(event.pokemon, event.player.worldObj);
	//		
	//		int pixelmonEVHP = pixelmon.stats.EVs.HP;
	//		int pixelmonEVAttack = pixelmon.stats.EVs.Attack;
	//		int pixelmonEVDefense = pixelmon.stats.EVs.Defence;
	//		int pixelmonEVSpAtt = pixelmon.stats.EVs.SpecialAttack;
	//		int pixelmonEVSpDef = pixelmon.stats.EVs.SpecialDefence;
	//		int pixelmonEVSpeed = pixelmon.stats.EVs.Speed;
	//		
	//		int combinedEV = pixelmonEVHP + pixelmonEVAttack + pixelmonEVDefense + pixelmonEVSpAtt + pixelmonEVSpDef + pixelmonEVSpeed;
	//	    
	//		if(combinedEV > 500)
	//		{
	//			event.setCanceled(true);
	//		}
			date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
			if(deletedPixelmon.containsKey(event.player.getUniqueID().toString()))
			{
				ArrayList<DeletedPixelmon> deletedPixelmonList = deletedPixelmon.get(event.player.getUniqueID().toString());
				deletedPixelmonList.add(new DeletedPixelmon(event.pokemon, date));
			}
			else
			{
				deletedPixelmon.put(event.player.getUniqueID().toString(), new ArrayList<DeletedPixelmon>());
				ArrayList<DeletedPixelmon> deletedPixelmonList = deletedPixelmon.get(event.player.getUniqueID().toString());
				deletedPixelmonList.add(new DeletedPixelmon(event.pokemon, date));
			}
			//long startTime = System.nanoTime();
	
//			if(checkForPCBlock(new BlockPos(event.player), event.player.worldObj))
//			{
				event.player.sendMessage(new TextComponentString(TextFormatting.GRAY + event.pokemon.getString("Name") + " was released via the PC"));
//			}
//			else
//			{
//				event.player.addChatMessage(new TextComponentString(TextFormatting.DARK_RED + event.pokemon.getString("Name") + " was attempted to be released without a PC nearby"));
//				event.player.addChatMessage(new TextComponentString(TextFormatting.DARK_RED + "Release undone, may need to relog"));
//				
//				MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
//				
//				for(EntityPlayerMP targetPlayer : server.getPlayerList().getPlayerList())
//				{
//					if(targetPlayer.canCommandSenderUseCommand(4, "miscfeatures.releasenotification"))
//					{
//						targetPlayer.addChatMessage(new TextComponentString(TextFormatting.YELLOW + event.player.getName() + "'s " + TextFormatting.DARK_RED + event.pokemon.getString("Name") + " was attempted to be released without a PC nearby"));
//					}
//				}
//				ArrayList<DeletedPixelmon> deletedPixelmonList = deletedPixelmon.get(event.player.getUniqueID().toString());
//				DeletedPixelmon pixelmon = deletedPixelmonList.get(deletedPixelmonList.size()-1);
//			
//				Optional<PlayerStorage> optPlayerStorage =  PixelmonStorage.pokeBallManager.getPlayerStorage(event.player);
//				if (!optPlayerStorage.isPresent()) 
//				{
//			        return;
//			    }
//				PlayerStorage playerStorage = (PlayerStorage) optPlayerStorage.get();
//				EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(pixelmon.GetPokemonData(), event.player.getEntityWorld());
//				playerStorage.addToParty(pokemon);
//				PixelmonStorage.pokeBallManager.savePlayer(server, playerStorage);
//					
//				deletedPixelmonList.remove(deletedPixelmonList.size()-1);
//	
//			}
			//long endTime = System.nanoTime();
			//event.player.addChatMessage(new TextComponentString("Took " + (endTime - startTime) + "ns or " + ((endTime - startTime) / 1000000) + "ms"));

		}
	}
	
	public boolean checkForPCBlock(BlockPos loc, World w)
	{
		for(int x = -15; x < 16; x++)
		{
			for(int z = -15; z < 16; z++)
			{
				for(int y = -15; y < 16; y++)
				{
					BlockPos locNew = loc.add(x, y, z);
					
					if(w.getBlockState(locNew).getBlock().equals(PixelmonBlocks.pc))
					{
						return true;
					}
														
				}
			}
		}
		return false;
	}
}
